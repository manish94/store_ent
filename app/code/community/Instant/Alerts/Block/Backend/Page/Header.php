<?php
class Instant_Alerts_Block_Backend_Page_Header
    extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface
{

    /**
     * Render fieldset html
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $html = '<div style="background:#EAF0EE;border:1px solid #CCCCCC;margin-bottom:10px;padding:20px 15px 15px; border-radius: 5px;">
                    <div style="margin: 0 auto;">
                    <h4 style="color:#EA7601;">SMS Community Edition v1.0.0 </h4>
                    <h4>This module requires <a href="http://springedge.com/messaging.php"> SpringEdge Messaging account </a> with API key and SMS credits. 
                    
                    </div>

                ';

        return $html;
    }
}