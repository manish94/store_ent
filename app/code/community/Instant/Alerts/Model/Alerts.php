<?php

class Instant_Alerts_Model_Alerts extends Mage_Core_Model_Abstract
{
    const CONFIG_PATH = 'alerts/';
    public function _construct()
    {
        parent::_construct();
        $this->_init('alerts/alerts');
    }

}