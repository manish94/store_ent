<?php
class Instant_Alerts_Model_Observer
{
	public function sendSmsOnOrderCreated(Varien_Event_Observer $observer)
	{
		if($this->getHelper()->isOrdersEnabled()) {
            $orders = $observer->getEvent()->getOrderIds();
			$order = Mage::getModel('sales/order')->load($orders['0']);
			if ($order instanceof Mage_Sales_Model_Order) {
                $host = "https://instantalerts.co/api/web/send/";
                

                $password = $this->getHelper()->getPassword();
				//$password = "136xe6k36d4rj605sx24oynicol5715nl8j";
                $smsto = $this->getHelper()->getTelephoneFromOrder($order);
                $smsfrom = $this->getHelper()->getSender();
				 //$smsfrom = "ENTSTO";
                $smsmsg = $this->getHelper()->getMessage($order);
				
				$grand_total = number_format($order->getGrandTotal());
				$smsmsg = str_replace("{{order_value}}",$grand_total,$smsmsg);
				
				
                
                //$data  .= '&username=' . urlencode($username);
                $data = '?apikey=' . urlencode($password);
                $data .= '&to=' . urlencode($smsto);
                $data .= '&sender=' . urlencode($smsfrom);
                $data .= '&message=' . urlencode($smsmsg);
                
                
                $url = $host.$data;
				echo $url . "<br>";
                $sendSms = $this->getHelper()->alerts($url);
            	try {
					Mage::getModel('alerts/alerts')
						->setOrderId($order->getIncrementId())
						->setFrom($smsfrom)
						->setTo($smsto)
						->setSmsMessage($smsmsg)
						->setStatus($sendSms['status'])
						->setStatusMessage($sendSms['status_message'])
						->setCreatedTime(now())
						->save();
				}
                catch (Exception $e) {}

				if($this->getHelper()->isOrdersNotify() and $this->getHelper()->getAdminTelephone()) {
				
				$host = "https://instantalerts.co/api/web/send/";
				
					$smsto = $this->getHelper()->getAdminTelephone();
					$smsmsg = Mage::helper('alerts')->__('A new order has been placed: %s',$order->getIncrementId());
                    //$data  .= '&username=' . urlencode($username);
                $data = '?apikey=' . urlencode($password);
                $data .= '&to=' . urlencode($smsto);
                $data .= '&sender=' . urlencode($smsfrom);
                $data .= '&message=' . urlencode($smsmsg);
                
                
                    $url = $host.$data;
                    $sendSms = $this->getHelper()->alerts($url);
					try {
						Mage::getModel('alerts/alerts')
							->setOrderId($order->getIncrementId())
							->setFrom($smsfrom)
							->setTo($smsto)
							->setSmsMessage($smsmsg)
							->setStatus($sendSms['status'])
							->setStatusMessage($sendSms['status_message'])
							->setCreatedTime(now())
							->save();
					}
					catch (Exception $e) {}
				}
			}
		}
	}
	
	public function sendSmsOnOrderHold(Varien_Event_Observer $observer)
	{
		if($this->getHelper()->isOrderHoldEnabled()) {
			$order = $observer->getOrder();
			if ($order instanceof Mage_Sales_Model_Order) {
				if ($order->getState() !== $order->getOrigData('state') && $order->getState() === Mage_Sales_Model_Order::STATE_HOLDED) {
                    $host = "https://instantalerts.co/api/web/send/";
                    

					$password = $this->getHelper()->getPassword();
					$smsto = $this->getHelper()->getTelephoneFromOrder($order);
					$smsfrom = $this->getHelper()->getSenderForOrderHold();
					$smsmsg = $this->getHelper()->getMessageForOrderHold($order);
                    //$data  .= '&username=' . urlencode($username);
                $data = '?apikey=' . urlencode($password);
                $data .= '&to=' . urlencode($smsto);
                $data .= '&sender=' . urlencode($smsfrom);
                $data .= '&message=' . urlencode($smsmsg);
                
                
                    $url = $host.$data;
                    $sendSms = $this->getHelper()->alerts($url);
					try {
						Mage::getModel('alerts/alerts')
							->setOrderId($order->getIncrementId())
							->setFrom($smsfrom)
							->setTo($smsto)
							->setSmsMessage($smsmsg)
							->setStatus($sendSms['status'])
							->setStatusMessage($sendSms['status_message'])
							->setCreatedTime(now())
							->save();
					}
					catch (Exception $e) {}
                    if($this->getHelper()->isOrdersHoldNotify() and $this->getHelper()->getAdminHoldTelephone()) {
                        $smsto = $this->getHelper()->getAdminHoldTelephone();
                        $smsmsg = Mage::helper('alerts')->__('%s has been placed on hold',$order->getIncrementId());
                       //$data  .= '&username=' . urlencode($username);
                $data = '?apikey=' . urlencode($password);
                $data .= '&to=' . urlencode($smsto);
                $data .= '&sender=' . urlencode($smsfrom);
                $data .= '&message=' . urlencode($smsmsg);
                
                
                        $url = $host.$data;
                        $sendSms = $this->getHelper()->alerts($url);
                        try {
                            Mage::getModel('alerts/alerts')
                                ->setOrderId($order->getIncrementId())
                                ->setFrom($smsfrom)
                                ->setTo($smsto)
                                ->setSmsMessage($smsmsg)
                                ->setStatus($sendSms['status'])
                                ->setStatusMessage($sendSms['status_message'])
                                ->setCreatedTime(now())
                                ->save();
                        }
                        catch (Exception $e) {}
                    }
				}
			}
		}
	}
	
	public function sendSmsOnOrderUnhold(Varien_Event_Observer $observer)
	{
		if($this->getHelper()->isOrderUnholdEnabled()) {
			$order = $observer->getOrder();
			if ($order instanceof Mage_Sales_Model_Order) {
				if ($order->getState() !== $order->getOrigData('state') && $order->getOrigData('state') === Mage_Sales_Model_Order::STATE_HOLDED) {
                    $host = "https://instantalerts.co/api/web/send/";

					$password = $this->getHelper()->getPassword();
					$smsto = $this->getHelper()->getTelephoneFromOrder($order);
					$smsfrom = $this->getHelper()->getSenderForOrderUnhold();
					$smsmsg = $this->getHelper()->getMessageForOrderUnhold($order);
					$data = '?apikey=' . urlencode($password);
					$data .= '&to=' . urlencode($smsto);
					$data .= '&sender=' . urlencode($smsfrom);
					$data .= '&message=' . urlencode($smsmsg);
					
                
                    $url = $host.$data;
                    $sendSms = $this->getHelper()->alerts($url);
					try {
						Mage::getModel('alerts/alerts')
							->setOrderId($order->getIncrementId())
							->setFrom($smsfrom)
							->setTo($smsto)
							->setSmsMessage($smsmsg)
							->setStatus($sendSms['status'])
							->setStatusMessage($sendSms['status_message'])
							->setCreatedTime(now())
							->save();
					}
					catch (Exception $e) {}
                    if($this->getHelper()->isOrdersUnholdNotify() and $this->getHelper()->getAdminUnholdTelephone()) {
                        $smsto = $this->getHelper()->getAdminUnholdTelephone();
                        $smsmsg = Mage::helper('alerts')->__('%s has been placed on unhold',$order->getIncrementId());
                       //$data  .= '&username=' . urlencode($username);
                $data = '?apikey=' . urlencode($password);
                $data .= '&to=' . urlencode($smsto);
                $data .= '&sender=' . urlencode($smsfrom);
                $data .= '&message=' . urlencode($smsmsg);
                
                
                        $url = $host.$data;
                        $sendSms = $this->getHelper()->alerts($url);
                        try {
                            Mage::getModel('alerts/alerts')
                                ->setOrderId($order->getIncrementId())
                                ->setFrom($smsfrom)
                                ->setTo($smsto)
                                ->setSmsMessage($smsmsg)
                                ->setStatus($sendSms['status'])
                                ->setStatusMessage($sendSms['status_message'])
                                ->setCreatedTime(now())
                                ->save();
                        }
                        catch (Exception $e) {}
                    }
				}
			}
		}
	}
	
	public function sendSmsOnOrderCanceled(Varien_Event_Observer $observer)
	{
		if($this->getHelper()->isOrderCanceledEnabled()) {
			$order = $observer->getOrder();
			if ($order instanceof Mage_Sales_Model_Order) {
				if ($order->getState() !== $order->getOrigData('state') && $order->getState() === Mage_Sales_Model_Order::STATE_CANCELED) {
                    $host = "https://instantalerts.co/api/web/send/";
                    

					$password = $this->getHelper()->getPassword();
					$smsto = $this->getHelper()->getTelephoneFromOrder($order);
					$smsfrom = $this->getHelper()->getSenderForOrderCanceled();
					$smsmsg = $this->getHelper()->getMessageForOrderCanceled($order);

					$data = '?apikey=' . urlencode($password);
					$data .= '&to=' . urlencode($smsto);
					$data .= '&sender=' . urlencode($smsfrom);
					$data .= '&message=' . urlencode($smsmsg);
					
                
                    $url = $host.$data;
                    $sendSms = $this->getHelper()->alerts($url);
					try {
						Mage::getModel('alerts/alerts')
							->setOrderId($order->getIncrementId())
							->setFrom($smsfrom)
							->setTo($smsto)
							->setSmsMessage($smsmsg)
							->setStatus($sendSms['status'])
							->setStatusMessage($sendSms['status_message'])
							->setCreatedTime(now())
							->save();
					}
					catch (Exception $e) {}
                    if($this->getHelper()->isOrdersCancelledNotify() and $this->getHelper()->getAdminCancelledTelephone()) {
                        $smsto = $this->getHelper()->getAdminCancelledTelephone();
                        $smsmsg = Mage::helper('alerts')->__('%s has been placed cancelled',$order->getIncrementId());
                        //$data  .= '&username=' . urlencode($username);
                $data = '?apikey=' . urlencode($password);
                $data .= '&to=' . urlencode($smsto);
                $data .= '&sender=' . urlencode($smsfrom);
                $data .= '&message=' . urlencode($smsmsg);
                
                
                        $url = $host.$data;
                        $sendSms = $this->getHelper()->alerts($url);
                        try {
                            Mage::getModel('alerts/alerts')
                                ->setOrderId($order->getIncrementId())
                                ->setFrom($smsfrom)
                                ->setTo($smsto)
                                ->setSmsMessage($smsmsg)
                                ->setStatus($sendSms['status'])
                                ->setStatusMessage($sendSms['status_message'])
                                ->setCreatedTime(now())
                                ->save();
                        }
                        catch (Exception $e) {}
                    }
				}
			}
		}
	}
	
	public function sendSmsOnShipmentCreated(Varien_Event_Observer $observer)
	{
		if($this->getHelper()->isShipmentsEnabled()) {
			$shipment = $observer->getEvent()->getShipment();
			$order = $shipment->getOrder();
			if ($order instanceof Mage_Sales_Model_Order) {
                $host = "https://instantalerts.co/api/web/send/";
                

				$password = $this->getHelper()->getPassword();
				$smsto = $this->getHelper()->getTelephoneFromOrder($order);
				$smsfrom = $this->getHelper()->getSenderForShipment();
				$smsmsg = $this->getHelper()->getMessageForShipment($order);
                //$data  .= '&username=' . urlencode($username);
                $data = '?apikey=' . urlencode($password);
                $data .= '&to=' . urlencode($smsto);
                $data .= '&sender=' . urlencode($smsfrom);
                $data .= '&message=' . urlencode($smsmsg);
                
                
                $url = $host.$data;
                $sendSms = $this->getHelper()->alerts($url);
				try {
					Mage::getModel('alerts/alerts')
						->setOrderId($order->getIncrementId())
						->setFrom($smsfrom)
						->setTo($smsto)
						->setSmsMessage($smsmsg)
						->setStatus($sendSms['status'])
						->setStatusMessage($sendSms['status_message'])
						->setCreatedTime(now())
						->save();
				}
                catch (Exception $e) {}
                if($this->getHelper()->isOrdersShipmentsNotify() and $this->getHelper()->getAdminShipmentsTelephone()) {
                    $smsto = $this->getHelper()->getAdminTelephone();
                    $smsmsg = Mage::helper('alerts')->__('%s is on shipment state',$order->getIncrementId());
                
                //$data  .= '&username=' . urlencode($username);
                $data = '?apikey=' . urlencode($password);
                $data .= '&to=' . urlencode($smsto);
                $data .= '&sender=' . urlencode($smsfrom);
                $data .= '&message=' . urlencode($smsmsg);
                
                
                    $url = $host.$data;
                    $sendSms = $this->getHelper()->alerts($url);
                    try {
                        Mage::getModel('alerts/alerts')
                            ->setOrderId($order->getIncrementId())
                            ->setFrom($smsfrom)
                            ->setTo($smsto)
                            ->setSmsMessage($smsmsg)
                            ->setStatus($sendSms['status'])
                            ->setStatusMessage($sendSms['status_message'])
                            ->setCreatedTime(now())
                            ->save();
                    }
                    catch (Exception $e) {}
                }
			}
		}
	}

	public function getHelper()
    {
        return Mage::helper('alerts/Data');
    }
}