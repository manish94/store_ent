<?php
class NextBits_CheckDelivery_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction() {
		
		$zip = $this->getRequest()->getParam('zipcode');
		$pincodearray = array();
		$pindata = Mage::getStoreConfig('checkdelivery/general/pincode');
		$pincodearray = explode(",", $pindata);
		
		$cod_pincodearray = array();
		$cod_pindata = Mage::getStoreConfig('checkdelivery/general/cod_pincode');
		$cod_pincodearray = explode(",", $cod_pindata);
		
		$cod_success = "COD Available";
		$cod_failure = "COD Unavailable";
		
		$success = Mage::getStoreConfig('checkdelivery/general/success_messgae');
		$failure = Mage::getStoreConfig('checkdelivery/general/failure_messgae');
		$empty = Mage::getStoreConfig('checkdelivery/general/empty_messgae');
		$successHtml = Mage::getStoreConfig('checkdelivery/general/success_html');
		$failureHtml = Mage::getStoreConfig('checkdelivery/general/failure_html');
		$defaultHtml = Mage::getStoreConfig('checkdelivery/general/default_html');
		
		$trimedZip = trim($zip);
		$response = array();
		if(isset($trimedZip) && !empty($trimedZip)){
			if (in_array($trimedZip, $pincodearray)) { 
				$msg = "<br><div>" . $success . "</div>";
				if(in_array($trimedZip, $cod_pincodearray)) {
					$msg .= "<div style='color:red'>" . $cod_failure . "</div>";
				} else { 
					$msg .= "<div>" . $cod_success . "</div>";
				}
				$response['message'] = $msg;
				$response['color'] = 'green';
				$response['html'] = "<font color='green'>" . $successHtml . "</font>";
				
			}else{
				$response['message'] = "<br><div>Delivery Unavailable</div>";
				$response['color'] = 'red';
				$response['html'] = "<font color='red'>" . $failureHtml  . "</font>";
				
			}
		}else{
			$response['message'] = $empty;
			$response['color'] = 'orange';
			$response['html'] = $defaultHtml;
		}
		
		
		echo json_encode($response);exit;
    }
	
	public function checkoutAction() {
		$zip = $this->getRequest()->getParam('zipcode');
		$pincodearray = array();
		$pindata = Mage::getStoreConfig('checkdelivery/general/pincode');
		$pincodearray = explode(",", $pindata);
		
		$cod_pincodearray = array();
		$cod_pindata = Mage::getStoreConfig('checkdelivery/general/cod_pincode');
		$cod_pincodearray = explode(",", $cod_pindata);
		
		$cod_success = "COD Available";
		$cod_failure = "COD Unavailable";
		
		$delivery_success = "Delivery possible in your area";
		$delivery_failure = "delivery not possible in your area";
		
		$trimedZip = trim($zip);
		$response = array();
		if(isset($trimedZip) && !empty($trimedZip)) {
			if (in_array($trimedZip, $pincodearray)) {
				$response['delivery'] = "Yes";
				if(in_array($trimedZip, $cod_pincodearray)) {
					$response['cod'] = "No";
					Mage::getSingleton('core/session')->setCodStatus('No');

				} else { 
					$response['cod'] = "Yes";
					Mage::getSingleton('core/session')->setCodStatus('Yes');
				}				
			}else{
				$response['delivery'] = "No";
			}
		}
		echo json_encode($response);exit;
	}
	
	
}