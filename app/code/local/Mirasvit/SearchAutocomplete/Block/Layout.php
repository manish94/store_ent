<?php

class Mirasvit_SearchAutocomplete_Block_Layout extends Mage_Core_Block_Template
{
    private $css;
    private $template;

    public function __construct()
    {
		parent::_construct();
        $this->defineTheme();
    }

    /**
     * Set css and template file for search box
     */
    private function defineTheme()
    {
        $theme = Mage::getSingleton('searchautocomplete/config')->getTheme();
        switch ($theme) {
            case 'default':
                $css      = Mirasvit_SearchAutocomplete_Model_Config::CSS_DEFAULT;
                $template = Mirasvit_SearchAutocomplete_Model_Config::TPL_DEFAULT;
                break;
            case 'rwd':
                $css      = Mirasvit_SearchAutocomplete_Model_Config::CSS_RWD;
                $template = Mirasvit_SearchAutocomplete_Model_Config::TPL_DEFAULT;
                break;
            default:
                $css      = Mirasvit_SearchAutocomplete_Model_Config::CSS_AMAZON;
                $template = Mirasvit_SearchAutocomplete_Model_Config::TPL_AMAZON;
                break;
        }
        $this->css = $css;
        $this->template = $template;
        if (file_exists(Mage::getBaseDir('skin').'/frontend/base/default/'.Mirasvit_SearchAutocomplete_Model_Config::CSS_CUSTOM)) {
            $this->css = Mirasvit_SearchAutocomplete_Model_Config::CSS_CUSTOM;
        }
    }

    public function addSearchAutocomplete()
    {
        $this->addSearchCss();
        $this->addForm();
    }

    /**
     * Add css file to layout
     */
    public function addSearchCss()
    {
        $head = $this->getLayout()->getBlock('head');
        if ($head instanceof Mage_Core_Block_Template) {
            $head->addCss($this->css);
        }

        return $this;
    }

    /**
     * Add searchautocomplete block to layout
     */
    public function addForm()
    {
        $searchBlock = $this->getLayout()->getBlock('top.search');
		//$searchParentBlock = "";
		//echo $this->template;
        if (!$searchBlock instanceof Mage_Core_Block_Template) { 
            $searchParentBlock = $this->getLayout()->getBlock('header');
        } else { 
            //$searchParentBlock = $searchBlock->getParentBlock();
			$searchParentBlock = $this->getLayout()->getBlock('header');
            $searchParentBlock->unsetChild($searchBlock->getBlockAlias());
        }
        $searchBlock = $this->getLayout()->createBlock('searchautocomplete/form')->setTemplate($this->template)->setNameInLayout('top.search');
        if($searchParentBlock) $searchParentBlock->setChild('topSearch', $searchBlock);
    }
} 
