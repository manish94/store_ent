<?php
class Retail_Analytics_Block_Onsiteproduct    
 extends Mage_Catalog_Block_Product_Abstract
{
	protected function _prepareLayout()
	{	
		
		parent::_prepareLayout();
	}
	
 	protected function _toHtml()
    {
    
    	$raaHelper = Mage::helper('retail_analytics');
         if (!$raaHelper->isModuleEnabled()) {
            return '';
        } 
        
        $isonline = $raaHelper->getRaaConfig('isonline');
        if ($isonline == "" || $isonline == "true") {
        	return '';
        }
        
       
        $isrecentlyviewed = $raaHelper->getRaaConfig('isrecentlyviewed');        
        if ($isrecentlyviewed != "false") {
        	$isrecentlyviewedbyid = $raaHelper->getRaaConfig('isrecentlyviewedbyip');
        	if($isrecentlyviewedbyid != "false") {
        		Mage::helper('retail_analytics/recentlyviewed')->saveRecentlyViewed($this->getProduct()->getId());
        	}
        	elseif($isrecentlyviewedbyid == "false" && Mage::getSingleton('customer/session')->isLoggedIn()) {
        		Mage::helper('retail_analytics/recentlyviewed')->saveRecentlyViewed($this->getProduct()->getId());
        	}        	
        }
      
        return parent::_toHtml();
    }			
    
    /**
     * Return the id of the element. If none is defined in the layout xml, then set a default one.
     *
     * @return string
     */
    public function getElementId()
    {
    	return $this->getDivId();
    }
}
