<?php
/**
 */
class Retail_Analytics_Block_Cart extends Mage_Checkout_Block_Cart_Abstract
{
    /**
     * Cached items in cart.
     *
     * @var Mage_Sales_Model_Quote_Item[]
     */
    protected $_items;

    /**
     * Render shopping cart content as hidden meta data if the module is enabled for the current store.
     *
     * @return string
     */
    protected function _toHtml()
    {
    	$raaHelper = Mage::helper('retail_analytics');
        if (!$raaHelper->isModuleEnabled() || count($this->getItems()) === 0) {
            return '';
        }
        
        /* $isonline = $raaHelper->getRaaConfig('isonline');
        if ($isonline == "false") {
        	return '';
        } */
        
        return parent::_toHtml();
    }

    /**
     * Returns all visible cart items. If it is a bundle product with dynamic price settings, we get it's products
     * and return them. Fixed price bundle is not supported.
     *
     * @return Mage_Sales_Model_Quote_Item[]
     */
    public function getItems()
    {
        if (!$this->_items) {
            $items = array();
            /** @var $visibleItems Mage_Sales_Model_Quote_Item[] */
            $visibleItems = parent::getItems();
            foreach ($visibleItems as $item) {
                $product = $item->getProduct();
                if ($product->getTypeId() === Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
                    if ((int)$product->getPriceType() === Mage_Bundle_Model_Product_Price::PRICE_TYPE_FIXED) {
                        continue;
                    }
                    $items = array_merge($items, $item->getChildren());
                } else {
                    $items[] = $item;
                }
            }

            $this->_items = $items;
        }

        return $this->_items;
    }

    /**
     * Returns the product id for a quote item.
     * If the product type is "grouped", then return the grouped product's id and not the id of the actual product.
     *
     * @param Mage_Sales_Model_Quote_Item $item
     *
     * @return int
     */
    public function getProductId($item)
    {
        switch ($item->getProductType()) {
            case Mage_Catalog_Model_Product_Type::TYPE_GROUPED:
                $option = $item->getOptionByCode('product_type');
                if ($option !== null) {
                    $productId = $option->getProductId();
                } else {
                    $productId = $item->getProductId();
                }
                break;

            default:
                $productId = $item->getProductId();
                break;
        }

        return (int)$productId;
    } 
	
    public function getProductSku($item)
    {
    	$product_id=$this->getProductId($item);
    	$data_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id',$product_id)->addAttributeToSelect ( "*" );
    	if ( $data_collection->count() > 0) {
    		return $data_collection->getFirstItem()->getSku();
    
    	}
    }
    
	public function getImageURL($item)
    {
    	 $product_id=$this->getProductId($item);	
    	 $data_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id',$product_id)->addAttributeToSelect ( "*" );
    	 foreach ( $data_collection as $product ) {
    	 	if(!$product->getImage() || $product->getImage() == 'no_selection')
    	 	{
    	 		return  $product->getImageUrl();
    	  	}
    	 	else{
    	 		return $product->getMediaConfig()->getMediaUrl($product->getImage());
    	 	}
    	 }
    }
	
	public function getRaaImageURL($item)
    {
		$product_id=$this->getProductId($item);
    	//$product = Mage::getModel( 'catalog/product' )->load($product_id)->addAttributeToSelect ( "*" );
		$data_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id',$product_id)->addAttributeToSelect ( "*" );
		foreach ( $data_collection as $product ) {
			$product->getData();
			$SmallImage =  $product->getSmallImage();
			if($SmallImage == null || $SmallImage == "" || $SmallImage == 'no_selection' )
			{
				$Image = $product->getImage();
				$thumnailImage = $product->getThumbnail();
				if($Image!=null && $Image!="" && $Image != 'no_selection')
					return $product->getMediaConfig()->getMediaUrl($Image);
				else if($thumnailImage!=null && $thumnailImage!="" && $thumnailImage != 'no_selection')
					return $product->getMediaConfig()->getMediaUrl($thumnailImage);
				else
					return  $product->getImageUrl();
			}
			else
			{
				return $product->getMediaConfig()->getMediaUrl($SmallImage);
			}
		}
    }
}
