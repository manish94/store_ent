<?php
/** 
 */
class Retail_Analytics_Block_Embed extends Mage_Core_Block_Template
{
    /**
     * Render JavaScript that handles the data gathering and displaying of recommended products
     * if the module is enabled for the current store.
     *
     * @return string
     */
    protected function _toHtml()
    {
    	$raaHelper = Mage::helper('retail_analytics');
        if (!$raaHelper->isModuleEnabled()) {        	
            return '';
        }
        
       /*  $isonline = $raaHelper->getRaaConfig('isonline');      
        if ($isonline == "false") {
        	return '';
        } */
        
        return parent::_toHtml();
    }
}
