<?php
/** 
 */
class Retail_Analytics_Block_Button2 extends Mage_Adminhtml_Block_System_Config_Form_Field
{
 protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $url = $this->getUrl("retail_analytics/index/checkServer");

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setType('button')
                    ->setClass('scalable')
                    ->setLabel('Validate Server Account')
                    ->setOnClick("setLocation('$url')")
                    ->toHtml();

        return $html;
    }
}
?>
