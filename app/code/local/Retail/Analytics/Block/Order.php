<?php
/** 
 */
class Retail_Analytics_Block_Order extends Mage_Checkout_Block_Success
{
	
    /**
     * Render order info as hidden meta data if the module is enabled for the current store.
     *
     * @return string
     */
    protected function _toHtml()
    {
    	$raaHelper = Mage::helper('retail_analytics');
    	
        if (!$raaHelper->isModuleEnabled()) {
            return '';
        }

       /*  $isonline = $raaHelper->getRaaConfig('isonline');
        if ($isonline == "false" || $isonline == false) {
        	return '';
        } */
        
        return parent::_toHtml();
    }

    /**
     * Return the last placed order for the customer.
     *
     * @return Mage_Sales_Model_Order
     */
    public function getLastOrder()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        return Mage::getModel('sales/order')->load($orderId);
    }

    /**
     * Returns an array of generic data objects for all ordered items.
     * The list includes possible discount and shipping cost as separate items.
     *
     * Structure:
     * array({
     *     productId: 1,
     *     quantity: 1,
     *     name: foo,
     *     price: 2.00
     * }, {...});
     *
     * @param Mage_Sales_Model_Order $order
     *
     * @return object[]
     */
    public function getOrderItems($order)
    {
        $items = array();

        /** @var $visibleItems Mage_Sales_Model_Order_Item[] */
        $visibleItems = $order->getAllItems();
        foreach ($visibleItems as $visibleItem) {
            $product = Mage::getModel('catalog/product')->load($visibleItem->getProductId());
            if ($product->getTypeId() === Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
                if ((int)$product->getPriceType() === Mage_Bundle_Model_Product_Price::PRICE_TYPE_FIXED) {
                    continue;
                }
                $children = $visibleItem->getChildrenItems();
                foreach ($children as $child) {
                    $items[] = $this->_orderModelToItem($child);
                }
            } else {
                $items[] = $this->_orderModelToItem($visibleItem);
            }
            
            //$product->setSku($product->getSku())->save();
            
        }

       /*  if (!empty($items)) {
            $items = array_merge($items, $this->_getOrderSpecialItems($order));
        } */

        return $items;
    }

    /**
     * Converts a order item model into a generic data object. 
     * @param Mage_Sales_Model_Order_Item $model
     *
     * @return object
     */
    protected function _orderModelToItem($model)
    {
        return (object)array(
            'productId' => $this->getProductId($model),
            'quantity'  => (int)$model->getQtyOrdered(),
            'name'      => $model->getName(),
        	'sku'      => $model->getSku(),
        	'price' => $model->getPriceInclTax(),
        	'discount' =>  $model->getDiscountAmount(),
        	'producttypeid' =>	$model->getProductType()
        );
    }

    /**
     * Returns an array of generic data objects for discount and shipping from the order.
    
     * @param Mage_Sales_Model_Order $order
     *
     * @return object[]
     */
    protected function _getOrderSpecialItems($order)
    {
        $items = array();

        if ($discount = $order->getDiscountAmount()) {
            $items[] = (object)array(
                'productId' => -1,
                'quantity'  => 1,
                'name'      => 'Discount',
            	'sku'   => '',
                'price' => $discount,
            	'discount' =>0,
            	'producttypeid' => 'discount'
            		
            );
        }

        if ($shippingInclTax = $order->getShippingInclTax()) {
            $items[] = (object)array(
                'productId' => -1,
                'quantity'  => 1,
                'name'      => 'Shipping and handling',
            	'sku'   => '',
                'price' => $shippingInclTax,
            	'discount' =>0,
            	'producttypeid' => 'shipping'
            );
        }

        return $items;
    }

    /**
     * Returns the product id for a order item.
     * If the product type is "grouped", then return the grouped product's id and not the id of the actual product.
     *
     * @param Mage_Sales_Model_Order_Item $item
     *
     * @return int
     */
    public function getProductId($item)
    {
        switch ($item->getProductType()) {
            case Mage_Catalog_Model_Product_Type::TYPE_GROUPED:
                $info = $item->getProductOptionByCode('info_buyRequest');
                if ($info !== null && isset($info['super_product_config']['product_id'])) {
                    $productId = $info['super_product_config']['product_id'];
                } else {
                    $productId = $item->getProductId();
                }
                break;

            default:
                $productId = $item->getProductId();
                break;
        }

        return (int)$productId;
    }
    
    public function getSimpleProducts($product)
    {    	
    	$conf = Mage::getModel('catalog/product_type_configurable')->setProduct($product);
    	$simple_collection = $conf->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();
    	foreach($simple_collection as $simple_product){
    		$data[] = array('value'=>$simple_product->getId(), 'code'=>'simple_product_id');
    	}    	
    	return $data;
    }
   
    
    public function get_category_name($productId) {
    	$product = Mage::getModel('catalog/product')->load($productId);
    	$category_name = "" ;
    	$cats = $product->getCategoryIds();
    
    	$cnt = 0 ;
    	foreach ($cats as $category_id) {
    		$_cat = Mage::getModel('catalog/category')->load($category_id) ;
    		$cnt++ ;
    		if($cnt == count($cats))
    			$category_name.=$_cat->getName() ;
    		else
    			$category_name.=$_cat->getName()."," ;
    	}
    	return $category_name ;
    }
    
    public function getOrderTransaction($order,$order_incrementId)
    {   	
    	$data = array();
    	
    	$data['id'] = $order_incrementId;
    	$data['affiliation'] = $this->jsQuoteEscape(Mage::app()->getStore()->getFrontendName());
    	$data['revenue'] = $order->getBaseGrandTotal();
    	$data['shipping'] = $order->getBaseShippingAmount();
    	$data['tax'] = $order->getBaseTaxAmount();  
    	 	
    	return json_encode ( $data );
    }
    
    public function getOrderItemTransaction($item,$order_incrementId)
    {       	
    	
    	return <<<HTML
ga('ecommerce:addItem', {
'id': '$order_incrementId',
'name': '{$this->jsQuoteEscape($item->getName())}',
'sku': '{$this->jsQuoteEscape($item->getSku())}',
'category': '{$this->jsQuoteEscape($this->get_category_name($item->getProductId()))}',
'price': '{$item->getBasePrice()}',
'quantity': '{$item->getQtyOrdered()}'   			
});
HTML;
    	
    }    
      
}
