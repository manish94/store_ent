<?php

$installer = $this;
$installer->startSetup();

$tableProductMap = $installer->getConnection()->newTable($installer->getTable('retail_analytics/productmap'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
        ), 'ID')
    ->addColumn('productid', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
        'nullable' => true,
        ), 'Productid')
    ->addColumn('map', Varien_Db_Ddl_Table::TYPE_VARCHAR,255, null, array(
        'nullable' => true,
        ), 'Map')
    ->addColumn('categoryid', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
        		'nullable' => true,
        ), 'Categoryid')
     ->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        		), 'Created')
   	->addColumn('modified',  Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(        		
       ), 'Modified')
    ->addIndex(
        $installer->getIdxName(
            $installer->getTable('retail_analytics/productmap'),
            array('productid'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array('productid'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
      ->addIndex(
        $installer->getIdxName(
        	$installer->getTable('retail_analytics/productmap'),
        	array('map'),
        	Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
        ),
        array('map'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))        
    ->setComment('retail analytics/productmap entity table');
$installer->getConnection()->createTable($tableProductMap); 

$tableProductReco = $installer->getConnection()->newTable($installer->getTable('retail_analytics/productreco'))
->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned' => true,
		'nullable' => false,
		'primary' => true,
		'identity' => true,
), 'ID')
->addColumn('productmap', Varien_Db_Ddl_Table::TYPE_VARCHAR,255, null, array(
		'nullable' => true,
), 'Productmap')
->addColumn('map', Varien_Db_Ddl_Table::TYPE_VARCHAR,255, null, array(
		'nullable' => true,
), 'Map')
->addColumn('weight', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,10',null, array(
		'nullable'  => true		
), 'Weight')
->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Created')
->addColumn('modified',  Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Modified')
->addIndex(
		$installer->getIdxName(
				$installer->getTable('retail_analytics/productreco'),
				array('productmap'),
				Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
		),
		array('productmap'),
		array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))
->setComment('retail analytics/productreco entity table');
$installer->getConnection()->createTable($tableProductReco);

$tableCustomerReco = $installer->getConnection()->newTable($installer->getTable('retail_analytics/customerreco'))
->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned' => true,
		'nullable' => false,
		'primary' => true,
		'identity' => true,
), 'ID')
->addColumn('customerid', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Customerid')
->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
		'nullable' => true,
), 'Email')
->addColumn('map', Varien_Db_Ddl_Table::TYPE_VARCHAR,255, null, array(
		'nullable' => true,
), 'Map')
->addColumn('weight', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,10',null, array(
		'nullable'  => true
), 'Weight')
->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Created')
->addColumn('modified',  Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Modified')
->addIndex(
		$installer->getIdxName(
				$installer->getTable('retail_analytics/customerreco'),
				array('email'),
				Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
		),
		array('email'),
		array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))
->setComment('retail analytics/customerreco entity table');
$installer->getConnection()->createTable($tableCustomerReco);


$tableCustomerNonReco = $installer->getConnection()->newTable($installer->getTable('retail_analytics/customernonreco'))
->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned' => true,
		'nullable' => false,
		'primary' => true,
		'identity' => true,
), 'ID')
->addColumn('customerid', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Customerid')
->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
		'nullable' => true,
), 'Email')
->addColumn('map', Varien_Db_Ddl_Table::TYPE_VARCHAR,255, null, array(
		'nullable' => true,
), 'Map')
->addColumn('weight', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,10',null, array(
		'nullable'  => true
), 'Weight')
->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Created')
->addColumn('modified',  Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Modified')
->addIndex(
		$installer->getIdxName(
				$installer->getTable('retail_analytics/customernonreco'),
				array('email'),
				Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
		),
		array('email'),
		array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))
->setComment('retail analytics/customernonreco entity table');
$installer->getConnection()->createTable($tableCustomerNonReco);



$tableRecentlyViewed = $installer->getConnection()->newTable($installer->getTable('retail_analytics/recentlyviewed'))
->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned' => true,
		'nullable' => false,
		'primary' => true,
		'identity' => true,
), 'ID')
->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
		'nullable' => true,
), 'Email')
->addColumn('customerid', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Customerid')
->addColumn('productid', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Productid')
->addColumn('ip', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Ip')
->addColumn('internalip', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Internalip')
->addColumn('ras', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Ras')
->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Status')
->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Created')
->addColumn('modified',  Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Modified')
->addIndex(
		$installer->getIdxName(
				$installer->getTable('retail_analytics/recentlyviewed'),
				array('email'),
				Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
		),
		array('email'),
		array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))
->addIndex(
		$installer->getIdxName(
				$installer->getTable('retail_analytics/recentlyviewed'),
				array('ip'),
				Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
		),
		array('ip'),
		array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))
->addIndex(
		$installer->getIdxName(
				$installer->getTable('retail_analytics/recentlyviewed'),
				array('ip','internalip'),
				Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
		),
		array('ip','internalip'),
		array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))				
->setComment('retail analytics/recentlyviewed entity table');
$installer->getConnection()->createTable($tableRecentlyViewed);


$tablePageView = $installer->getConnection()->newTable($installer->getTable('retail_analytics/pageview'))
->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned' => true,
		'nullable' => false,
		'primary' => true,
		'identity' => true,
), 'ID')
->addColumn('pageid', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable' => true,
), 'Pageid')
->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
		'nullable' => true,
), 'Title')
->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Status')
->addColumn('viewedtotal', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable' => true,
), 'Viewedtotal')
->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Created')
->setComment('retail analytics/pageview entity table');
$installer->getConnection()->createTable($tablePageView);


$tableOnsiteConfig = $installer->getConnection()->newTable($installer->getTable('retail_analytics/onsiteconfig'))
->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned' => true,
		'nullable' => false,
		'primary' => true,
		'identity' => true,
), 'ID')
->addColumn('slotid', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
		'nullable' => true,
), 'Slotid')
->addColumn('slotname', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
		'nullable' => true,
), 'Slotname')
->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
		'nullable' => true,
), 'Title')
->addColumn('ras', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Ras')
->addColumn('priority', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable' => true,
), 'Priority')
->addColumn('noofproduct', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable' => true,
), 'Noofproduct')
->addColumn('pageid', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable' => true,
), 'Pageid')
->addColumn('pbrurl', Varien_Db_Ddl_Table::TYPE_VARCHAR,255, null, array(
		'nullable' => true,
), 'Pbrurl')
->addColumn('ipenable', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
		'nullable' => true,
), 'Ipenable')
->addColumn('timeduration', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable' => true,
), 'Timeduration')
->addColumn('enable', Varien_Db_Ddl_Table::TYPE_BOOLEAN, true, array(
		'nullable' => true,
), 'Enable')
->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Created')
->addColumn('modified',  Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Modified')
->setComment('retail analytics/onsiteconfig entity table');
$installer->getConnection()->createTable($tableOnsiteConfig);

if (!$installer->getConnection()->isTableExists($installer->getTable('retail_analytics/raaconfig'))) {
	$tableRaaConfig = $installer->getConnection()->newTable($installer->getTable('retail_analytics/raaconfig'))
	->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
			'unsigned' => true,
			'nullable' => false,
			'primary' => true,
			'identity' => true,
	), 'ID')
	->addColumn('raakey', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
			'nullable' => true,
	), 'Raakey')
	->addColumn('raavalue', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
			'nullable' => true,
	), 'Raavalue')
	->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
	), 'Created')
	->setComment('retail analytics/raaconfig entity table');
	$installer->getConnection()->createTable($tableRaaConfig);
}
	
$tableRecooffer = $installer->getConnection()->newTable($installer->getTable('retail_analytics/recooffer'))
->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned' => true,
		'nullable' => false,
		'primary' => true,
		'identity' => true,
), 'ID')
->addColumn('customerid', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Customerid')
->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
		'nullable' => true,
), 'Email')
->addColumn('productid', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Productid')
->addColumn('couponcode', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Couponcode')
->addColumn('validfrom', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Validfrom')
->addColumn('validto', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Validto')
->addColumn('price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,3',null,  array(
		'nullable' => true,
), 'Price')
->addColumn('offerprice', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,3',null, array(
		'nullable' => true,
), 'Offerprice')
->addColumn('campaign', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Campaign')
->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, 'Active', array(
		'nullable' => true,
), 'Status')
->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Created')
->setComment('retail analytics/coupon entity table');
$installer->getConnection()->createTable($tableRecooffer);

$tableAbandonedcart = $installer->getConnection()->newTable($installer->getTable('retail_analytics/abandonedcart'))
->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned' => true,
		'nullable' => false,
		'primary' => true,
		'identity' => true,
), 'ID')
->addColumn('customerid', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Customerid')
->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
		'nullable' => true,
), 'Email')
->addColumn('productid', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Productid')
->addColumn('ras', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Ras')
->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Status')
->addColumn('quantity', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable' => true,
), 'Quantity')
->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Created')
->addColumn('modified', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Modified')
->setComment('retail analytics/abandonedcart entity table');
$installer->getConnection()->createTable($tableAbandonedcart);


$installer->endSetup();