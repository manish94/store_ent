<?php
 
$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/productmap'),'incrementalid', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Incrementalid'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/productmap'),'etc', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Etc'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/productreco'),'incrementalid', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Incrementalid'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/productreco'),'etc', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Etc'
));


$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/customerreco'),'incrementalid', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Incrementalid'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/customerreco'),'etc', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Etc'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/customernonreco'),'incrementalid', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Incrementalid'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/customernonreco'),'etc', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Etc'
));

$installer->endSetup();


$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/recentlyviewed'),'incrementalid', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Incrementalid'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/recentlyviewed'),'etc', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Etc'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/recentlyviewed'),'storecurrencycode', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Storecurrencycode'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/pageview'),'incrementalid', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Incrementalid'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/pageview'),'etc', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Etc'
));


$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/onsiteconfig'),'incrementalid', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Incrementalid'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/onsiteconfig'),'etc', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Etc'
));


$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/recooffer'),'incrementalid', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Incrementalid'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/recooffer'),'etc', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Etc'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/abandonedcart'),'incrementalid', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Incrementalid'
));

$installer->getConnection()
->addColumn($installer->getTable('retail_analytics/abandonedcart'),'etc', array(
		'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
		'nullable'  => true,
		'length'    => 255,
		'default' => '',
		'comment'   => 'Etc'
));

$installer->endSetup();

