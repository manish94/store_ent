<?php

$installer = $this;
$installer->startSetup();

$tableRaaConfig = $installer->getConnection()->newTable($installer->getTable('retail_analytics/raaconfig'))
->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned' => true,
		'nullable' => false,
		'primary' => true,
		'identity' => true,
), 'ID')
->addColumn('raakey', Varien_Db_Ddl_Table::TYPE_VARCHAR,100, null, array(
		'nullable' => true,
), 'Raakey')
->addColumn('raavalue', Varien_Db_Ddl_Table::TYPE_VARCHAR,200, null, array(
		'nullable' => true,
), 'Raavalue')
->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Created')
->setComment('retail analytics/raaconfig entity table');
$installer->getConnection()->createTable($tableRaaConfig);