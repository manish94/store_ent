<?php

class Retail_Analytics_Model_Config_Customer extends Mage_Core_Model_Config_Data
{
	
	public function toOptionArray()
	{
		return array(				
				array('value'=>'0', 'label'=>'Select'),
				array('value'=>'1', 'label'=>'All'),
				array('value'=>'2', 'label'=>'Reco Self'),
				array('value'=>'3', 'label'=>'Reco Noneself')				
		);
	}
	 /**
     * Save object data.
     * Validates that the account is set.
     *
     * @return Nosto_Tagging_Model_Config_Account
     */
    public function save()
    {
        $customer = $this->getValue();
        /* if (empty($customer)) {
            Mage::throwException(Mage::helper('retail_analytics')->__('Show customer recommendations is required.'));
        } */

        return parent::save();
    }
}
