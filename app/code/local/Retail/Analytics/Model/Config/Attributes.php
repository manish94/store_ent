<?php

class Retail_Analytics_Model_Config_Attributes extends Mage_Core_Model_Config_Data
{
	
	public function toOptionArray()
	{
		$attributesArray = array();
		
		$attributes = Mage::getResourceModel('catalog/product_attribute_collection')->getItems();		
		foreach ($attributes as $attribute){
			
			$attributeCode = $attribute->getAttributecode();
			$attributeLabel = $attribute->getFrontendLabel();	
			if($attributeLabel !="" && $attributeLabel != null)
			{	
				$attributesArray[] = array('value'=>$attributeCode, 'label'=>$attributeLabel);
			}
		}		
		return $attributesArray;	
	}
	 /**
     * Save object data.
     * Validates that the account is set.
     *
     * @return Nosto_Tagging_Model_Config_Account
     */
    public function save()
    {
        $attributes = $this->getValue();
       /*  if (empty($product)) {
            Mage::throwException(Mage::helper('retail_analytics')->__('Show product recommendations is required.'));
        } */

        return parent::save();
    }
}
