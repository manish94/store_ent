<?php

class Retail_Analytics_Model_Config_Account extends Mage_Core_Model_Config_Data
{
    /**
     * Save object data.
     * Validates that the account is set.
     *
     * @return Nosto_Tagging_Model_Config_Account
     */
    public function save()
    {
        $account = $this->getValue();
        if (empty($account)) {
            Mage::throwException(Mage::helper('retail_analytics')->__('Account id is required.'));
        }

        return parent::save();
    }
}
