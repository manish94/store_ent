<?php

class Retail_Analytics_Model_Config_Server2 extends Mage_Core_Model_Config_Data
{
	/**
	 * Save object data.
	 * Validates that the server is set and does not include the protocol.
	 *
	 * @return Nosto_Tagging_Model_Config_Server
	 */
	public function save()
	{
		$server = $this->getValue();

		if (empty($server)) {
			Mage::throwException(Mage::helper('retail_analytics')->__('Secure Server URL is required.'));
		}
		return parent::save();
	}
}
