<?php
/**
 * Magento 
 */
class Retail_Analytics_Model_Observer {
	
	// cart events.
	public function addItemInCart($observer) {
		try {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			if ($raaHelper->isModuleEnabled ()) {
				// Mage::log ( ' add to cart. ');
				$data = array (
						"ip" => $raaHelper->getClientIP (),
						"date" => date ( now () ) 
				);
				if (Mage::getSingleton ( 'customer/session' )->isLoggedIn ()) {
					
					$customer = Mage::getSingleton ( 'customer/session' )->getCustomer ();
					$custkey = ($customer->getCustkey () != NULL && $customer->getCustkey () != "") ? $customer->getCustkey () : $customer->getId ();
					
					$customerArray = array ();
					$customerArray ["entity_id"] = $customer->getId ();
					$customerArray ["customer_id"] = $customer->getId ();
					$customerArray ["email"] = $customer->getEmail ();
					$customerArray ["firstname"] = $customer->getFirstname ();
					$customerArray ["lastname"] = $customer->getLastname ();
					$customerArray ["phone"] = $customer->getPhone ();
					// Mage::log ( ' customer: ' . Mage::helper ( 'core' )->jsonEncode( $customerArray) );
					$data ["customer"] = $customerArray;
					
					$product = $observer->getEvent ()->getQuoteItem ()->getProduct ();
					$raaPriceHelper = Mage::helper ( 'retail_analytics/price' );
					$itemArray = array ();
					$description = ($product->getDescription () == '') ? $product->getName () : $product->getDescription ();
					$itemArray ["entity_id"] = $product->getId ();
					$itemArray ["product_id"] = $product->getId ();
					$itemArray ["sku"] = $product->getSku ();
					$itemArray ["quantity"] = $product->getQty ();
					$itemArray ["name"] = $product->getName ();
					$itemArray ["description"] = $description;
					$itemArray ["price"] = $raaPriceHelper->getFormattedPrice ( $product->getPrice () );
					$itemArray ["url"] = $product->getProductUrl ();
					$itemArray ["imageurl"] = $product->getThumbnailUrl ();
					
					$categoryIds = $product->getCategoryIds ();
					$itemArray ["category_id"] = (isset ( $categoryIds [0] ) ? $categoryIds [count ( $categoryIds ) - 1] : 0);
					$parentIds = Mage::getResourceSingleton ( 'catalog/product_type_configurable' )->getParentIdsByChild ( $product->getId () );
					if ($parentIds != null && count ( $parentIds ) > 0) {
						$tempParentId = $parentIds [0];
						$itemArray ["parent_id"] = $tempParentId;
						$tempProduct = Mage::getModel ( 'catalog/product' )->load ( $tempParentId );
						$itemArray ["url"] = $tempProduct->getProductUrl ();
						if ($tempProduct->getImage () == 'no_selection') {
							$itemArray ["imageurl"] = $tempProduct->getImageUrl ();
						} else {
							$itemArray ["imageurl"] = $tempProduct->getMediaConfig ()->getMediaUrl ( $tempProduct->getImage () );
						}
					}
					$item = $observer->getQuoteItem ();
					$specialPrice = $item->getOriginalPrice ();
					Mage::log ( ' $specialPrice: ' . $specialPrice );
					$data ["cart"] = $itemArray;
					$this->sendData ( "/api/rest/insert/", $data );
				}
			}
		} catch ( Exception $e ) {
			Mage::log ( 'Error at addItemInCart: ' . $e->getMessage () );
		}
		return true;
	}	
	public function removeItemFromCart($observer) {
		/*
		 * $cartItem = $observer->getEvent()->getQuoteItem(); $itemId = $cartItem->getSku(); Mage::log($itemId .' item removed from cart.');
		 */
		try {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			if ($raaHelper->isModuleEnabled ()) {
				// Mage::log ( ' remove to cart. ');
				$data = array (
						"ip" => $raaHelper->getClientIP (),
						"date" => date ( now () ) 
				);
				
				if (Mage::getSingleton ( 'customer/session' )->isLoggedIn ()) {
					$customer = Mage::getSingleton ( 'customer/session' )->getCustomer ();
					$customerArray = array ();
					$customerArray ["entity_id"] = $customer->getId ();
					$customerArray ["customer_id"] = $customer->getId ();
					$customerArray ["email"] = $customer->getEmail ();
					$customerArray ["firstname"] = $customer->getFirstname ();
					$customerArray ["lastname"] = $customer->getLastname ();
					$customerArray ["phone"] = $customer->getPhone ();
					$data ["customer"] = $customerArray;
					
					$itemArray = array ();
					$product = $observer->getEvent ()->getQuoteItem ()->getProduct ();
					$itemArray ["entity_id"] = $product->getId ();
					$itemArray ["product_id"] = $product->getId ();
					$data ["cart"] = $itemArray;
					$this->sendData ( "/api/rest/remove/", $data );
				}
			}
		} catch ( Exception $e ) {
			Mage::log ( 'Error at removeItemFromCart: ' . $e->getMessage () );
		}
		return true;
	}
	public function updateCart($observer) {
		try {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			if ($raaHelper->isModuleEnabled ()) {
				
				$data = array (
						"ip" => $raaHelper->getClientIP (),
						"date" => date ( now () ) 
				);
				
				if (Mage::getSingleton ( 'customer/session' )->isLoggedIn ()) {
					$customer = Mage::getSingleton ( 'customer/session' )->getCustomer ();
					
					$customerArray = array ();
					$customerArray ["entity_id"] = $customer->getId ();
					$customerArray ["customer_id"] = $customer->getId ();
					$customerArray ["email"] = $customer->getEmail ();
					$customerArray ["firstname"] = $customer->getFirstname ();
					$customerArray ["lastname"] = $customer->getLastname ();
					$customerArray ["phone"] = $customer->getPhone ();
					$data ["customer"] = $customerArray;
					
					$cartArray = array ();
					$cartHelper = Mage::helper ( 'checkout/cart' );
					$items = $cartHelper->getCart ()->getItems ();
					foreach ( $items as $item ) {
						$itemArray = array ();
						$itemArray ["entity_id"] = $item->getProduct ()->getId ();
						$itemArray ["product_id"] = $item->getProduct ()->getId ();
						$itemArray ["quantity"] = $item->getQty ();
						array_push ( $cartArray, $itemArray );
					}
					$data ["cart"] = $cartArray;
					$this->sendData ( "/api/rest/update/", $data );
				}
			}
		} catch ( Exception $e ) {
			Mage::log ( 'Error at updateCart : ' . $e->getMessage () );
		}
		return true;
	}
	public function clearCart($observer) {
		try {
			$post = Mage::app ()->getRequest ()->getPost ( 'update_cart_action' ); // get value
			if ($post == 'empty_cart') {
				// Mage::log ( ' clear cart ');
				$raaHelper = Mage::helper ( 'retail_analytics' );
				if ($raaHelper->isModuleEnabled ()) {
					
					$data = array (
							"ip" => $raaHelper->getClientIP (),
							"date" => date ( now () ) 
					);
					
					if (Mage::getSingleton ( 'customer/session' )->isLoggedIn ()) {
						$customer = Mage::getSingleton ( 'customer/session' )->getCustomer ();
						
						$customerArray = array ();
						$customerArray ["entity_id"] = $customer->getId ();
						$customerArray ["customer_id"] = $customer->getId ();
						$customerArray ["email"] = $customer->getEmail ();
						$customerArray ["firstname"] = $customer->getFirstname ();
						$customerArray ["lastname"] = $customer->getLastname ();
						$customerArray ["phone"] = $customer->getPhone ();
						$data ["customer"] = $customerArray;
						
						$this->sendData ( "/api/rest/clear/", $data );
					}
				}
			}
		} catch ( Exception $e ) {
			Mage::log ( 'Error at clearCart : ' . $e->getMessage () );
		}
		return true;
	}
		
	
	public function getObserverProduct($id) {
		$productArray = array ();
		try {				
				$raaHelper = Mage::helper ( 'retail_analytics' );								
				$product = Mage::getModel ( 'catalog/product' )->load ($id);
				$productArray ["product_id"] = $id;
				$productArray ["sku"] = $product->getSku ();
				$productArray ['price'] = $product->getPrice();
				$productArray ['specialprice'] = $product->getFinalPrice ();							
				$discountPercent = 0;
				if ( $productArray ["special_price"] != $productArray ['price']) {
					$discountPercent = (($productArray ['price'] - $productArray ["special_price"]) * 100)/$productArray ['price'];
				}				
				$productArray ["discount_percent"] = $discountPercent;
				
				
				$productArray ["name"] = $product->getName ();
				$productArray ['url'] = $product->getProductUrl (); 
				
				//$width = $raaHelper->getImageWidth();
				//$height = $raaHelper->getImageHeight();
				//$productArray ['imageurl'] = $product->getImageUrl();
				//$productArray ['cacheimageurl'] = (string)Mage::helper('catalog/image')->init($product, "small_image")->resize($width,$height);
			
				$productArray ['status'] = $product->getAttributeText("status");
				$productArray ['visibility'] =  $product->getAttributeText("visibility");
				$productArray ['instock'] = $product->getIsInStock ();
				$productArray ["quantity"] = $product->getStockItem ()->getQty ();			
								
				
				
				return $productArray;
				
		} catch ( Exception $e ) {
			Mage::log ( 'Error at Observer class method getObserverProduct  : ' . $e->getMessage () );
			return $productArray;
		}
	
	}
	
	//product events.
	public function changeProductStockInventory($observer) {
		try {
			
			$raaHelper = Mage::helper ( 'retail_analytics' );
			if ($raaHelper->isModuleEnabled ()) {
				$event = $observer->getEvent();
				$_item = $event->getItem();
				$data = $this->getObserverProduct($_item->getProductId());
				$this->httpRequest("/api/rest/update/product/fields/",$data);				
			}
			
			return true;
		} catch ( Exception $e ) {
			Mage::log ( 'Error at changeProductStockInventory Observer : ' . $e->getMessage () );
			return true;
		}	
	}	
	
	//product events.
	public function catalogProductImportFinishBefore($observer) {
		try {
				
			$raaHelper = Mage::helper ( 'retail_analytics' );
			if ($raaHelper->isModuleEnabled ()) {
				$event = $observer->getEvent();
	
				Mage::log (" catalog_product_import_finish_before : " . Mage::helper ( 'core' )->jsonEncode( $event) );
				
			   
			}
				
			return true;
		} catch ( Exception $e ) {
			Mage::log ( 'Error at changeProductStockInventory Observer : ' . $e->getMessage () );
			return true;
		}
	}
	
	
	public function viewProductAfter($observer) {
		try {		
								
				$product = Mage::registry('current_product');
				if ($product instanceof Mage_Catalog_Model_Product){					
					$raaHelper = Mage::helper ( 'retail_analytics' );
					if ($raaHelper->isModuleEnabled ()) {								
							$productArray = array ();				
							$raaHelper = Mage::helper ( 'retail_analytics' );							     
							//$product = Mage::getModel ( 'catalog/product' )->load ($id);					
							$productArray ["entity_id"] = $product->getId();
							$productArray ["product_id"] = $product->getId();
							$productArray ["sku"] = $product->getSku ();
							$productArray ["name"] = $product->getName ();
							$productArray ['url'] = $product->getProductUrl ();
						
							$width = $raaHelper->getImageWidth();
							$height = $raaHelper->getImageHeight();
							$productArray ['imageurl'] = $product->getImageUrl();
							$productArray ['cacheimageurl'] = (string)Mage::helper('catalog/image')->init($product, "small_image")->resize($width,$height);
						
							$productArray ["quantity"] = $product->getStockItem ()->getQty ();
							$productArray ['price'] =$product->getPrice();
							$productArray ['special_price'] = $product->getFinalPrice ();
							$productArray ['status'] = $product->getAttributeText("status");
							$productArray ['visibility'] =  $product->getAttributeText("visibility");
							$productArray ['instock'] = $product->getIsInStock ();
							$price = $product->getPrice ();
							$discountPercent = 0;
							if ( $productArray ["special_price"] != $price) {
								$discountPercent = (($price - $productArray ["special_price"]) * 100)/$price;
							}
						
							$productArray ["discount_percent"] = $discountPercent;		
							$this->httpRequest("/api/rest/insert/",$productArray); 
							
					}
				}
			return true;
		} catch ( Exception $e ) {
			Mage::log ( 'Error at viewProductAfter Observer : ' . $e->getMessage () );
			return true;
		}
	}
	
	// order events.
	public function placeOrderAfter($observer) {
		try {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			if ($raaHelper->isModuleEnabled ()) {
				$order = $observer->getEvent()->getOrder();
				$data = array ();	
				$currencyModel = Mage::getModel('directory/currency');
				$currencies = $currencyModel->getConfigAllowCurrencies();
				$baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
				$defaultCurrencies = $currencyModel->getConfigBaseCurrencies();
				$rates=$currencyModel->getCurrencyRates($defaultCurrencies, $currencies);
	
				
				
				$currencyArray = array ("base_currency_code"=>$order['base_currency_code'],
						"store_currency_code"=>$order['store_currency_code'],
						"rate_format"=>"MUL",
						"rates"=>$rates);
					
				$data ["currency"] = $currencyArray;
	
				
				$customerArray = array ();
				$customerArray ["customer_id"] = $order->getCustomerId();
				$customerArray ["email"] = $order->getCustomerEmail ();
				$customerArray ["firstname"] = $order->getCustomerFirstname ();
				$customerArray ["lastname"] = $order->getCustomerLastname ();			
				$data ["customer"] = $customerArray;
	
	
				$productsArray = array ();
				$productArray = array ();
				$productArray["product_id"] = "-2";
				$productArray["entity_id"] = $order->getId();
				$productArray["order_id"] = $order->getId();
				$productArray["increment_id"] = $order->getIncrementId();
				$productArray["sub_total"] = $order->getSubtotal();
				$productArray["grand_total"] = $order->getGrandTotal();
				$productArray["coupon_code"] = $order['coupon_code'];
				$productArray["status"] = $order->getStatus();			
				$productArray["created_at"] =  $order->getCreatedAt();
				$productArray["store_id"] = $order->getStoreId();
				$productsArray[] = $productArray;
	
				$ordered_items = $order->getAllItems();
				Foreach($ordered_items as $item){
					$productArray = array ();
					$productArray["product_id"] = $item->getProductId();
					$productArray["sku"] = $item->getSku();
					$productArray["name"] = $item->getName();
					$productArray["quantity"] = $item->getQtyOrdered();
					$productArray["price"] = $item->getPriceInclTax();
					$productArray["discount"] = $item->getDiscountAmount();
					$productArray["producttypeid"] = $item->getProductType();
					$productsArray[] = $productArray;
				}
	
				$infoArray = array ("order_id"=>$order->getId(),"increment_id"=>$order->getIncrementId(),"created_at"=>$order->getCreatedAt());
				$orderArray = array ("products"=>$productsArray,"info"=>$infoArray);
				$data ["order"] = $orderArray;
	
				$this->httpRequest("/api/rest/insert/",$data);
	
			}
				
			return true;
		} catch ( Exception $e ) {
			Mage::log ( 'Error at addItemInCart: ' . $e->getMessage () );
			return true;
		}
	
	}
	
	function httpRequest($url, $data) {		
		try {		
		if(function_exists('fsockopen')) {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			$host = $raaHelper->getServer ();
			//$host = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'www.retailautomata.com' : $host;
			$port = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? '8443' : '8080';
			$raa_services = $raaHelper->getRAAServicesName ();
			$raa_account = $raaHelper->getAccount ();
			$path = '/' . $raa_services . '/' . $url . $raa_account;		
			
			$paramStr = "";	
			$method = "POST";			
			if ($method == "GET") {
			foreach ($params as $name=> $val) {
					$paramStr .= $name . "=";
					$paramStr .= urlencode($val);
					$paramStr .= "&";
				}
				$path .= "?" . $paramStr;
			}
			else {
				$paramStr = "request=" . Mage::helper ( 'core' )->jsonEncode ( $data );
			}		
			
			// Create the connection
			$sock = fsockopen($host, $port);
			if($sock){
			fputs ( $sock, "$method $path HTTP/1.1\r\n" );
			fputs ( $sock, "Host: $host\r\n" );
			fputs ( $sock, "Content-type: " . "application/x-www-form-urlencoded\r\n" );
			if ($method == "POST") {
				fputs ( $sock, "Content-length: " . strlen ( $paramStr ) . "\r\n" );
			}
			fputs ( $sock, "Connection: close\r\n\r\n" );
			if ($method == "POST") {
				fputs ( $sock, $paramStr );
			}
			
			/*// Buffer the result
			$result = "";
			 while ( ! feof ( $sock ) ) {
				$result .= fgets ( $sock, 1024 );
			} */			
			$result = fgets ( $sock, 1024 );
			fclose ( $sock );	
			
			//Mage::log (" httpRequest path: ". $host . $path. ' data: ' . Mage::helper ( 'core' )->jsonEncode( $data) );
			//Mage::log (" httpRequest result: " . Mage::helper ( 'core' )->jsonEncode( $result) );
			
			}
		}			
		} catch ( Exception $e ) {
				Mage::log ( 'Error: ' . $e->getMessage () );
			}
		}
		
		public function sendData($url, $data) {
			try {
				$content = "request=" . Mage::helper ( 'core' )->jsonEncode ( $data );
				// Mage::log ( ' send data: ' . $content);
				$raaHelper = Mage::helper ( 'retail_analytics' );
				$raa_server = $raaHelper->getServer ();
				$raa_services = $raaHelper->getRAAServicesName ();
				$raa_account = $raaHelper->getAccount ();
				$server_url = $raa_server . '/' . $raa_services . '/' . $url . $raa_account;
				$curl = curl_init ( $server_url );
				curl_setopt ( $curl, CURLOPT_HEADER, false );
				curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
				curl_setopt ( $curl, CURLOPT_HTTPHEADER, array (
				"Content-type",
				"application/x-www-form-urlencoded"
						) );
						curl_setopt ( $curl, CURLOPT_POST, true );
						curl_setopt ( $curl, CURLOPT_POSTFIELDS, $content );
						curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
						$result = curl_exec ( $curl );
						$response = Mage::helper ( 'core' )->jsonDecode ( $result ); // json_decode($result);
						// Mage::log ( ' response: ' .$response);
						curl_close ( $curl );
			} catch ( Exception $e ) {
				Mage::log ( 'Error: ' . $e->getMessage () );
			}
		}
		
}
