<?php
class Retail_Analytics_Model_Pageview extends Mage_Core_Model_Abstract
{
	protected function _construct()
	{
		$this->_init('retail_analytics/pageview');
	}
}