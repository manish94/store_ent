<?php

class Retail_Analytics_Helper_Raaconfig extends Mage_Core_Helper_Abstract
{
		
	public function saveRaaconfig($key, $value) {
		
		if(is_array($value)) {
			//$value = Mage::helper( 'core' )->jsonDecode( $value );
			$value = serialize($value);
			//$unserialized_array = unserialize($serialized_array);
		}
		
		$raaconfig_collection = Mage::getModel('retail_analytics/raaconfig')->getCollection ()->addFieldToFilter ( 'raakey', array ($key) );
		if($raaconfig_collection->count() > 0) {
			$updateconfig = $raaconfig_collection->getFirstItem();
			$updateconfig->setRaakey($key);
			$updateconfig->setRaavalue($value);			
			$updateconfig->setCreated(now());
			$updateconfig->save();
		}	
		else 
		{	
			$raaconfig = Mage::getModel('retail_analytics/raaconfig');
			$raaconfig->setRaakey($key);
			$raaconfig->setRaavalue($value);
			$raaconfig->setCreated(now());
			$raaconfig->save();
		}
	}	
	
	
	public function setRaaconfig($data) {
	
		try {
			
			foreach ( $data as $row ) {
				foreach($row as $key => $value) {				
					$this->saveRaaconfig( $key, $value);
				}
			}
		}
		catch ( Exception $e ) {
			return false;
		}
		return true;
	}
	
	
	
	public function deleteRaaconfig($key) {
	
		$raaconfig_collection = Mage::getModel('retail_analytics/raaconfig')->getCollection ()->addFieldToFilter ( 'raakey', array ($key) );
		if($raaconfig_collection->count() > 0) {
			$deleteconfig = $raaconfig_collection->getFirstItem();		
			$deleteconfig->delete();
		}	
		
	}
	
	
	public function removeRaaconfig($data) {	
	
		try {
			
			foreach ( $data as $key ) {	
					$this->deleteRaaconfig( $key);	
			}
		}			
		catch ( Exception $e ) {
			return false;
		}		
		return true;
	}	
	
		
	public function getRaaconfig($key) {
	
		$value = "";
		try {
				
			$raaconfig_collection = Mage::getModel('retail_analytics/raaconfig')->getCollection ()->addFieldToFilter ( 'raakey', array ($key) );
			if($raaconfig_collection->count() > 0) {
				$value = $raaconfig_collection->getFirstItem()->getRaavalue();
			}  
			
			return $value;
		}
		catch ( Exception $e ) {
			return $value;
		}
		
		return $value;
	}
	

	
	
	public function getRaaconfigs() {
		$data = array ();
		try {
			$raaconfigs = Mage::getModel('retail_analytics/raaconfig')->getCollection();
			foreach($raaconfigs as $raaconfig)
			{				
					$dataArray = array();
					$dataArray [$raaconfig->getData('raakey')] = $raaconfig->getData('raavalue');
					$dataArray ['created_at'] = $raaconfig->getData('created');
					$data[] =  $dataArray;				
			}
			return $data;
		} catch ( Exception $ex ) {
			return $data;
		}
	}
	
}