<?php

class Retail_Analytics_Helper_Productmap extends Mage_Core_Helper_Abstract
{
	public function saveProductMap($data) {	
		$productmap = Mage::getModel('retail_analytics/productmap');		
		$productmap->setProductid($data['productid']);
		$productmap->setMap($data['map']);
		$productmap->setCategoryid($data['categoryid']);
		$productmap->setCreated(now());
		$productmap->setModified(now());		
		$productmap->save();
	}
	
	
	
	public function addProductMap($data) {
	
		try {		
			foreach ( $data as $row ) {
				$this->saveProductMap( $row );
			}
			
			return true;
		}
			
		catch ( Exception $e ) {
			return false;
		}
	}
	
	
	
	public function getProductMapById($productid) {
		try{
			
			$data = array();
			$data_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'productid', array (
				$productid
			) );	
					
			if ($data_collection->count () > 0) {
				return $data_collection->getLastItem()->getData();
			}	
			return $data;
		}
		catch ( Exception $e ) {
			echo json_encode ( $data );
		}
	}
	
	public function getProductMapLastItem() {
	try{
				
			$data = array();				
			$data_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ();
			$data = $data_collection->getLastItem()->getData();
			return $data;
		}
		catch ( Exception $e ) {
			echo json_encode ( $data );
		}
	}
		
	
	
}