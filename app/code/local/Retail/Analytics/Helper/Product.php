<?php

class Retail_Analytics_Helper_Product extends Mage_Core_Helper_Abstract
{
	//Helper Product
	public function isInStock($key,$valueArray){
		if(strtolower($key)=='sku' )
		{
			$key='sku';
		}
		else if (strtolower($key)=='entity_id')
		{
			$key='entity_id';
		}
		else {
			$key='entity_id';
		}
		
		$collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter($key,$valueArray)
                ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                ->addAttributeToSelect('*'); 
		$returnArray=array();
		foreach ( $collection as $product ) {
			//var_dump($product);
			$stockItem=$product->getStock_item();
			if($stockItem->getIs_in_stock()){
				$returnArray[]=$product->getData ($key);
			} 
		}
		return $returnArray;		
	}
	
	public function checkStockById($id){
		
		$result = false;
		$product = Mage::getModel ( 'catalog/product' )->load($id);
		if($product)
		{
			$product_id = $product->getData ( 'entity_id' );
			if($product_id !=null)
			{
				$stockItem = $product->getStock_item();
				$result = $stockItem->getIs_in_stock();
			}
		}		
		return $result;
	}
		
	public function checkStockBySku($sku){
	
		$result = false;
		$product = Mage::getModel ( 'catalog/product' )->loadByAttribute('sku',$sku);
		if($product)
		{
			$product_id = $product->getData ( 'entity_id' );
			if($product_id !=null)
			{
				$stockItem = $product->getStock_item();
				$result = $stockItem->getIs_in_stock();
			}
		}
		return $result;
	}

	
}