<?php
class Retail_Analytics_Helper_Upload extends Mage_Core_Helper_Abstract {
		
	public function uploadFile($path,$fileToUpload) {
		try{
								
			$target_dir = $path;		
				
			$target_file = $target_dir.'/' . basename($_FILES["fileToUpload"]["name"]);		
								
			if(file_exists($target_file)) {						
				if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
					echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";					
				} else {
					echo "Sorry, there was an error uploading your file.";
				}					
				
			}
			else {
				echo "Sorry, file not exists at the target path.";
			}
			
		}
		catch ( Exception $e ) {
			echo $e->getMessage();		
		}
	}
}