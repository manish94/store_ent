<?php

class Retail_Analytics_Helper_Onsite extends Mage_Core_Helper_Abstract
{
		
	public function getCmsBlockID($identifier,$type)
	{
		 $default_block_id = "";
		 if($type == "header") {
		 	$default_block_id = "raa_slot_default_header";
		 }	
		 elseif($type == "content") {
		 	$default_block_id = "raa_slot_default_content";
		 }	
	 	elseif($type == "script") {
		 	$default_block_id = "raa_slot_default_script";
		 }		
		 
		 $block_id = $identifier.'_'.$type;	
		 $cmsBlockId = Mage::helper ( 'retail_analytics/raaconfig' )->getRaaconfig($block_id);
		 if($cmsBlockId == "" || $cmsBlockId == null)
		 {	
		 	$cmsBlockId = Mage::helper ( 'retail_analytics/raaconfig' )->getRaaconfig($default_block_id);		 	
		 } 		 
		
 	 	$cmsBlock = Mage::getModel('cms/block')->load($cmsBlockId);
	 	if ($cmsBlock->isObjectNew()) {
	 		$cmsBlockId = "";
	 	} 
		
		 
		 
		 return $cmsBlockId;
	}
	
	public function getStaticBlock($identifier,$filterArray)
	{
		// loading the static block
		$block = Mage::getModel('cms/block')
		->setStoreId(Mage::app()->getStore()->getId())
		->load($identifier);
	
		// loading the filter which will get the array we created and parse the block content
		$filter = Mage::getModel('cms/template_filter');
		/* @var $filter Mage_Cms_Model_Template_Filter */
		$filter->setVariables($filterArray);
	
		// return the filtered block content.
		return $filter->filter($block->getContent());
	
	}
	
	public function getProductFilter() {
		try {
				
			$productArray = array();
			$raaconfig_collection = Mage::getModel('retail_analytics/raaconfig')->getCollection ()->addFieldToFilter ( 'raakey', array('like' => '%productfilters%') );
			if($raaconfig_collection->count() > 0) {
				$value = $raaconfig_collection->getFirstItem()->getRaavalue();
				foreach ($raaconfig_collection as $productfilters)
				{
					$productArray[] = unserialize($productfilters->getData('raavalue'));
				}
			}
	
			return $productArray;
	
		} catch ( Exception $e ) {
			return false;
		}
	}
	
	
	public function applyProductFilters($product_collection) {
		try
		 {
					$productfilters = $this->getProductFilter();
    					foreach ($productfilters as $filter) {
    						
							if( $filter['attribute'] == 'qty')
							{
								$product_collection = $product_collection->joinField('qty',
										'cataloginventory/stock_item',
										'qty',
										'product_id=entity_id',
										'{{table}}.stock_id=1',
										'left')
										->addAttributeToFilter('qty', array($filter['operator'] => $filter['value']));
							} 
							else if( $filter['attribute'] == 'is_in_stock')
							{
								Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($product_collection);							
							}
							else
							{
								$product_collection = $product_collection->addAttributeToFilter ( $filter['attribute'], array (
										$filter['operator'] => $filter['value']
								));
							} 
						}	
						
						Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($product_collection);
						Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($product_collection);
						
			return $product_collection;
	
		} catch ( Exception $e ) {
			return false;
		}
	}
	
	
	public function getCategoryRelatedProducts($categoryid,$productid,$noofproduct=10) {
		
		$data = array();
		try{	 
			
			if($categoryid !=null && $categoryid != "")
			{
					
					
					$product_collection = Mage::getModel('catalog/product')->getCollection()
    					->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id=entity_id', null, 'left')   
    					->addAttributeToSelect ( "*" )    					
    					->addAttributeToFilter('category_id', array(
						            array('finset' => array($categoryid)),
						    ))
						->addAttributeToFilter ( 'entity_id', array (
    							"neq" => $productid
    					));
    					
						
					$product_collection = $this->applyProductFilters($product_collection);
					$product_collection->getSelect ()->limit ($noofproduct );
								
					$data = $product_collection;
					
				
			}
	
			return $data;
		}
		catch ( Exception $e ) {
			return $data;
		}
	}
	
	
 public function getSimilarProducts($productid,$noofproduct=10) {    	
 	
    	try{      	
    		
    		$data = array();
    		$productIdies = array();
    		$map="";
    		$data_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'productid', array ($productid) );
    			
    		if ($data_collection->count () > 0) {
    			$map = $data_collection->getFirstItem()->getMap();
    
    			$map_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'map', array ($map));
    			if ($map_collection->count () > 0) {
    				
    				foreach ( $map_collection as $product ) {
    					$id = $product->getProductid();
    					if($id != $productid){
    						$productIdies[] = $id;
    					}
    				}
    				    					
    				$product_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
    						"in" => $productIdies
    				))
    				->addAttributeToSelect ( "*" );
    				
    				$product_collection = $this->applyProductFilters($product_collection);  
    				
    				$product_collection->getSelect ()->limit ($noofproduct );   		    					
    				$data = $product_collection;    				
    			}
    		}
    
    		return $data;
    	}
    	catch ( Exception $e ) {
    		return $data;
    	}
    }
    
    
    public function getRecoProducts($productid,$noofproduct=10) {
    
    	try{
    		
    		$data = array();
    		$productIdies = array();
    		$map="";
    		$data_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'productid', array ($productid) );
    		 
    		if ($data_collection->count () > 0) {
    			$map = $data_collection->getFirstItem()->getMap();
    			
    			$reco_collection = Mage::getModel ( 'retail_analytics/productreco' )->getCollection ()->addFieldToFilter ( 'Productmap', array ($map));
    			if ($reco_collection->count () > 0) {    					    			
    					$map = $reco_collection->getFirstItem()->getMap();    				
    					
    					$map_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'map', array ($map));
    					if ($map_collection->count () > 0) {
    					
    						foreach ( $map_collection as $product ) {
    							$id = $product->getProductid();
    							if($id != $productid){
    								$productIdies[] = $id;
    							}
    						}
    						 
    						$product_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
    								"in" => $productIdies
    						))
    						->addAttributeToSelect ( "*" );
    						
	    					$product_collection = $this->applyProductFilters($product_collection);  
    						$product_collection->getSelect ()->limit ($noofproduct );
    					 	$data = $product_collection;    						
    					}
	    			}
    			}    		
    
    		return $data;
    	}
    	catch ( Exception $e ) {
    		return $data;
    	}
    }
    
    public function getCustomerOffer($email,$noofproduct) {
    
    try{
    		$data = array("data"=>array(),"offerprice"=>array());
    		if($email != null) {
    		$productIdies = array();
			//$date = new Zend_Date();
			$today =  date("Y-m-d 00:00:00");
    		$recooffer_collection = Mage::getModel ( 'retail_analytics/recooffer' )->getCollection ()
    		->addFieldToFilter ( 'validto', array('gteq' => $today) )
    		->addFieldToFilter ( 'email', array($email) );
    		
    			if($recooffer_collection->count()>0) {
		    		foreach ($recooffer_collection as $recooffer) {
		    			 
		    			$coupon = Mage::getModel('salesrule/coupon')->load($recooffer->getData('couponcode'), 'code');
		    			$rule = Mage::getModel('salesrule/rule')->load($coupon->getRuleId());
		    			if($rule->getIsActive()){    	
		    				$productIdies[] = $recooffer->getData('productid');
		    				$offerArray = array("offerprice"=>$recooffer->getData('offerprice'),"couponcode"=>$recooffer->getData('couponcode'),"validto"=>$recooffer->getData('validto'));
		    				$data["offerprice"][$recooffer->getData('productid')] = $offerArray;
		    			}
		    		}  	    				
		    		
    				$product_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
    						"in" => $productIdies
    				))
    				->addAttributeToSelect ( "*" );
    				
    				$product_collection = $this->applyProductFilters($product_collection);  
    				
    				$product_collection->getSelect ()->limit ($noofproduct );  
    				$data["data"] = $product_collection;
    			}    		
    		}
    		return $data;
    	}
    	catch ( Exception $e ) {
    		return $data;
    	}
    }
    
    public function getCustomerReco($email,$noofproduct) {
    
    	try{
    		$data = array();
    		$productIdies = array();
    		$map="";
    		$data_collection = Mage::getModel ( 'retail_analytics/customerreco' )->getCollection ()->addFieldToFilter ( 'email', array ($email) );
    		 
    		if ($data_collection->count () > 0) {
    			$map = $data_collection->getFirstItem()->getMap();
    
    			$map_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'map', array ($map));
    			if ($map_collection->count () > 0) {
    
    				foreach ( $map_collection as $product ) {
    					$id = $product->getProductid();
    						$productIdies[] = $id;
    				}
    				 
    				$product_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
    						"in" => $productIdies
    				))
    				->addAttributeToSelect ( "*" );
    				
    				$product_collection = $this->applyProductFilters($product_collection);  
    				$product_collection->getSelect ()->limit ($noofproduct );
    				$data = $product_collection;
    			}
    		}
    
    		return $data;
    	}
    	catch ( Exception $e ) {
    		return $data;
    	}
    }
    
    public function getCustomerNonReco($email,$noofproduct) {
    
    	try{
    		$data = array();
    		$productIdies = array();
    		$map="";
    		$data_collection = Mage::getModel ( 'retail_analytics/customernonreco' )->getCollection ()->addFieldToFilter ( 'email', array ($email) );
    		 
    		if ($data_collection->count () > 0) {
    			$map = $data_collection->getFirstItem()->getMap();
    
    			$map_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'map', array ($map));
    			if ($map_collection->count () > 0) {
    
    				foreach ( $map_collection as $product ) {
    					$id = $product->getProductid();
    					$productIdies[] = $id;
    				}
    					
    				$product_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
    						"in" => $productIdies
    				))
    				->addAttributeToSelect ( "*" );
    				
    				$product_collection = $this->applyProductFilters($product_collection);  
    				$product_collection->getSelect ()->limit ($noofproduct );
     				$data = $product_collection;
    			}
    		}
    
    		return $data;
    	}
    	catch ( Exception $e ) {
    		return $data;
    	}
    }
    
    public function getProductYouMayLike($email,$noofproduct) {
    
    
    	try{
    		$half_noofproduct = ($noofproduct / 2);
    		$data = array();    		
    		$customer_reco = $this->getCustomerReco($email,$noofproduct);
    		$customer_none_reco = $this->getCustomerNonReco($email,$noofproduct);  
    		$customer_reco_no = count($customer_reco);
    		$customer_none_reco_no =  count($customer_none_reco);
    		$customer_reco_no = ($customer_reco_no >= $half_noofproduct && $customer_none_reco_no >= $half_noofproduct)?$half_noofproduct:$customer_reco_no;
    		$customer_none_reco_no = ($noofproduct - $customer_reco_no);
    		
    		$i=0;
    		foreach ( $customer_reco as $product ) {
    			if($i < $customer_reco_no)
    			{	
    				$data[] = $product;
    			}
    		}
    		
    		$i=0;
    		foreach ( $customer_none_reco as $product ) {
    			if($i < $customer_none_reco_no)
    			{
    				$data[] = $product;
    			}
    		}
    		
    
    		return $data;
    	}
    	catch ( Exception $e ) {
    		return $data;
    	}
    }
    
    public function getRecentlyViewed($productid,$email,$noofproduct,$ipenable) {
       
    	
    	try{
    		
    		$data = array();
    		$productIdies = array();
    		$data_collection = array();
    		if($email != null) {
    			$data_collection = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ()->addFieldToFilter ( 'email', array ($email) );
    		}
    		else if($ipenable == true || $ipenable == "1") { 
    			$raaHelper = Mage::helper ( 'retail_analytics/data' );
    			$externalip = $raaHelper->getExternalIP();    			
    			$internalip = $raaHelper->getInternalIp();	
    			
    			$data_collection = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ()->addFieldToFilter ( 'ip', array ($externalip) )->addFieldToFilter ( 'internalip', array ($internalip) )->addFieldToFilter( 'customerid', array('null' => true) );
    		}
    		if (count($data_collection) > 0) {    			
    
    				foreach ( $data_collection as $recentlyviewed ) {
    					$id = $recentlyviewed->getProductid();
    					if($id != $productid) {
    						$productIdies[] = $id;
    					}
    				}
    					
    				$product_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
    						"in" => $productIdies
    				))
    				->addAttributeToSelect ( "*" );
    				
    				$product_collection = $this->applyProductFilters($product_collection);  
    				
    				$product_collection->getSelect ()->limit ($noofproduct );
     				$data = $product_collection;
    			
    	}
    
    		return $data;
    	}
    	catch ( Exception $e ) {
    		return $data;
    	}
    }
    
    
    public function getRecentlyViewedSimilarProducts($productid,$email,$noofproduct,$ipenable) {
    	 
    	 
    	try{
    		$noofproduct = intval($noofproduct);
    		$data = array();
    		$productIdies = array();
    		$data_collection = array();
    		if($email != null) {
    			$data_collection = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ()->addFieldToFilter ( 'email', array ($email) );
    		}
    		else if($ipenable == true || $ipenable == "1") { 
    			$raaHelper = Mage::helper ( 'retail_analytics/data' );
    			$externalip = $raaHelper->getExternalIP();
    			$internalip = $raaHelper->getInternalIp();
    			
    			$data_collection = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ()->addFieldToFilter ( 'ip', array ($externalip) )->addFieldToFilter ( 'internalip', array ($internalip) )->addFieldToFilter( 'customerid', array('null' => true) );
    		}
    		if (count($data_collection) > 0) {
    
    			foreach ( $data_collection as $recentlyviewed ) {
    				$id = $recentlyviewed->getProductid();
    				if($id != $productid) {
    					$productIdies[] = $id;
    				}
    			}
    				
    			
    			$productmap_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'productid', array ('in' => $productIdies) );
    			 
    			if ($productmap_collection->count () > 0) {
    				$mapArray = array();    			
    				foreach ( $productmap_collection as $productmap ) {
    					$mapArray[] = $productmap->getMap();
    				}    				
    				
    				$map_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'map', array ('in' => $mapArray));
    				if ($map_collection->count () > 0) {
    					$productIdArray = array();
    					foreach ( $map_collection as $product ) {
    						$id = $product->getProductid();
    						if(!in_array($id, $productIdies)){
    							$productIdArray[] = $id;
    						}
    					}
    					
    					$product_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
    							"in" => $productIdArray
    					))->addAttributeToSelect ( "*" );
    					
    					$product_collection = $this->applyProductFilters($product_collection);  
    					
    					$product_collection->getSelect ()->order(new Zend_Db_Expr('RAND()'))->limit ($noofproduct );    
    					$data = $product_collection;
    					
    					
    				}
    			}    			 
    		}
    
    		return $data;
    	}
    	catch ( Exception $e ) {
    		return $data;
    	}
    }
    
    public function getRecentlyViewedRecoProducts($productid,$email,$noofproduct,$ipenable) {
    
    
    	try{
    		$noofproduct = intval($noofproduct);
    		$data = array();
    		$productIdies = array();
    		$data_collection = array();
    		if($email != null) {
    			$data_collection = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ()->addFieldToFilter ( 'email', array ($email) );
    		}
    		else if($ipenable == true || $ipenable == "1") { 
    			$raaHelper = Mage::helper ( 'retail_analytics/data' );
    			$externalip = $raaHelper->getExternalIP();      			
    			$internalip = $raaHelper->getInternalIp();	    			
    			$data_collection = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ()->addFieldToFilter ( 'ip', array ($externalip) )->addFieldToFilter ( 'internalip', array ($internalip) )->addFieldToFilter( 'customerid', array('null' => true) );
    		}
    		if (count($data_collection) > 0) {
    
    			foreach ( $data_collection as $recentlyviewed ) {
    				$id = $recentlyviewed->getProductid();
    				if($id != $productid) {
    					$productIdies[] = $id;
    				}
    			}
    
    			 
    			$productmap_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'productid', array ('in' => $productIdies) );
    
    			if ($productmap_collection->count () > 0) {
    				$mapArray = array();
    				foreach ( $productmap_collection as $productmap ) {
    					$mapArray[] = $productmap->getMap();
    				}
    				
    				$reco_collection = Mage::getModel ( 'retail_analytics/productreco' )->getCollection ()->addFieldToFilter ( 'Productmap', array ('in' => $mapArray));
    				if ($reco_collection->count () > 0) {
    					
    				$mapArray = array();
    				foreach ( $reco_collection as $recomap ) {
    					$mapArray[] = $recomap->getMap();
    				}
    				
    				$map_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'map', array ('in' => $mapArray));
    				if ($map_collection->count () > 0) {
    					$productIdArray = array();
    					foreach ( $map_collection as $product ) {
    						$id = $product->getProductid();
    						if(!in_array($id, $productIdies)){
    							$productIdArray[] = $id;
    						}
    					}
    						
    					$product_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
    							"in" => $productIdArray
    					))->addAttributeToSelect ( "*" );
    					
    					$product_collection = $this->applyProductFilters($product_collection);  
    					$product_collection->getSelect ()->order(new Zend_Db_Expr('RAND()'))->limit ($noofproduct );
    					
    					$data = $product_collection;
    						
    						
    					}
    				}
    			}
    		}
    
    		return $data;
    	}
    	catch ( Exception $e ) {
    		return $data;
    	}
    }
    
    
    public function getBestSellingProducts($from, $noofproduct, $period='month')
    {
    	
    	$data = array ();
    	$data2 = array ();
    	try {
    
    		$current_category = Mage::helper ( 'retail_analytics' )->getCurrentCategory();
    		
    		$date = new Zend_Date();
    		$toDate = $date->getDate()->get('Y-MM-dd');
    		$fromDate = null;
    		
    		if($from != "" && $from != null && $from != 0){
    			$fromDate = $date->subMonth($from)->getDate()->get('Y-MM-dd');
    		}
    		
    		$storeId    = Mage::app()->getStore()->getId();
    
    		$totalsCollection = Mage::getResourceModel('sales/report_bestsellers_collection')
    		->setPeriod($period);
    		//->setDateRange($fromDate, $toDate);
    		//->addStoreFilter($storeId)
    		//->setAggregatedColumns($this->_getAggregatedColumns())
    		//->isTotals(true);
    
    		 
    		if($fromDate != null){
    			$totalsCollection = $totalsCollection->setDateRange($fromDate, $toDate);
    		}
    
    		$productQty = array();
    
    		foreach ($totalsCollection as $item) {
    			
		    			
		    			$productid =  $item->getData('product_id');
		    			$prod = Mage::getModel('catalog/product')->load($productid);
		    			if($current_category != "")
		    			{		    				
		    				$categoryIds = $prod->getCategoryIds();
		    				if (isset($categoryIds[0])){    				
								if (!in_array($current_category, $categoryIds)){
									continue;
								}
		    				}
		    			}	
    				   
		    			$productArray = array();
		    			$productArray['productid'] = $productid;
		    			$productArray['name'] =  $item->getData('product_name');
		    			$productArray['sku'] =  $item->getData('sku'); 	
		    							
			    			if($productArray['sku'] == null || $productArray['sku'] == "")
			    			{    
			    					$productArray['sku'] = $prod->getSku();	    				
			    			}	    		
		    	   
		    			if (array_key_exists($productArray['productid'], $productQty)) {
		    				$productQty[$productArray['productid']] = intval( intval($productQty[$productArray['productid']]) + intval($item->getData('qty_ordered')));
		    			}
		    			else {
		    				$productQty[$productArray['productid']] = intval($item->getData('qty_ordered'));
		    				$data2[] = $productArray;
		    			}  
		    		
		    			
    		}
    
    		foreach ($data2 as $row)
    		{
    			$tempArray = $row;
    			$tempArray['ordered_qty'] = $productQty[$tempArray['productid']];
    			$data[] = $tempArray;
    		}
    			
    		$productQtyArray = array();
    		foreach ($data as $key => $row)
    		{
    			$productQtyArray[$key] = $row['ordered_qty'];
    		}
    			
    		array_multisort($productQtyArray, SORT_DESC, $data);
    		
    		$productIdies = array();
    		foreach ($data as  $row)
    		{    			
    			$product = Mage::getModel('catalog/product')->load($row['productid']);
    			if($product->getTypeId() == "simple"){
    				$parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
    				if(!$parentIds){
    					$parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($product->getId());     					
    				}
    				
    				if(isset($parentIds[0])){
    					$productIdies[] = $parentIds[0];    					
    				}
    				else {
    					$productIdies[] = $row['productid'];
    				}
    			}
    			else {    				
    				$productIdies[] = $row['productid'];
    			}
    		}    		
    		
    		$product_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
    				"in" => $productIdies
    		))->addAttributeToSelect ( "*" );
    		$product_collection = $this->applyProductFilters($product_collection);      		
    		$product_collection->getSelect ()->limit ($noofproduct );    

    		
    		
    		
    		return $product_collection;
    		 
    	} catch ( Exception $e ) {
    		 
    		return array();
    	}
    }
    
    
    public function getPageSlots($pageid,$productid=null,$email=null) {
    	 
    	try{
    	
    	$data = array();
    	$products = array();    
    	
    	if($pageid !=null) {	    		
    
	    	if($email == null && Mage::getSingleton('customer/session')->isLoggedIn()) {
	    		$customerData = Mage::getSingleton('customer/session')->getCustomer();    		
	    		$email = $customerData->getEmail();
	    	} 
	    	
	    	$raaHelper = Mage::helper ( 'retail_analytics' );
	    	$developmentmodeemail = $raaHelper->getRaaConfig('developmentmodeemail');
	    	if ($developmentmodeemail != "") {
	    		 if($developmentmodeemail != $email) {
	    			return $data;
	    		 }
	    	}
	  
    		$reco_slots = Mage::getModel ( 'retail_analytics/onsiteconfig' )->getCollection ()->addFieldToFilter ( 'pageid', array ($pageid) );
    				
    		if ($reco_slots->count() > 0) {
    			
    			$isPageViewed = false;
    			foreach ($reco_slots as $slot)
    			{
    				$slotArray = array();
    				$slotid = $slot->getSlotid();
    				$enable = $slot->getEnable();
    				if(!$enable){
    					continue;
    				}    				
    					
    				$slot->setPbrurl(str_replace("%23","#",$slot->getPbrurl()));    			
    				switch ($slotid){
    					case "raa_reco_bseller":
    						$slotArray = $slot->getData();
    						$products = $this->getBestSellingProducts($slotArray['timeduration'],$slotArray['noofproduct']);
    						$isPageViewed = ($isPageViewed || count($products) > 0)?true:false;
    						$slotArray["data"] = $products;
    						$data[] = $slotArray;
    						break;
	    				case "raa_reco_related":   				
	    					$slotArray = $slot->getData();
	    					$products = $this->getSimilarProducts($productid,$slotArray['noofproduct']);
	    					$isPageViewed = ($isPageViewed || count($products) > 0)?true:false;
	    					$slotArray["data"] = $products;	    					
	    					$data[] = $slotArray;
	    					break;
    					case "raa_reco_upsell":
    						$slotArray = $slot->getData();
    						$products = $this->getRecoProducts($productid,$slotArray['noofproduct']);
    						$isPageViewed = ($isPageViewed || count($products) > 0)?true:false;
    						$slotArray["data"] = $products;
    						$data[] = $slotArray;
    						break;
    					case "raa_reco_catrelated":
    						$slotArray = $slot->getData();
    						$products = $this->getCategoryRelatedProducts($raaHelper->getCurrentCategory(),$productid,$slotArray['noofproduct']);
    						$isPageViewed = ($isPageViewed || count($products) > 0)?true:false;
    						$slotArray["data"] = $products;
    						$data[] = $slotArray;
    						break;
    					case "raa_reco_customer_offer":
    						$slotArray = $slot->getData();
    						if($email != null) {
    							$slotArray["rac"] =$email;
    						}
    						
    						$products = $this->getCustomerOffer($email,$slotArray['noofproduct']);     						 						
    						 if($isPageViewed || count($products["data"]) > 0){
    							$isPageViewed = true;
    							$slotArray["offerprice"] = $products["offerprice"];;
    						 } else {
    						 	$isPageViewed = false;
    						 }    						 
	    					
    						$slotArray["data"] = $products["data"];
    						$data[] = $slotArray;
    						break;
    					case "raa_reco_self":
    						$slotArray = $slot->getData();
    						if($email != null) {
    							$slotArray["rac"] =$email;
    						}
    						
    						$products = $this->getCustomerReco($email,$slotArray['noofproduct']);
    						$isPageViewed = ($isPageViewed || count($products) > 0)?true:false;
    						$slotArray["data"] = $products;
    						$data[] = $slotArray;
    						break;
    					case "raa_reco_nonself":
    						$slotArray = $slot->getData();
    						if($email != null) {
    							$slotArray["rac"] = $email;
    						}
    						$products = $this->getCustomerNonReco($email,$slotArray['noofproduct']);
    						$isPageViewed = ($isPageViewed || count($products) > 0)?true:false;
    						$slotArray["data"] = $products;
    						$data[] = $slotArray;
    						break;    
    					case "raa_reco_umaylike":
    						$slotArray = $slot->getData();
    						if($email != null) {
    							$slotArray["rac"] =$email;
    						}
    						
    						$products = $this->getProductYouMayLike($email,$slotArray['noofproduct']);
    						$isPageViewed = ($isPageViewed || count($products) > 0)?true:false;
    						$slotArray["data"] = $products;
    						$data[] = $slotArray;
    						break;
    					case "raa_reco_browsing":
    						$slotArray = $slot->getData();
    						$products = $this->getRecentlyViewed($productid, $email,$slotArray['noofproduct'],$slotArray['ipenable']);
    						$isPageViewed = ($isPageViewed || count($products) > 0)?true:false;
    						$slotArray["data"] = $products;
    						$data[] = $slotArray;
    						break;
    					case "raa_reco_brrelated":
    						$slotArray = $slot->getData();
    						$products = $this->getRecentlyViewedSimilarProducts($productid, $email,$slotArray['noofproduct'],$slotArray['ipenable']);
    						$isPageViewed = ($isPageViewed || count($products) > 0)?true:false;
    						$slotArray["data"] = $products;
    						$data[] = $slotArray;
    						break;
    					case "raa_reco_brupsell":
    						$slotArray = $slot->getData();
    						$products = $this->getRecentlyViewedRecoProducts($productid, $email,$slotArray['noofproduct'],$slotArray['ipenable']);
    						$isPageViewed = ($isPageViewed || count($products) > 0)?true:false;
    						$slotArray["data"] = $products;
    						$data[] = $slotArray;
    						break;
    					default:
    						break;	
    				}    			
    				
    			}
    			
    			$priority = array();
    			foreach ($data as $key => $row)
    			{
    				$priority[$key] = $row['priority'];
    			}
    			
    			array_multisort($priority, SORT_ASC, $data);
    			
    			if($isPageViewed){
    				Mage::helper ( 'retail_analytics/pageviewed' )->savePageViewed($pageid);    
    				$params = Mage::app()->getRequest ()->getParams ();
    				if (isset($params['ras']))
    				{    					
    					Mage::helper ( 'retail_analytics/pageviewed' )->savePageViewed(10);
    				}				
    			}
    		}        		
    		
    	
    	}
    	
    	return $data;
    }
    	catch ( Exception $e ) {
    		echo $data;
    	}
    }
}