<?php
class Retail_Analytics_Helper_Productservices extends Mage_Core_Helper_Abstract {
	
	public function getRaaImageURL($product)
	{
		$product->getData();
		 
		$SmallImage =  $product->getSmallImage();
		if($SmallImage == null || $SmallImage == "" || $SmallImage == 'no_selection' )
		{
			$Image = $product->getImage();
			$thumnailImage = $product->getThumbnail();
			if($Image!=null && $Image!="" && $Image != 'no_selection')
				return $product->getMediaConfig()->getMediaUrl($Image);
			else if($thumnailImage!=null && $thumnailImage!="" && $thumnailImage != 'no_selection')
				return $product->getMediaConfig()->getMediaUrl($thumnailImage);
			else
				return  $product->getImageUrl();
		}
		else
		{
			return $product->getMediaConfig()->getMediaUrl($SmallImage);
		}
	}
	
	public function raaAttributeSets() {
		$data = array ();
		try {
			$sets = Mage::getModel('eav/entity_attribute_set')->getCollection();
			foreach($sets as $set)
			{
				if( $set->getData ("entity_type_id") == "4")
				{
					$dataArray ["attribute_set_id"] = $set->getData ("attribute_set_id");
					$dataArray ["attribute_set_name"] = $set->getData ("attribute_set_name");
					$data[] =  $dataArray;
				}
			}
			return $data;
		} catch ( Exception $ex ) {
			return $data;
		}
	}
	
	public function raaAttributes($setId) {
		$data = array ();
		try {
			$data = Mage::getModel ( 'catalog/product_attribute_api' )->items ( $setId );
			return $data;
		} catch ( Exception $ex ) {
			return $data;
		}
	}
	
	public function getAttributeOptions($attributeid) {
		$data = array ();
		try {
				
			$attribute_model = Mage::getModel ( 'eav/entity_attribute' );
			$attribute_options_model = Mage::getModel ( 'eav/entity_attribute_source_table' );
			$attribute = $attribute_model->load ( $attributeid );
			$attribute_table = $attribute_options_model->setAttribute ( $attribute );
			$options = $attribute_options_model->getAllOptions ( false );
			foreach ( $options as $option ) {
				$data [] = array (
						'attribute_id' => $attributeid,
						'optionid' => $option ['value'],
						'value' => $option ['label']
				);
			}
			return $data;
		} catch ( Exception $ex ) {
			return $data;
		}
	}	
	
	public function raaProductAttributes() {
	
		$columnArray = array (
				"product_id",
				"sku",
				"name",
				"description",
				"price"
		);
	
		$data = array ();
		try {
	
			$raaSchemaHelper = Mage::helper('retail_analytics/schema');
			$attributesArray = array();
			$attributes = Mage::getResourceModel('catalog/product_attribute_collection')->getItems();
	
			foreach ($attributes as $attribute){
				$attributeCode = $attribute->getAttributecode();
				$attributeLabel = $attribute->getFrontendLabel();
				if (!in_array($attributeCode, $columnArray)) {
					if($attributeLabel !="" && $attributeLabel != null)
					{
						$data[] = array("mrchfield"=>$attributeCode,"datatype"=>$raaSchemaHelper->getDefaultDataType());
					}
				}
	
			}
	
			return $data;
		} catch ( Exception $ex ) {
			return $data;
		}
	}
	
	public function raaProductCategory($lastId, $limit) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		try {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			$attributes = explode ( ",", $raaHelper->getAttributes () );
			Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
			$data_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
					"gt" => $lastId
			) )->addAttributeToSelect ( "*" );
			$data_collection->getSelect ()->limit ( $limit );			
	
			if ($data_collection->count () > 0) {
				$lastId = $data_collection->getLastItem ()->getId ();
				foreach ( $data_collection as $product ) {
	
					
					$prod = Mage::getModel ( 'catalog/product' )->load ( $product->getId () );
					$productArray = array ();
					$productArray ['entity_id'] = $product->getData ( 'entity_id' );
					$productArray ['product_id'] = $product->getData ( 'entity_id' );
					$productArray ['sku'] = $product->getData ( 'sku' );
					$productArray ['website_ids'] = $product->getWebsiteIds();
					$productArray ['store_ids'] = $product->getStoreIds();
	
					$categoryIds = $prod->getCategoryIds();
					if (isset($categoryIds[0])){
						$parentCategory="";
						$_categoryids = array();
						$_parentCategoryids = array();
	
						$productArray["category_info"] = array();
						foreach ( $categoryIds as $_category ) {
							$category = Mage::getModel('catalog/category')->load($_category);
							$cat_path = $category->getPath();
							$productArray["category_info"][] = $cat_path;
	
							$childCategories = $category->getChildrenCount();
							$parentCategory=$_category;
							if($childCategories == "0")
							{
								$_categoryids[] = $_category;
							}
							else {
								$_parentCategoryids [] = $parentCategory;
							}
	
						}
	
						if(count($_categoryids)==0 && $parentCategory != "" ){
							$_categoryids[] = $parentCategory;
						}
							
						if(count($_categoryids) > 0)
						{
							$productArray ["category_id"] = $_categoryids[0];
							$_categoryids = array_merge($_categoryids,$_parentCategoryids);
							$productArray ["category_ids"] = Mage::helper ( 'core' )->jsonDecode(Mage::helper ( 'core' )->jsonEncode( $_categoryids));
							//$productArray ['root_category_id'] = Mage::helper('retail_analytics/categoryservices')->getRootCatId($productArray ["category_id"]);
						}
	
					}
						
					$data [] = $productArray;
				}
	
				$retObj ['data'] = $data;
				$retObj ['lastid'] = $lastId;
				if ($data_collection->count () < $limit) {
					$retObj ['islast'] = true;
				} else {
					$retObj ['islast'] = false;
				}
			} else {
				$retObj ['lastid'] = $lastId;
				$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
	
			return $retObj;
		} catch ( Exception $e ) {
	
			$retObj ['lastid'] = - 1;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}
		
	public function raaProductById($id,$isadmin=false,$skip = null) {
		$productArray = array ();
		try{
				
			if($isadmin != false)
			{
				Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
			}

			if($skip == null )
			{
				$skip = array("description");
			}
			
			$product = Mage::getModel ( 'catalog/product' )->load($id);
	
			if($product)
			{
				$product_id = $product->getData ( 'entity_id' );
				if($product_id !=null)
				{
					$helperdata = Mage::helper('retail_analytics/data');
					$width = $helperdata->getImageWidth();
					$height = $helperdata->getImageHeight();
					$imagetype = "small_image";
					$productArray ['entity_id'] = $product_id;
					$productArray ['product_id'] = $product_id;
					$productArray ['url'] = $product->getProductUrl ();
					$productArray ['color'] = $product->getColor ();
					$productArray ['imageurl'] = $this->getRaaImageURL($product);
					$productArray ["instock"] = $product->getIsInStock ();
					$productArray ["quantity"] = $product->getStockItem ()->getQty ();
					$productArray ['base_currency_code']= Mage::app()->getStore()->getBaseCurrencyCode();
					$productArray ['website_ids'] = $product->getWebsiteIds();
					$productArray ['store_ids'] = $product->getStoreIds();
					$productArray ['cacheimageurl'] = (string)Mage::helper('catalog/image')->init($product, $imagetype)->resize($width,$height);
						
					if($product->getTypeID() == "configurable")
					{
							
						$productArray ["product_child"] = array();
						$productArray ["child_sku"] = array();
						$totalQty = 0;
						$childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);
						foreach($childProducts as $child) {
							$productArray ["product_child"][] = $child->getId();
							$productArray ["child_sku"][] = $child->getSku();
							$totalQty = ($totalQty + $child->getStockItem ()->getQty ());
						}
	
						$productArray ["quantity"] = $totalQty;
					}
	
						
					$attributes = $product->getAttributes();
					foreach ($attributes as $attribute) {
							
						$attributeCode = $attribute->getAttributeCode();
						if (!in_array($attributeCode,$skip))
						{
							$productArray [$attribute->getAttributeCode() ] = $attribute->getFrontend()->getValue($product);
						}
					}
						
					$productArray ["special_price"] = $product->getFinalPrice();
					$discountPercent = 0;
					if($productArray ["special_price"]!=$productArray ["price"])
					{
						$discountPercent = ($productArray ["price"] - $productArray ["special_price"]) * 100/$productArray ["price"];
					}
					$productArray ["discount_percent"] = $discountPercent;
					$categoryIds = $product->getCategoryIds();
					if (isset($categoryIds[0])){
						$_categoryids = array();
						$_parentCategoryids = array();
						$parentCategory="";
	
						$productArray["category_info"] = array();
						foreach ( $categoryIds as $_category ) {
							$category = Mage::getModel('catalog/category')->load($_category);
							$cat_path = $category->getPath();
							$productArray["category_info"][] = $cat_path;
	
							$childCategories = $category->getChildrenCount();
							$parentCategory=$_category;
							if($childCategories == "0")
							{
								$_categoryids[] = $_category;
							}
							else {
								$_parentCategoryids [] = $parentCategory;
							}
	
						}
						if(count($_categoryids)==0 && $parentCategory != "" ){
							$_categoryids[] = $parentCategory;
						}
						if(count($_categoryids) > 0)
						{
							$productArray ["category_id"] = $_categoryids[0];
							$_categoryids = array_merge($_categoryids,$_parentCategoryids);
							$productArray ["category_ids"] = Mage::helper ( 'core' )->jsonDecode(Mage::helper ( 'core' )->jsonEncode( $_categoryids));
							//$productArray ['root_category_id'] =  Mage::helper('retail_analytics/categoryservices')->getRootCatId($productArray ["category_id"]);
						}
					}
				}
			}
				
			return $productArray;
		}
	
		catch (Exception $ex)
		{
			return $productArray;
		}
	
	}
	
	public function raaProductsFinalPrice($lastId, $limit,$imagetype,$width ,$height,$isadmin=false) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		try {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			$attributes = explode ( ",", $raaHelper->getAttributes () );
			if($isadmin != false)
			{
				Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
			}
			$data_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
					"gt" => $lastId
			));
			$data_collection->getSelect ()->limit ( $limit );
			
	
			if ($data_collection->count () > 0) {
				$lastId = $data_collection->getLastItem ()->getId ();
				foreach ( $data_collection as $prod ) {
					
					$product = Mage::getModel ( 'catalog/product' )->load ( $prod->getId () );
					$productArray = array ();
					$productArray ['product_id'] = $product->getData ( 'entity_id' );
					$productArray ['sku'] = $product->getData ( 'sku' );
					$productArray ['name'] = $product->getData ( 'name' );
					$price = $product->getPrice ();
					$productArray ['price'] = $price;
					$productArray ['special_price'] = $product->getFinalPrice ();
					$productArray ['status'] = $product->getAttributeText("status");
					$productArray ['visibility'] =  $product->getAttributeText("visibility");
					$productArray ['instock'] = $product->getIsInStock ();
					$productArray ['quantity'] = $product->getStockItem ()->getQty ();
					$productArray ['url'] = $product->getProductUrl ();
					$productArray ['imageurl'] = $this->getRaaImageURL($product);
					$productArray ['cacheimageurl'] = (string)Mage::helper('catalog/image')->init($product, $imagetype)->resize($width,$height);
					$discountPercent = 0;
					if ($productArray ["special_price"] != $price) {
						$discountPercent = (($price - $productArray ["special_price"]) * 100)/$price;
					}
						
					$productArray ["discount_percent"] = $discountPercent;
						
					if($product->getTypeID() == "configurable")
					{
						$productArray ["product_child"] = array();
						$productArray ["child_sku"] = array();
						$totalQty = 0;
						$childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);
						foreach($childProducts as $child) {
							$productArray ["product_child"][] = $child->getId();
							$productArray ["child_sku"][] = $child->getSku();
							$totalQty = ($totalQty + $child->getStockItem ()->getQty ());
						}
							
						$productArray ["quantity"] = $totalQty;
					}
						
					$data [] = $productArray;
				}
				$retObj ['data'] = $data;
				$retObj ['lastid'] = $lastId;
				if ($data_collection->count () < $limit) {
					$retObj ['islast'] = true;
				} else {
					$retObj ['islast'] = false;
				}
			} else {
				$retObj ['lastid'] = $lastId;
				$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
	
			return $retObj;
		} catch ( Exception $e ) {
	
			$retObj ['lastid'] = - 1;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}
			
	public function raaProducts($lastId, $limit,$isadmin=false,$skip = null) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		if($skip == null )
		{
			$skip = array("description");
		}
	
		try {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			$attributes = explode ( ",", $raaHelper->getAttributes () );
			if($isadmin != false)
			{
				Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
			}
			$data_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
					"gt" => $lastId
			) )->addAttributeToSelect ( "*" );
			$data_collection->getSelect ()->limit ( $limit );
			
			$helperdata = Mage::helper('retail_analytics/data');
			$width = $helperdata->getImageWidth();
			$height = $helperdata->getImageHeight();
			$imagetype = "small_image";
			if ($data_collection->count () > 0) {
				$lastId = $data_collection->getLastItem ()->getId ();
				foreach ( $data_collection as $product ) {
					
					$prod = Mage::getModel ( 'catalog/product' )->load ( $product->getId () );
					$productArray = array ();
					$productArray ['entity_id'] = $product->getData ( 'entity_id' );
					$productArray ['product_id'] = $product->getData ( 'entity_id' );
					$productArray ['url'] = $product->getProductUrl ();
					$productArray ['color'] = $product->getColor ();
					$productArray ['imageurl'] = $this->getRaaImageURL($prod);
					$productArray ['cacheimageurl'] = (string)Mage::helper('catalog/image')->init($product, $imagetype)->resize($width,$height);
					$productArray ["instock"] = $prod->getIsInStock ();
					$productArray ["quantity"] = $prod->getStockItem ()->getQty ();
					$productArray ['base_currency_code']= Mage::app()->getStore()->getBaseCurrencyCode();
					$productArray ['website_ids'] = $product->getWebsiteIds();
					$productArray ['store_ids'] = $product->getStoreIds();
	
					if($prod->getTypeID() == "configurable")
					{
	
						$productArray ["product_child"] = array();
						$productArray ["child_sku"] = array();
						$totalQty = 0;
						$childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$prod);
						foreach($childProducts as $child) {
							$productArray ["product_child"][] = $child->getId();
							$productArray ["child_sku"][] = $child->getSku();
							$totalQty = ($totalQty + $child->getStockItem ()->getQty ());
						}
	
						$productArray ["quantity"] = $totalQty;
					}
	
					$attributes = $prod->getAttributes();
					foreach ($attributes as $attribute) {
							
						$attributeCode = $attribute->getAttributeCode();
						if (!in_array($attributeCode,$skip))
						{
							$productArray [$attribute->getAttributeCode() ] = $attribute->getFrontend()->getValue($prod);
						}
					}
	
					$productArray ["special_price"] = $prod->getFinalPrice();
					$discountPercent = 0;
					if($productArray ["special_price"]!=$productArray ["price"])
					{
						$discountPercent = ($productArray ["price"] - $productArray ["special_price"]) * 100/$productArray ["price"];
					}
					$productArray ["discount_percent"] = $discountPercent;
					$categoryIds = $prod->getCategoryIds();
					if (isset($categoryIds[0])){
						$_categoryids = array();
						$_parentCategoryids = array();
						$parentCategory="";
							
	
						$productArray["category_info"] = array();
						foreach ( $categoryIds as $_category ) {
							$category = Mage::getModel('catalog/category')->load($_category);
							$cat_path = $category->getPath();
							$productArray["category_info"][] = $cat_path;
	
	
							$childCategories = $category->getChildrenCount();
							$parentCategory=$_category;
							if($childCategories == "0")
							{
								$_categoryids[] = $_category;
							}
							else {
								$_parentCategoryids [] = $parentCategory;
							}
	
						}
							
	
						if(count($_categoryids)==0 && $parentCategory != "" ){
							$_categoryids[] = $parentCategory;
							
						}
						if(count($_categoryids) > 0)
						{
							$productArray ["category_id"] = $_categoryids[0];
							$_categoryids = array_merge($_categoryids,$_parentCategoryids);
							$productArray ["category_ids"] = Mage::helper ( 'core' )->jsonDecode(Mage::helper ( 'core' )->jsonEncode( $_categoryids));
							//$productArray ['root_category_id'] = Mage::helper('retail_analytics/categoryservices')->getRootCatId($productArray ["category_id"]);
						}
					}
	
					$data [] = $productArray;
				}
	
				$retObj ['data'] = $data;
				$retObj ['lastid'] = $lastId;
				if ($data_collection->count () < $limit) {
					$retObj ['islast'] = true;
				} else {
					$retObj ['islast'] = false;
				}
			} else {
				$retObj ['lastid'] = $lastId;
				$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
	
			return $retObj;
		} catch ( Exception $e ) {
	
			$retObj ['lastid'] = - 1;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}
		
	public function raaProductsByDate($lastdate,$lastid,$limit,$skip=null) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		$RowCount = 0;
		if($skip == null )
		{
			$skip = array("description");
		}
	
		try {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			$attributes = explode ( ",", $raaHelper->getAttributes () );
	
			$data_collection = Mage::getModel ( 'catalog/product' )->getCollection ();
				
			if($lastid==0)
			{
				$data_collection = $data_collection->addAttributeToFilter('updated_at', array('gteq' => $lastdate))
				->addAttributeToFilter('entity_id', array('gt' => $lastid))
				->addAttributeToSelect ( "*" )
				->addAttributeToSort("updated_at","ASC")
				->addAttributeToSort("entity_id","ASC");
				$data_collection->getSelect ()->limit ( $limit );
				$RowCount = $data_collection->count ();
	
			}
			else
			{
	
				$RowCount = Mage::getModel ( 'catalog/product' )->getCollection ()
				->addAttributeToFilter('updated_at', array('eq' => $lastdate))
				->addAttributeToFilter('entity_id', array('gt' => $lastid))
				->count ();
				$data_collection = $data_collection->addAttributeToFilter('updated_at', array('eq' => $lastdate))
				->addAttributeToFilter('entity_id', array('gt' => $lastid))
				->addAttributeToSelect ( "*" )
				->addAttributeToSort("entity_id","ASC");
				$data_collection->getSelect ()->limit ( $limit );
	
			}
				
			
			$helperdata = Mage::helper('retail_analytics/data');
			$width = $helperdata->getImageWidth();
			$height = $helperdata->getImageHeight();
			$imagetype = "small_image";
			if ($data_collection->count () > 0) {
				$lastdatesend = $data_collection->getLastItem ()->getUpdatedAt();
				$lastidsend=$data_collection->getLastItem()->getId();
				foreach ( $data_collection as $product ) {
					
					
					$prod = Mage::getModel ( 'catalog/product' )->load ( $product->getId () );
					$productArray = array ();
					$productArray ['entity_id'] = $product->getData ( 'entity_id' );
					$productArray ['product_id'] = $product->getData ( 'entity_id' );
					$productArray ['url'] = $product->getProductUrl ();
					$productArray ['color'] = $product->getColor();
					$productArray ['imageurl'] = $this->getRaaImageURL($prod);
					$productArray ["instock"] = $prod->getIsInStock ();
					$productArray ["quantity"] = $prod->getStockItem ()->getQty ();
					$productArray ['base_currency_code']= Mage::app()->getStore()->getBaseCurrencyCode();
					$productArray ['website_ids'] = $product->getWebsiteIds();
					$productArray ['store_ids'] = $product->getStoreIds();
					$productArray ['cacheimageurl'] = (string)Mage::helper('catalog/image')->init($product, $imagetype)->resize($width,$height);
						
						
					if($prod->getTypeID() == "configurable")
					{
							
						$productArray ["product_child"] = array();
						$productArray ["child_sku"] = array();
						$totalQty = 0;
						$childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$prod);
						foreach($childProducts as $child) {
							$productArray ["product_child"][] = $child->getId();
							$productArray ["child_sku"][] = $child->getSku();
							$totalQty = ($totalQty + $child->getStockItem ()->getQty ());
						}
	
						$productArray ["quantity"] = $totalQty;
					}
	
						
					$attributes = $prod->getAttributes();
					foreach ($attributes as $attribute) {
							
						$attributeCode = $attribute->getAttributeCode();
						if (!in_array($attributeCode,$skip))
						{
							$productArray [$attribute->getAttributeCode() ] = $attribute->getFrontend()->getValue($prod);
								
						}
					}
						
					$productArray ["special_price"] = $prod->getFinalPrice();
					$discountPercent = 0;
					if($productArray ["special_price"]!=$productArray ["price"])
					{
						$discountPercent = ($productArray ["price"] - $productArray ["special_price"]) * 100/$productArray ["price"];
					}
					$productArray ["discount_percent"] = $discountPercent;
					$categoryIds = $prod->getCategoryIds();
					if (isset($categoryIds[0])){
						$parentCategory="";
						$_categoryids = array();
						$_parentCategoryids = array();
	
						$productArray["category_info"] = array();
						foreach ( $categoryIds as $_category ) {
							$category = Mage::getModel('catalog/category')->load($_category);
							$cat_path = $category->getPath();
							$productArray["category_info"][] = $cat_path;
								
	
							$childCategories = $category->getChildrenCount();
							$parentCategory=$_category;
							if($childCategories == "0")
							{
								$_categoryids[] = $_category;
							}
							else {
								$_parentCategoryids [] = $parentCategory;
							}
	
						}
	
						if(count($_categoryids)==0 && $parentCategory != "" ){
							$_categoryids[] = $parentCategory;
						}
						if(count($_categoryids) > 0)
						{
							$productArray ["category_id"] = $_categoryids[0];
							$_categoryids = array_merge($_categoryids,$_parentCategoryids);
							$productArray ["category_ids"] = Mage::helper ( 'core' )->jsonDecode(Mage::helper ( 'core' )->jsonEncode( $_categoryids));
							//$productArray ['root_category_id'] = Mage::helper('retail_analytics/categoryservices')->getRootCatId($productArray ["category_id"]);
								
						}
					}
	
					$data [] = $productArray;
				}
	
	
				$retObj ['data'] = $data;
				$retObj ['lastdate'] = $lastdatesend;
				if($lastdate != $lastdatesend  )
				{
					$lastidsend=0;
				}
				else if($RowCount<$limit)
				{
					$lastidsend=0;
					$duration = 1;
					$dateinsec = strtotime($lastdatesend);
					$lastdatesend = $dateinsec+$duration;
					$lastdatesend = date('Y-m-d H:i:s',$lastdatesend);
					$retObj ['lastdate'] = $lastdatesend;
				}
				$retObj ['lastid'] = $lastidsend;
				if ($data_collection->count () < $limit && $lastid==0)
				{
					$retObj ['islast'] = true;
				}
				else
				{
					$retObj ['islast'] = false;
				}
			} else {
				$duration = 1;
				$dateinsec = strtotime($lastdate);
				$lastdatesend = $dateinsec+$duration;
				$lastdatesend = date('Y-m-d H:i:s',$lastdatesend);
				$retObj ['lastdate'] = $lastdatesend;
				$retObj ['lastid'] = 0;
				if($lastid>0)
				{
					$retObj ['islast'] = false;
				}
				else
					$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
			return $retObj;
		} catch ( Exception $e ) {
			$retObj ['lastid'] = -1;
			$retObj ['lastdate'] = $lastdate;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}
		
	public function raaProductAttributesByDate($lastdate,$lastid,$limit,$skip=null) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		$RowCount = 0;
		if($skip == null )
		{
			$skip = array();
		}
	
		try {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			$attributes = explode ( ",", $raaHelper->getAttributes () );
	
			$data_collection = Mage::getModel ( 'catalog/product' )->getCollection ();
	
			if($lastid==0)
			{
				$data_collection = $data_collection->addAttributeToFilter('updated_at', array('gteq' => $lastdate))
				->addAttributeToFilter('entity_id', array('gt' => $lastid))
				->addAttributeToSelect ( "*" )
				->addAttributeToSort("updated_at","ASC")
				->addAttributeToSort("entity_id","ASC");
				$data_collection->getSelect ()->limit ( $limit );
				$RowCount = $data_collection->count ();
	
			}
			else
			{
	
				$RowCount = Mage::getModel ( 'catalog/product' )->getCollection ()
				->addAttributeToFilter('updated_at', array('eq' => $lastdate))
				->addAttributeToFilter('entity_id', array('gt' => $lastid))
				->count ();
				$data_collection = $data_collection->addAttributeToFilter('updated_at', array('eq' => $lastdate))
				->addAttributeToFilter('entity_id', array('gt' => $lastid))
				->addAttributeToSelect ( "*" )
				->addAttributeToSort("entity_id","ASC");
				$data_collection->getSelect ()->limit ( $limit );
				//$RowCount = $data_collection->count ();
	
			}
	
			
			$helperdata = Mage::helper('retail_analytics/data');
			if ($data_collection->count () > 0) {
				$lastdatesend = $data_collection->getLastItem ()->getUpdatedAt();
				$lastidsend=$data_collection->getLastItem()->getId();
				foreach ( $data_collection as $product ) {
	
				
					$prod = Mage::getModel ( 'catalog/product' )->load ( $product->getId () );
					$productArray = array ();
					$productArray ['product_id'] = $product->getData ( 'entity_id' );
	
					$attributes = $prod->getAttributes();
					foreach ($attributes as $attribute) {
						$attributeCode = $attribute->getAttributeCode();
						if (!in_array($attributeCode,$skip))
						{
							$productArray [$attribute->getAttributeCode() ] = $attribute->getFrontend()->getValue($prod);
						}
					}
	
					$data [] = $productArray;
				}
	
	
				$retObj ['data'] = $data;
				$retObj ['lastdate'] = $lastdatesend;
				if($lastdate != $lastdatesend  )
				{
					$lastidsend=0;
				}
				else if($RowCount<$limit)
				{
					$lastidsend=0;
					$duration = 1;
					$dateinsec = strtotime($lastdatesend);
					$lastdatesend = $dateinsec+$duration;
					$lastdatesend = date('Y-m-d H:i:s',$lastdatesend);
					$retObj ['lastdate'] = $lastdatesend;
				}
				$retObj ['lastid'] = $lastidsend;
				if ($data_collection->count () < $limit && $lastid==0)
				{
					$retObj ['islast'] = true;
				}
				else
				{
					$retObj ['islast'] = false;
				}
			} else {
				$duration = 1;
				$dateinsec = strtotime($lastdate);
				$lastdatesend = $dateinsec+$duration;
				$lastdatesend = date('Y-m-d H:i:s',$lastdatesend);
				$retObj ['lastdate'] = $lastdatesend;
				$retObj ['lastid'] = 0;
				if($lastid>0)
				{
					$retObj ['islast'] = false;
				}
				else
					$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
			return $retObj;
			//return array ();
		} catch ( Exception $e ) {
			$retObj ['lastid'] = -1;
			$retObj ['lastdate'] = $lastdate;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
			//return array ();
		}
	}
	
	public function syncProducts($lastId) {
	
		$retObj = array ();
		$prod_limit=100;
		try {
			$collection_of_products = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
					"gt" => $lastId
			) )->addAttributeToSelect ( "sku" );
	
			$collection_of_products->getSelect()->limit ($prod_limit);
			$model = Mage::getModel('catalog/product');
	
			if ($collection_of_products->count () > 0) {
				$returnProductArray = array ();
				$urlArray=array();
				$imageurlArray=array();
				$retId = $collection_of_products->getLastItem ()->getId ();
	
				foreach ( $collection_of_products as $product ) {
					$returnProduct=array();
					$product_id=$product->getId();
					$prod = $model->load($product_id);
					$returnProduct['product_id']=$product_id;
					$returnProduct['sku']= $product->getSku ();
					$returnProduct['url']= $product->getProductUrl();
					$imageurl = $product->getImage();
					if($imageurl != "no_selection" && $imageurl != null)
					{
						$productArray ['imageurl'] = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $imageurl;
					}
					$returnProduct["instock"] =  $prod->getIsInStock ();
					$returnProduct["itemqty"] =  $prod->getStockItem()->getQty();
					$returnProductArray[]=$returnProduct;
				}
				$retObj ['product'] = $returnProductArray;
				$retObj ['lastid'] = $retId;
	
				if($collection_of_products->count() < $prod_limit){
					$retObj ['islast'] =true;
				}
				else{
					$retObj ['islast'] =false;
				}
	
			} else {
				$retObj ['lastid'] = $lastId;
				$retObj ['islast'] =true;
			}
	
			return $retObj;
				
		} catch ( Exception $e ) {
			$retObj ['lastid'] = - 1;
			$retObj ['islast'] =false;
			return $retObj;
		}
	}

    public function raaProductCatalogDiscount($lastId, $limit) {
	$retObj = array ();
	$data = array ();
	$new_lastId = 0;
	try {
		$raaHelper = Mage::helper ( 'retail_analytics' );
		$attributes = explode ( ",", $raaHelper->getAttributes () );
		Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
		$data_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
				"gt" => $lastId
		));
		$data_collection->getSelect ()->limit ( $limit );
		

		if ($data_collection->count () > 0) {
			$lastId = $data_collection->getLastItem ()->getId ();
			foreach ( $data_collection as $prod ) {
				
				$product = Mage::getModel ( 'catalog/product' )->load ($prod->getId () );
				//var_dump($prod);
				$productArray = array ();
				$productArray ['productid'] = $product->getData ( 'entity_id' );
				$productArray ['product_id'] = $product->getData ( 'entity_id' );
				$productArray ['price'] =$product->getPrice();
				$catalogPrice = Mage::getModel('catalogrule/rule')->calcProductPriceRule($product,$product->getPrice());
				$productArray ['catalogprice'] = $catalogPrice;
				$data [] = $productArray;
			}

			$retObj ['data'] = $data;
			$retObj ['lastid'] = $lastId;
			if ($data_collection->count () < $limit) {
				$retObj ['islast'] = true;
			} else {
				$retObj ['islast'] = false;
			}
		} else {
			$retObj ['lastid'] = $lastId;
			$retObj ['islast'] = true;
			$retObj ['data'] = $data;
		}

		return $retObj;
	} catch ( Exception $e ) {

		$retObj ['lastid'] = - 1;
		$retObj ['islast'] = false;
		$retObj ['data'] = $data;
		return $retObj;
	}
}

}