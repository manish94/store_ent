<?php
class Retail_Analytics_Helper_Customerservices extends Mage_Core_Helper_Abstract {
	
	
	public function raaCustomerGroups() {
		$data = array ();
		try {
			$groups = Mage::getModel('customer/group')->getCollection();
			foreach($groups as $group)
			{
				$data[] =  $group->getData ();
			}
			return $data;
		} catch ( Exception $ex ) {
			return $data;
		}
	}
	
	public function raaCustomers($lastId, $limit) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		try {
			$data_collection = Mage::getModel ( 'customer/customer' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
					"gt" => $lastId
			) )->addAttributeToSelect ( "*" );
			$data_collection->getSelect ()->limit ( $limit );
				
			if ($data_collection->count () > 0) {
				$lastId = $data_collection->getLastItem ()->getId ();
				foreach ( $data_collection as $customer ) {
					$customerArray = array ();
					$customerArray ['entity_id'] = $customer->getData ( 'entity_id' );
					$customerArray ['customer_id'] = $customer->getData ( 'entity_id' );
					$customerArray ['firstname'] = $customer->getData ( 'firstname' );
					$customerArray ['lastname'] = $customer->getData ( 'lastname' );
					$customerArray ['email'] = $customer->getData ( 'email' );
					$customerArray ['phone'] = $customer->getData ( 'phone' );
					$customerArray ['group_id'] = $customer->getData ( 'group_id' );
					$customerArray ['created_at'] = $customer->getData ( 'created_at' );
					$customerArray ['updated_at'] = $customer->getData ( 'updated_at' );
					$customerArray ['website_id'] = $customer->getData ( 'website_id' );
					$data [] = $customerArray;
				}
	
				$retObj ['data'] = $data;
				$retObj ['lastid'] = $lastId;
				if ($data_collection->count () < $limit) {
					$retObj ['islast'] = true;
				} else {
					$retObj ['islast'] = false;
				}
			} else {
				$retObj ['lastid'] = $lastId;
				$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
				
			return $retObj;
		} catch ( Exception $e ) {
				
			$retObj ['lastid'] = - 1;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}
	
	public function raaCustomerById($id) {
		$customerArray = array ();
		try{
			$customer = Mage::getModel('customer/customer')->load($id);
			if($customer)
			{
				$customer_id = $customer->getData ( 'entity_id' );
				if($customer_id !=null)
				{
					$customerArray ['entity_id'] = $customer_id;
					$customerArray ['customer_id'] = $customer_id;
					$customerArray ['firstname'] = $customer->getData ( 'firstname' );
					$customerArray ['lastname'] = $customer->getData ( 'lastname' );
					$customerArray ['email'] = $customer->getData ( 'email' );
					$customerArray ['phone'] = $customer->getData ( 'phone' );
					$customerArray ['group_id'] = $customer->getData ( 'group_id' );
					$customerArray ['created_at'] = $customer->getData ( 'created_at' );
					$customerArray ['updated_at'] = $customer->getData ( 'updated_at' );
					$customerArray ['website_id'] = $customer->getData ( 'website_id' );
				}
			}
			return $customerArray;
		}
	
		catch (Exception $ex)
		{
			return $customerArray;
		}
	
	}
	
	public function raaSubscribers($lastId, $limit) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		try {
			$data_collection = Mage::getModel ( 'newsletter/subscriber' )->getCollection ()->addFieldToFilter ( 'subscriber_id', array (
					"gt" => $lastId
			) );
			$data_collection->getSelect ()->limit ( $limit );
	
			if ($data_collection->count () > 0) {
				$lastId = $data_collection->getLastItem ()->getId ();
				foreach ( $data_collection as $subscriber ) {
	
					$subscriberArray = array ();
					$subscriberArray['subscriber_id'] = $subscriber->getData ('subscriber_id');
					$subscriberArray['store_id'] = $subscriber->getData ('store_id');
					$subscriberArray['customer_id'] = $subscriber->getData ('customer_id');
					$subscriberArray['email'] = $subscriber->getData ('subscriber_email');
					$subscriberArray['issubscribed'] = ($subscriber->getData ('subscriber_status') == "1")?true:false;
					$data [] = $subscriberArray;
				}
	
				$retObj ['data'] = $data;
				$retObj ['lastid'] = $lastId;
				if ($data_collection->count () < $limit) {
					$retObj ['islast'] = true;
				} else {
					$retObj ['islast'] = false;
				}
			} else {
				$retObj ['lastid'] = $lastId;
				$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
	
			return $retObj;
		} catch ( Exception $e ) {
	
			$retObj ['lastid'] = - 1;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}
}