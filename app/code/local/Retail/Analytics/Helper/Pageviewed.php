<?php

class Retail_Analytics_Helper_Pageviewed extends Mage_Core_Helper_Abstract
{
	public function getPageTitle($pageid) {
		switch ($pageid)
		{
			case 1:
				return "Home Page";
				break;
			case 2:
				return "Account Page";
				break;
			case 3:
				return "Product Page";
				break;
			case 4:
				return "Cart Page";
				break;
			case 5:
				return "Category Page";
				break;
			case 6:
				return "Oreder Confirm";
				break;
			case 10:
				return "Clicked";
				break;
		}
	}
		
	public function savePageViewed($pageid) {
			$viewedtotal = 0;			
			//$date = new Zend_Date();
			//$date->getDate()->get('Y-MM-dd 00:00:00');
			$today = date("Y-m-d 00:00:00");
			$data_collection = Mage::getModel ( 'retail_analytics/pageview' )->getCollection ()->addFieldToFilter ( 'created', array ($today ) )->addFieldToFilter( 'pageid', array($pageid) );
			if ($data_collection->count() > 0){
				$id = $data_collection->getFirstItem()->getId();	
				$pageViewed = Mage::getModel ( 'retail_analytics/pageview' )->load($id);
				$viewedtotal = $pageViewed->getViewedtotal();
				$pageViewed->setPageid($pageid);
				$pageViewed->setTitle($this->getPageTitle($pageid));
				$pageViewed->setStatus('Active');				
				$pageViewed->setViewedtotal($viewedtotal+1);
				$pageViewed->setCreated($today);
				$pageViewed->save();
			}
			else {
				$pageViewed = Mage::getModel ( 'retail_analytics/pageview' );
				$pageViewed->setPageid($pageid);
				$pageViewed->setTitle($this->getPageTitle($pageid));
				$pageViewed->setStatus('Active');			
				$pageViewed->setViewedtotal($viewedtotal+1);
				$pageViewed->setCreated($today);
				$pageViewed->save();
			}
	}	
	
	
	
	
	public function getPageViewed($lastdate) {
		
			$retObj = array ();
			$data = array ();	
			try {	
				
			$today = date("Y-m-d 00:00:00");
			$data_collection = Mage::getModel ( 'retail_analytics/pageview' )->getCollection ();
			
			if($lastdate == "0")
			{
				$data_collection->addFieldToFilter ( 'created', array ('lt'=>$today) );				
			}
			else 
			{
				$data_collection->addFieldToFilter ( 'created', array ($lastdate) );
			}
			
			if ($data_collection->count() > 0){
				foreach ($data_collection as $pageview){
					$data[] = $pageview->getData();					
				}
				
				$retObj ['data'] = $data;
				$retObj ['lastdate'] = $today;
			}	
			
			return $retObj;
			} catch ( Exception $e ) {
			
				$retObj ['lastdate'] = $today;			
				$retObj ['data'] = $data;
				return $retObj;
			}		
	}	
	
	
}