<?php

class Retail_Analytics_Helper_Onsiteconfig extends Mage_Core_Helper_Abstract
{
		
	public function saveOnsiteconfig($data) {		
		try {
		$updateconfig = Mage::getModel('retail_analytics/onsiteconfig');
		
		$onsiteconfig_collection = Mage::getModel('retail_analytics/onsiteconfig')->getCollection ()
		->addFieldToFilter ( 'slotid', array ($data['slotid']) )
		->addFieldToFilter ( 'pageid', array ($data['pageid']) );		
		
		if($onsiteconfig_collection->count() > 0) {
			$updateconfig = $onsiteconfig_collection->getFirstItem();			
		}	
		
		$ipenable = false;
		if (array_key_exists("ipenable",$data)){
			$ipenable = ( $data['ipenable'] == "true" || $data['ipenable'] == "TRUE")?true:false;		
		}
		
		$enable = false;
		if (array_key_exists("enable",$data)){
			$enable = ( $data['enable'] == "true" || $data['enable'] == "TRUE")?true:false;
		}	
		
		$updateconfig->setSlotid($data['slotid']);
		$updateconfig->setPageid($data['pageid']);
		$updateconfig->setSlotname($data['slotname']);	
		$updateconfig->setTitle($data['title']);
		$updateconfig->setRas($data['ras']);
		$updateconfig->setPriority($data['priority']);
		$updateconfig->setNoofproduct($data['noofproduct']);			
		$updateconfig->setPbrurl($data['pbrurl']);
		$updateconfig->setIpenable($ipenable);
		$updateconfig->setEnable($enable);
		$updateconfig->setTimeduration($data['timeduration']);
		$updateconfig->setCreated(now());
		$updateconfig->setModified(now());
		$updateconfig->save();
		return true;
		}
		catch ( Exception $e ) {
			return false;
		}
	}	
	
	
	public function addOnsiteconfig($data) {
	
		try {	
					
			$result = $this->saveOnsiteconfig($data);				
			return $result;
		}
		catch ( Exception $e ) {
			return false;
		}
		
	}
	
	
	
	public function deleteOnsiteconfig($data) {
	
		$onsiteconfig_collection = Mage::getModel('retail_analytics/onsiteconfig')->getCollection ()
		->addFieldToFilter ( 'slotid', array ($data['slotid']) )
		->addFieldToFilter ( 'pageid', array ($data['pageid']) );
		
		if($onsiteconfig_collection->count() > 0) {
			$deleteconfig = $onsiteconfig_collection->getFirstItem();		
			$deleteconfig->delete();
		}	
		
	}
	
	
	public function removeOnsiteconfig($data) {
	
		try {	
				$this->deleteOnsiteconfig( $data);	
			
			return true;
		}			
		catch ( Exception $e ) {
			return false;
		}		
		return true;
	}	
	
	
	public function updateOnsiteconfig($pageid,$enable) {
		
		try {
			
			$onsiteconfigs = Mage::getModel('retail_analytics/onsiteconfig')->getCollection()
			->addFieldToFilter ( 'pageid', array ($pageid) );
			
			foreach($onsiteconfigs as $onsiteconfig)
			{				
					$dataArray = array();
					$updateconfig = Mage::getModel('retail_analytics/onsiteconfig')->load($onsiteconfig->getData('id'));
					$updateconfig->setEnable($enable);
					$updateconfig->setModified(now());
					$updateconfig->save();			
			}
			return true;
			
		} catch ( Exception $ex ) {
			return false;
		}
	}
	
}