<?php
class Retail_Analytics_Helper_Categoryservices extends Mage_Core_Helper_Abstract {
	
	public function setRootCatMapRegistry() {
		$cat_map = array ();
		try {
			$categories = Mage::getModel ( 'catalog/category' )->getCollection ()->addFieldToFilter ( 'parent_id', array (
					1
			) )->addAttributeToSelect ( "*" );
			foreach ( $categories as $category ) {
				$root_catid = $category->getId ();
				$rootcategoryModel = Mage::getModel ( 'catalog/category' );
				$rootcategoryModel = $rootcategoryModel->load ( $root_catid );
				$subcats = $rootcategoryModel->getChildren ();
				$cat_map [$root_catid] = $root_catid;
	
				foreach ( explode ( ',', $subcats ) as $subCatid ) {
					$_category = Mage::getModel ( 'catalog/category' )->load ( $subCatid );
					$cat_map [$subCatid] = $root_catid;
					$sub_cat = Mage::getModel ( 'catalog/category' )->load ( $_category->getId () );
					$sub_subcats = $sub_cat->getChildren ();
					foreach ( explode ( ',', $sub_subcats ) as $sub_subCatid ) {
						$_sub_category = Mage::getModel ( 'catalog/category' )->load ( $sub_subCatid );
						$cat_map [$sub_subCatid] = $root_catid;
						$sub_sub_cat = Mage::getModel ( 'catalog/category' )->load ( $sub_subCatid );
						$sub_sub_subcats = $sub_sub_cat->getChildren ();
						foreach ( explode ( ',', $sub_sub_subcats ) as $sub_sub_subCatid ) {
							$_sub_sub_category = Mage::getModel ( 'catalog/category' )->load ( $sub_sub_subCatid );
							$cat_map [$sub_sub_subCatid] = $root_catid;
							$sub_sub_sub_subcats = $_sub_sub_category->getChildren ();
							foreach ( explode ( ',', $sub_sub_sub_subcats ) as $sub_sub_sub_subCatid ) {
								$_sub_sub_sub_category = Mage::getModel ( 'catalog/category' )->load ( $sub_sub_sub_subCatid );
								$cat_map [$sub_sub_sub_subCatid] = $root_catid;
							}
						}
					}
				}
			}
			$key = "raa_root_cat_map";
			Mage::unregister ( $key );
			Mage::register ( $key, $cat_map );
			return true;
		} catch ( Exception $e ) {
			return true;
		}
	}
	
	public function getRootCatId($catid) {
		$key = "raa_root_cat_map";
		$rootCatId;
		$cat_map = Mage::registry ( $key );
		if ($cat_map == null) {
			$this->setRootCatMapRegistry ();
			$cat_map = Mage::registry ( $key );
		}
		try {
			$rootCatId = $cat_map [$catid];
		} catch ( Exception $e ) {
			return 0;
		}
	
		return $rootCatId;
	}
	
	public function raaCategories($lastId, $limit) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		try {
	
			$categories = Mage::getModel('catalog/category')->getCollection()->addFieldToFilter ( 'entity_id', array (
					"gt" => $lastId
			) )->addAttributeToSelect ( "*" );
				
			$categories->getSelect ()->limit ( $limit );
			if ($categories->count () > 0) {
				$lastId = $categories->getLastItem ()->getId ();
				foreach($categories as $category)
				{
					$dataArray ["category_id"] = $category->getData ( 'entity_id' );
					$attributes = $category->getData();
					foreach ($attributes as $key=>$value)
					{
						$dataArray [$key] = $value;
					}
	
					$data[] = $dataArray;
				}
	
				$retObj ['data'] = $data;
				$retObj ['lastid'] = $lastId;
				if ($categories->count () < $limit) {
					$retObj ['islast'] = true;
				} else {
					$retObj ['islast'] = false;
				}
			} else {
				$retObj ['lastid'] = $lastId;
				$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
	
			return $retObj;
	
		} catch ( Exception $ex ) {
			$retObj ['lastid'] = - 1;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}
	
	public function getSubCategories($parentId, $data) {
		try {
			$categoryModel = Mage::getModel ( 'catalog/category' );
			$subCategories = $categoryModel->getCategories ( $parentId );
			foreach ( $subCategories as $category ) {
				$dataArray = array ();
				$catId = $category->getData ( 'entity_id' );
				$dataArray ["category_id"] = $catId;
				$dataArray ["name"] = $category->getData ( 'name' );
				$dataArray ["parent_id"] = $category->getData ( 'parent_id' );
				$data[] = $dataArray;
				if($categoryModel->getCategories ($catId)->count() > 0 )
				{
					$data = $this->getSubCategories($catId,$data);
				}
			}
			return $data;
		}
		catch ( Exception $ex ) {
			return $data;
		}
	}
	
}