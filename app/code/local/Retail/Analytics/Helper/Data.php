<?php

class  Retail_Analytics_Helper_Data extends Mage_Core_Helper_Abstract {

	/**
	 * Path to get client specific resource directory .
	 */
	const XML_PATH_RESOURCEDIR = 'retail_analytics/settings/resourcedir';
	
	
	/**
	 * Path to cached image width .
	 */
	const XML_PATH_IMAGE_WIDTH = 'retail_analytics/settings/imagewidth';
	
	/**
	 * Path to cached image height .
	 */
	const XML_PATH_IMAGE_HEIGHT = 'retail_analytics/settings/imageheight';
	
	/**
	 * Path to retailreco image type.
	 */
	const XML_PATH_IMAGE_TYPE = 'retail_analytics/settings/imagetype';
	
	/**
	 * Path to retailreco plugin version.
	 */
	const XML_PATH_VERSION = 'retail_analytics/settings/version';
	
	
	 /**
     * Path to store config raa module enabled state.
     */
    const XML_PATH_ENABLED = 'retail_analytics/settings/enabled';

    /**
     * Path to store config raa service server address.
     */
    const XML_PATH_SERVER = 'retail_analytics/settings/server';   
     
    /**
     * Path to store config raa service server address.
     */
    const XML_PATH_ALLOW_IP = 'retail_analytics/settings/allowip';
    
    /**
     * Path to store config raa service account name.
     */
    const XML_PATH_ACCOUNT = 'retail_analytics/settings/account';

    /**
     * Path to store config raa service prediction.
     */
    const XML_PATH_PREDICTION = 'retail_analytics/settings/prediction';
    
    /**
     * Path to the store config collect_email_addresses option.
     */
    const XML_PATH_COLLECT_EMAIL_ADDRESSES = 'retail_analytics/tagging_options/collect_email_addresses';
    
    /**
     * Path to store config raa service customer.
     */
    const XML_PATH_CUSTOMER = 'retail_analytics/settings/customer';
    /**
     * Path to store config raa service product.
     */
    const XML_PATH_PRODUCT = 'retail_analytics/settings/product';
    /**
     * Path to store config raa service attribute.
     */
    const XML_PATH_ATTRIBUTES = 'retail_analytics/settings/attributes';
    /**
     * Path to store config raa service cart.
     */
    const XML_PATH_CART = 'retail_analytics/settings/cart';
    
    /**
     * RAA SERVICES PROJECT NAME.
     */
    public $raa_service = "RAService";
    const XML_PATH_RASERVICE = 'retail_analytics/settings/raservice';
    
    /**
     * RAA Admin PROJECT NAME.
     */
    public $raa_admin = "RAAdmin";
    const XML_PATH_RAADMIN = 'retail_analytics/settings/raadmin';
    
    public function getRaaconfig($key) {
    
    	$value = "";
    	try {
    
    		$raaconfig_collection = Mage::getModel('retail_analytics/raaconfig')->getCollection ()->addFieldToFilter ( 'raakey', array ($key) );
    		if($raaconfig_collection->count() > 0) {
    			$value = $raaconfig_collection->getFirstItem()->getRaavalue();
    		}    		 
    	
    	}
    	catch ( Exception $e ) {
    		return $value;
    	}
    
    	return $value;
    }
   
    public function raaJsonDecode($json) {  
    	  		
    	$data = array();
        try   {        	
        	$data =  Mage::helper( 'core' )->jsonDecode($json);
        	
        }
        catch(Exception $e)
        {
        	$data =  json_decode($json);
        }  
        
        return  $data ;
    	
    }
    
    
    public function getRAAServicesName() {
    	//return $this->raa_service;
    	return $this->getRaaconfig("raservice");
    }
    
  
    
    public function getRAAdminName() {
    	//return $this->raa_admin;
    	return $this->getRaaconfig("raadmin");
    }
    
    
    
	/**
	 * Check if module exists and enabled in global config.
	 * Also checks if the module is enabled for the current store and if the needed criteria has been provided for the
	 * module to work.
	 *
	 * @param string $moduleName the full module name, example Mage_Core
	 *
	 * @return boolean
	 */
 	public function isModuleEnabled($moduleName = null)
    {
        if (!parent::isModuleEnabled($moduleName) || !$this->getEnabled() || !$this->getAccount()) {
            return false;
        }

        return true;
    }
    
    public function raaModulesStatus() {
    	$data = array();
    	try{
    		
	    	$data['Retail_Analytics'] = Mage::helper('core')->isModuleEnabled('Retail_Analytics');
	    	$data['Retail_Coupon'] = Mage::helper('core')->isModuleEnabled('Retail_Coupon');
	    	$data['Retail_Log'] = Mage::helper('core')->isModuleEnabled('Retail_Log');	 
	    	$data['Predictive Email Enable'] = $this->getEnabled();
	    	return $data;
    	}
    	catch ( Exception $e ) {
    		echo false;
    	}
    
    }
    
    
    /**
     * Builds a tagging string of the given category including all its parent categories.
     * The categories are sorted by their position in the category tree path.
     *
     * @param Mage_Catalog_Model_Category $category
     *
     * @return string
     */
    public function buildCategoryString($category)
    {
    	$data = array();
    
    	if ($category instanceof Mage_Catalog_Model_Category) {
    		/** @var $categories Mage_Catalog_Model_Category[] */
    		$categories = $category->getParentCategories();
    		$path = $category->getPathInStore();
    		$ids = array_reverse(explode(',', $path));
    		foreach ($ids as $id) {
    			if (isset($categories[$id]) && $categories[$id]->getName()) {
    				$data[] = $categories[$id]->getName();
    			}
    		}
    	}
    
    	if (!empty($data)) {
    		return DS . implode(DS, $data);
    	} else {
    		return '';
    	}
    }
    
    /**
     * Formats date into raa format, i.e. Y-m-d.
     *
     * @param string $date
     *
     * @return string
     */
    public function getFormattedDate($date)
    {
    	return date('Y-m-d', strtotime($date));
    }
    
    
    public function getImageType($store = null)
    {
    	//return Mage::getStoreConfig(self::XML_PATH_IMAGE_TYPE, $store);
    	$imagetype = $this->getRaaconfig("imagetype");
    	$imagetype = ($imagetype == "")?"small_image":$imagetype;
    	return $imagetype;
    }    
    
    public function getImageWidth($store = null)
    {    	  	
    	$imagewidth = $this->getRaaconfig("imagewidth");
    	if($imagewidth == "") {
    		$imagewidth = Mage::getStoreConfig(self::XML_PATH_IMAGE_WIDTH, $store);
    	}
    	
    	return $imagewidth;
    }
    
    public function getImageHeight($store = null)
    {    	
    	$imageheight = $this->getRaaconfig("imageheight");
    	if($imageheight == "") {
    		$imageheight =  Mage::getStoreConfig(self::XML_PATH_IMAGE_HEIGHT, $store);
    	}
    	return $imageheight;
    }
    
    public function getPageImageWidth($page)
    {
    	$imagewidth = $this->getRaaconfig("imagewidth_".$page);
    	if($imagewidth == "") {
    		$imagewidth = $this->getImageWidth();
    	}
    	 
    	return $imagewidth;
    }
    
    public function getPageImageHeight($page)
    {
    	$imageheight = $this->getRaaconfig("imageheight_".$page);
    	if($imageheight == "") {
    		$imageheight = $this->getImageHeight();
    	}
    	return $imageheight;
    }
    
    
  
    
    public function getResourcedir($store = null)
    {
    	$resourcedir = $this->getRaaconfig("resourcedir");
    	if($resourcedir == "") {
    		$resourcedir = Mage::getStoreConfig(self::XML_PATH_RESOURCEDIR, $store);
    	}    
    		    	
    	return $resourcedir;
    }
    
    /**
     * Return plugin vertion.
     *
     * @param mixed $store
     *
     * @return string
     */
    public function getVersion($store = null)
    {
    	return Mage::getStoreConfig(self::XML_PATH_VERSION, $store);
    }
	
	/**
	 * Return if the module is enabled.
	 *
	 * @param mixed $store
	 *
	 * @return boolean
	 */
	public function getEnabled($store = null)
	{
		return Mage::getStoreConfig(self::XML_PATH_ENABLED, $store);
	}
	
	/**
	 * Return the server address to the raa service.
	 *
	 * @param mixed $store
	 *
	 * @return string
	 */
	public function getServer($store = null)
	{
		$server =  $this->getRaaconfig("server");
		if($server == "") {
			$server = Mage::getStoreConfig(self::XML_PATH_SERVER, $store);
		}
		
		return $server;
	}
	
	/**
	 * Return the account name that is used by this store to access the raa service.
	 *
	 * @param mixed $store
	 *
	 * @return string
	 */
	public function getAccount($store = null)
	{
		$account = $this->getRaaconfig("account");
		if($account == "")
		{
			$account = Mage::getStoreConfig(self::XML_PATH_ACCOUNT, $store);
		}
		return $account;
	}
	
	/**
	 * Return if customer email addresses should be collected.
	 *
	 * @param mixed $store
	 *
	 * @return boolean
	 */
	public function getCollectEmailAddresses($store = null)
	{
		return (boolean)Mage::getStoreConfig(self::XML_PATH_COLLECT_EMAIL_ADDRESSES, $store);
	}
	/**
	 * Return the account name that is used by this store to access the raa service.
	 *
	 * @param mixed $store
	 *
	 * @return string
	 */
	public function getCustomer($store = null)
	{
		return Mage::getStoreConfig(self::XML_PATH_CUSTOMER, $store);
	}
	/**
	 * Return the account name that is used by this store to access the raa service.
	 *
	 * @param mixed $store
	 *
	 * @return string
	 */
	public function getProduct($store = null)
	{
		return Mage::getStoreConfig(self::XML_PATH_PRODUCT, $store);
	}
	/**
	 * Return the account name that is used by this store to access the raa service.
	 *
	 * @param mixed $store
	 *
	 * @return string
	 */
	public function getCart($store = null)
	{
		return Mage::getStoreConfig(self::XML_PATH_CART, $store);
	}
	/**
	 * Return the product attributes name that is used by this store to access the raa service.
	 *
	 * @param mixed $store
	 *
	 * @return string
	 */
	public function getAttributes($store = null)
	{
		return Mage::getStoreConfig(self::XML_PATH_ATTRIBUTES, $store);
	}
	
	/**
	 * Return the prediction value that is used by this store to access the raa service.
	 *
	 * @param mixed $store
	 *
	 * @return boolean
	 */
	public function getPrediction($store = null)
	{
		//return Mage::getStoreConfig(self::XML_PATH_PREDICTION, $store);
		$prediction = $this->getRaaconfig("prediction");
		$prediction = ($prediction == "")?true:$prediction;
		return $prediction;
	}
		
	
	function raaValidateIP($store = null) {
		try {
			
			$allowip  = $this->getRaaconfig("allowip");
			if($allowip == "")
			{
				$allowip = Mage::getStoreConfig(self::XML_PATH_ALLOW_IP, $store);
			}
			
			if($allowip == "" || $allowip == null)
			{	
				return true;
			} 
			else 
			{
				$allowipArray = explode(',', $allowip);			
				$ipaddress = $this->getExternalIP();			
				if (in_array($ipaddress, $allowipArray)) {
					return true;
				} else {
					return false;
				}
			}	
		} catch ( Exception $e ) {			
			return true;
		}
	}
	
	function raaValidateDefaultIP($store = null) {
		try {				
			
				$allowipArray = array("104.199.151.78","104.155.226.59","183.182.85.106");
				$ipaddress = $this->getExternalIP();
				if (in_array($ipaddress, $allowipArray)) {
					return true;
				} else {
					return false;
				}
			
		} catch ( Exception $e ) {
			return true;
		}
	}
	
	
	
	function getClientIP() {
		$ipaddress = '';
		$ipaddress = Mage::helper('core/http')->getRemoteAddr(false);
		return $ipaddress;
	}	
	function getDomainID() {
		$domainid = '';
		$domainid = $_SERVER['HTTP_HOST'];
		return $domainid;
	}
		
	
	public function validateServerAccount() {
		
		$result = "";
		
		try {			
			
			$url = '/api/rest/validate/';
			$raa_server = $this->getServer ();
			$raa_services = $this->getRAAServicesName();
			$raa_account = $this->getAccount ();
			$server_url = $raa_server .'/'.$raa_services.'/'. $url . $raa_account;
			$curl = curl_init ( $server_url );
			curl_setopt ( $curl, CURLOPT_HEADER, false );
			curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $curl, CURLOPT_HTTPHEADER, array ("Content-type",	"application/x-www-form-urlencoded") );					
					curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
					$result = curl_exec ( $curl );
					//$result = Mage::helper ( 'core' )->jsonDecode ( $result );
					$result =$this->raaJsonDecode( $result );
					Mage::log ( $server_url . ' response  : ' . Mage::helper ( 'core' )->jsonEncode( $result) );
					curl_close ( $curl );
					return $result;
		} catch ( Exception $e ) {
			$result = $e->getMessage() ;
			return $result;
		}
		
		
	}
	
	public function getCurrentCategory()
	{
		$categoryid = "";
		try {
		$current_category = Mage::registry('current_category');
		if($current_category != null)
		{
			$categoryid = $current_category->getId();
		}
		
		return $categoryid;
		} catch ( Exception $e ) {			 
			return $categoryid;
		}
	} 
	
	function getExternalIP() {
				
		$ipaddress = "";
		try {
			$ipaddress = Mage::helper('core/http')->getRemoteAddr(false);
			if($ipaddress == "" || $ipaddress == null) {
				$ipaddress = $_SERVER['REMOTE_ADDR'];
			}
			return $ipaddress;
		} catch ( Exception $e ) {
			return $ipaddress;
		}
	}
	
	public function getInternalIp()
	{
		$internalip = "999";
		try {
			$internalip = Mage::getModel('core/cookie')->get('rainternalip');
			if(($internalip == null || $internalip == "" ) && isset($_COOKIE['rainternalip'])) {
				$internalip = $_COOKIE['rainternalip'];
			}			
			$internalip = ($internalip == null || $internalip == "")?"999":$internalip;	
			return $internalip;
			
		} catch ( Exception $e ) {
			return $internalip;
		}
	}
	
	function setRas() {
		$params = Mage::app()->getRequest ()->getParams ();
		if (isset($params['ras']))
		{
			$racv = $this->getRaaConfig('racv');
			$racv = ($racv == "" || $racv == null)?1:intval($racv);
			Mage::getModel('core/cookie')->set('ras','retailreco', time()+(3600 * $racv));
		}
	}
	
	
	function getRas() {
		$ras = Mage::getModel('core/cookie')->get('ras');
		if(($ras == null || $ras == "" ) && isset($_COOKIE['ras'])) {
			$ras = $_COOKIE['ras'];
		}
		
		$ras = ($ras == null || $ras == "" )?"":$ras;
		return $ras;
	}
	
	function getModuleConfig($path,$store = null)
	{		
		//echo $path;
		echo Mage::getStoreConfig($path, $store);		
	}
	
	function showCheckAllowIP() {
		try {
	
			$allowip  = $this->getRaaconfig("allowip");
			if($allowip == "")
			{
				$allowip = Mage::getStoreConfig(self::XML_PATH_ALLOW_IP, $store);
			}
	
			
			if($allowip == "" || $allowip == null)
			{
				echo true;
			}
			else
			{
				$allowipArray = explode(',', $allowip);
				echo ' allowip : ' .var_dump($allowipArray).'<br>';
				$ipaddress = $this->getExternalIP();
				echo 'your ip : ' . $ipaddress;
	
				if (in_array($ipaddress, $allowipArray)) {
					echo true;
				} else {
					echo false;
				}
	
			}
		} catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
}