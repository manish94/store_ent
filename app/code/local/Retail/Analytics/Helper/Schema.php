<?php
class Retail_Analytics_Helper_Schema extends Mage_Core_Helper_Abstract {
	/**
	 * schema table name header.
	 */
	public $tableNameHeader = 'TABLE_NAME';
	/**
	 * schema data type header.
	 */
	public $dataTypeHeader = 'DATA_TYPE';
	/**
	 * schema field name header.
	 */
	public $fieldNameHeader = 'COLUMN_NAME';
	/**
	 * schema default data type.
	 */
	public $schemaDefaultDataType = 'VARCHAR';
	/**
	 * schema customer table name.
	 */
	public $customerTableName = 'Customer';
	/**
	 * schema product table name.
	 */
	public $productTableName = 'Product';
	/**
	 * schema order table name.
	 */
	public $orderTableName = 'Order';
	/**
	 * schema cart table name.
	 */
	public $cartTableName = 'Cart';
	/**
	 * schema browsing table name.
	 */
	public $browsingTableName = 'Browsing';
	public function getTableNameHeader() {
		return $this->tableNameHeader;
	}
	public function getDataTypeHeader() {
		return $this->dataTypeHeader;
	}
	public function getFieldNameHeader() {
		return $this->fieldNameHeader;
	}
	public function getDefaultDataType() {
		return $this->schemaDefaultDataType;
	}
	public function getCustomerTableName() {
		return $this->customerTableName;
	}
	public function getProductTableName() {
		return $this->productTableName;
	}
	public function getOrderTableName() {
		return $this->orderTableName;
	}
	public function getCartTableName() {
		return $this->cartTableName;
	}
	public function getBrowsingTableName() {
		return $this->browsingTableName;
	}
	public function getCustomerSchemaFields() {
		$fieldArray = array ();
		$columnArray = array (
				"customer_id",
				"firstname",
				"lastname",
				"email",
				"phone",
				"created_at",
				"updated_at"
		);
		foreach ( $columnArray as &$column ) {
			$fieldArray [] = array (
					$this->getTableNameHeader () => $this->getCustomerTableName (),
					$this->getDataTypeHeader () => $this->getDefaultDataType (),
					$this->getFieldNameHeader () => $column 
			);
		}
		return $fieldArray;
	}
	public function getProductSchemaFields() {
		$fieldArray = array ();
		$columnArray = array (
				"product_id",
				"sku",
				"name",
				"description",
				"price",
				"category_id",
				"url",
				"imageurl",
				"quantity",
				"created_at",
				"updated_at"
		);
		
		foreach ( $columnArray as &$column ) {
			$fieldArray [] = array (
					$this->getTableNameHeader () => $this->getProductTableName (),
					$this->getDataTypeHeader () => $this->getDefaultDataType (),
					$this->getFieldNameHeader () => $column 
			);
		}
		
		$raaHelper = Mage::helper('retail_analytics');
    	$attributes = explode(",", $raaHelper->getAttributes());    	
    	foreach ($attributes as $attribute){    
    		if (!in_array($attribute, $columnArray)) {    			
    			$fieldArray [] = array (
    				$this->getTableNameHeader () => $this->getProductTableName (),
    				$this->getDataTypeHeader () => $this->getDefaultDataType (),
    				$this->getFieldNameHeader () => $attribute
    			);
    		}		
    		
    	}
		
		return $fieldArray;
	}
	public function getOrderSchemaFields() {
		$fieldArray = array ();
		$columnArray = array (
				"order_id",
				"customer_id",
				"customer_email",
				"customer_gender",
				"customer_firstname",
				"customer_middlename",
				"customer_lastname",
				"customer_dob",
				"customer_is_guest",
				"customer_group_id",
				"product_id",
				"price",
				"quantity",
				"created_at",
				"updated_at"
		);
		foreach ( $columnArray as &$column ) {
			$fieldArray [] = array (
					$this->getTableNameHeader () => $this->getOrderTableName (),
					$this->getDataTypeHeader () => $this->getDefaultDataType (),
					$this->getFieldNameHeader () => $column 
			);
		}
		return $fieldArray;
	}
	public function getCartSchemaFields() {
		$fieldArray = array ();
		$columnArray = array (
				"customer_id",
				"product_id",
				"price",
				"quantity",
				"date" 
		);
		foreach ( $columnArray as &$column ) {
			$fieldArray [] = array (
					$this->getTableNameHeader () => $this->getCartTableName (),
					$this->getDataTypeHeader () => $this->getDefaultDataType (),
					$this->getFieldNameHeader () => $column 
			);
		}
		return $fieldArray;
	}
	public function getBrowsingSchemaFields() {
		$fieldArray = array ();
		$columnArray = array (
				"customer_id",
				"product_id",
				"sku",
				"name",
				"description",
				"price",
				"category",
				"url",
				"imageurl",
				"quantity",
				"date" 
		);
		foreach ( $columnArray as &$column ) {
			$fieldArray [] = array (
					$this->getTableNameHeader () => $this->getBrowsingTableName (),
					$this->getDataTypeHeader () => $this->getDefaultDataType (),
					$this->getFieldNameHeader () => $column 
			);
		}
		return $fieldArray;
	}
	public function getModelSchema() {
		$csvData = array ();
		try {
			
			$csvHeader = array (
					$this->getTableNameHeader (),
					$this->getDataTypeHeader (),
					$this->getFieldNameHeader () 
			);
			$csvData [] = implode ( ',', $csvHeader ) . "\n";
			
			$fields = $this->getCustomerSchemaFields();			
			foreach ( $fields as &$field ) {
				$csvData [] = implode ( ',', $field ) . "\n";
			}	
			
			$fields = $this->getProductSchemaFields();
			foreach ( $fields as &$field ) {
				$csvData [] = implode ( ',', $field ) . "\n";
			}
			
			$fields = $this->getOrderSchemaFields();
			foreach ( $fields as &$field ) {
				$csvData [] = implode ( ',', $field ) . "\n";
			}
			
			$fields = $this->getCartSchemaFields();
			foreach ( $fields as &$field ) {
				$csvData [] = implode ( ',', $field ) . "\n";
			}
			
			$fields = $this->getBrowsingSchemaFields();
			foreach ( $fields as &$field ) {
				$csvData [] = implode ( ',', $field ) . "\n";
			}
			
			
			return $csvData;
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );		
			return $csvData;	
		}
	}
}