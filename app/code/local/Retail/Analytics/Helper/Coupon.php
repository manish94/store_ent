<?php

class Retail_Analytics_Helper_Coupon extends Mage_Core_Helper_Abstract
{
	
	public function paramsAction()
	{
		foreach($this->getRequest()->getParams() as $key=>$value) {
			//$array=Mage::helper('core')->jsonDecode($value);
			$array = Mage::helper('retail_analytics')->raaJsonDecode($value);
			var_export($array);
			$this->processRecoCoupon($array);
	
		}
	}
	
	function getCustomerGroup($customer_email)
	{
		/* $customergroupmodel = Mage::getModel("customer/group");
		$customergroup=$customergroupmodel->getCollection()->addFieldToFilter('customer_group_code', $customergroup)->getData();
		var_export($customergroup); */
		
		$customer = Mage::getModel("customer/customer")->getCollection()->addAttributeToSelect("*")->addFieldToFilter('email',$customer_email)->getData();
    	$groupid = $customer[0]['group_id'];    
		return $groupid;
	
	}
	
	function getALLCustomerGroup(){
		$customergroupmodel = Mage::getModel("customer/group");
		$customergroups=$customergroupmodel->getCollection()->getData();
		//var_export($customergroups);
		$groupids=array();
		foreach ( $customergroups as $group){
			$groupid= $group['customer_group_id'];
			$groupids[]=$groupid;
		}
		return $groupids;
	
	}
	
	public function getAttrValue($attr,$value)
	{
	
		$attribute_code = $attr;
		$attribute_details = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attribute_code);
		$attribute = $attribute_details->getData();		
		$options = $attribute_details->getSource()->getAllOptions();
		Foreach($options as $option){
			if($option["label"] == $value ){
				return $option["value"];
			}
		}
		return NULL;
	}
	
	/*The below function creates sku conditions and actions*/
	function  processCoupon($reco)
	{
		try
		{   
			$name = $reco ['couponcode'];
			if($name==null || trim($name," ")=="")
			{
				return false;
			}
			$description = $name;
			if(!empty($reco ['validfrom']))
			{
			$fromdate = $reco ['validfrom'];
			}
			if(!empty($reco ['validto']))
			{
			$todate = $reco ['validto'];
			}
	
			if ($reco ['discounttype'] == 1) {
				$actionType = 'by_percent';
			} else {
				$actionType = 'by_fixed';
			}
				
			$discount = $reco ['discount'];
			$websiteId = 1;
			$usesPerCoupon = 1;
			$notLogedinGroupId = 0;
	
			//$groupcode=$reco['loyaltygroup'];	
			//$groupid = $group=Mage::getModel('customer/group')->load($groupcode, 'customer_group_code')->getCustomerGroupId();
			$groupids = array();
			$groupid = $this->getCustomerGroup( $reco['email'] );
			if($groupid==null || $groupid=="")
			{
				$groupids[] = 1;
				$groupids[] = 0;
			}
			else 
			{
				$groupids[] = $groupid;
			}
			$conditionProducs = array ();				
			/* foreach ( $reco['sku'] as $value ) {
				$conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ('sku')->setOperator ( '==' )->setValue ( $value );
				$conditionProducs [] = $conditionProduct;
			} */
			
			$atrribute=$reco['attribute'];
			if(strtolower($atrribute)== 'sku')
			{
				foreach ( $reco [$atrribute] as $value ) {
					$conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( 'sku' )->setOperator ( '==' )->setValue ( $value );
					$conditionProducs [] = $conditionProduct;
				}
			}
			else {
				$model = Mage::getModel('catalog/product');
				foreach ( $reco [$atrribute] as $value ) {
					$tempProd = $model->load($value);
					$tempSku=$tempProd->getSku ();
					$conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( 'sku' )->setOperator ( '==' )->setValue ( $tempSku );
					$conditionProducs [] = $conditionProduct;
				}
			}			
			
			$shoppingCartPriceRule = Mage::getModel('salesrule/rule');
				
			$temp=$shoppingCartPriceRule->setName ( $name )->setDescription ( $description )->setIsActive ( 1 )->setWebsiteIds ( array (
					$websiteId
			) )->setCustomerGroupIds ( 	$groupids	)
			->setSortOrder ( '' )->setSimpleAction ( $actionType )
			->setDiscountAmount ( $discount )
			->setStopRulesProcessing ( 0 )
			->setCoupon_code ( $name )
			->setCouponType ( Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC )
			->setUses_per_customer ( 1)->setUses_per_coupon ( $usesPerCoupon );
			
			if(!empty($fromdate)&& !empty($todate))
			{
			$temp->setFromDate ( $fromdate );
			$temp->setToDate ( $todate );
			}
				
			$conditionCart = Mage::getModel ( 'salesrule/rule_condition_product' )
			-> setType ( 'salesrule/rule_condition_address' )
			->setAttribute ( 'base_subtotal' )
			->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] );
			
			$conditionProductFound = Mage::getModel ( 'salesrule/rule_condition_product_found' )->setAggregator('any')->setConditions ( $conditionProducs );
				
			$condition = Mage::getModel ( 'salesrule/rule_condition_combine' )->setConditions ( array (
					$conditionProductFound,$conditionCart
			) );
				
			$actionconditionInner = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setAggregator('any')->setConditions ( $conditionProducs );
	
			/* $conditionProduct2 = Mage::getModel ( 'salesrule/rule_condition_product' )
			-> setType ( 'salesrule/rule_condition_product' )
			->setAttribute ( 'quote_item_row_total' )
			->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] ); */
			
			
			$actioncondition = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setConditions ( array (
					$actionconditionInner
					//,$conditionProduct2
			) );
				
			$temp->setConditions ( $condition );
			$temp->setActions ( $actioncondition );
			$temp->save();
				
			return true;
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'core/session' )->addError ( Mage::helper ( 'catalog' )->__ ( $e->getMessage () ) );
			return false;
		}
	
	}
	
	
	function  processConditionalCoupon($reco)
	{
		try
		{
			$name = $reco ['couponcode'];
			if($name==null || trim($name," ")=="")
			{
				return false;
			}
			$description = $name;
			if(!empty($reco ['validfrom']))
			{
				$fromdate = $reco ['validfrom'];
			}
			if(!empty($reco ['validto']))
			{
				$todate = $reco ['validto'];
			}
	
			if ($reco ['discounttype'] == 1) {
				$actionType = 'by_percent';
			} else {
				$actionType = 'cart_fixed';
			}
			$groupids = array();
			if(array_key_exists('email',$reco))
			{
				
				$groupid = $this->getCustomerGroup( $reco['email'] );
				if($groupid==null || $groupid=="")
				{
					$groupids[] = 1;
					$groupids[] = 0;
				}
				else
				{
					$groupids[] = $groupid;
				}
			}
			else
			{
				$groupids = $this->getALLCustomerGroup();
			}
			$discount = $reco ['discount'];
			$websiteId = 1;
			$conditionProducs = array ();
			$atrribute1=$reco['attribute'];
			
			if($atrribute1=="category")
				$atrribute="category_ids";
			else 
				$atrribute = $atrribute1;
			
			
			$operator=$reco['operator'];
			switch ($operator) {
				case 'eq' :
					foreach ( $reco [$atrribute1] as $value ) {
						$conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( $atrribute )->setOperator ( '==' )->setValue ( $value );
						$conditionProducs [] = $conditionProduct;
					}
					break;
				case 'neq' :
					foreach ( $reco [$atrribute1] as $value ) {
						$conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( $atrribute )->setOperator ( '!=' )->setValue ( $value );
						$conditionProducs [] = $conditionProduct;
					}
					break;
				case 'in' :
					$strvalue = implode ( ",", $reco [$atrribute1] );
					$conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( $atrribute )->setOperator ( '()' )->setValue ( $strvalue );
					$conditionProducs [] = $conditionProduct;
					break;
				case 'notin' :
					$strvalue = implode ( ",", $reco [$atrribute1] );
					$conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( $atrribute )->setOperator ( '!()' )->setValue ( $strvalue );
					$conditionProducs [] = $conditionProduct;
					break;
			}
		
				
			$shoppingCartPriceRule = Mage::getModel('salesrule/rule');
	
			$temp=$shoppingCartPriceRule->setName ( $name )->setDescription ( $description )->setIsActive ( 1 )->setWebsiteIds ( array (
					$websiteId
			) )->setCustomerGroupIds ( 	$groupids	)
			->setSortOrder ( '' )->setSimpleAction ( $actionType )
			->setDiscountAmount ( $discount )
			->setStopRulesProcessing ( 0 )
			->setCoupon_code ( $name )
			->setCouponType ( Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC );
			
			if(array_key_exists('usespercustomer',$reco))
			{
				$temp->setUses_per_customer ( $reco['usespercustomer']);
			}
				
			if(!empty($fromdate)&& !empty($todate))
			{
				$temp->setFromDate ( $fromdate );
				$temp->setToDate ( $todate );
			}
	
			$conditionCart = Mage::getModel ( 'salesrule/rule_condition_product' )
			-> setType ( 'salesrule/rule_condition_address' )
			->setAttribute ( 'base_subtotal' )
			->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] );
				
			$conditionProductFound = Mage::getModel ( 'salesrule/rule_condition_product_found' )->setAggregator('any')->setConditions ( $conditionProducs );
	
			$condition = Mage::getModel ( 'salesrule/rule_condition_combine' )->setConditions ( array (
					$conditionProductFound,$conditionCart
			) );
	
			$actionconditionInner = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setAggregator('any')->setConditions ( $conditionProducs );
	
			/* $conditionProduct2 = Mage::getModel ( 'salesrule/rule_condition_product' )
				-> setType ( 'salesrule/rule_condition_product' )
			->setAttribute ( 'quote_item_row_total' )
			->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] ); */
				
				
			$actioncondition = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setConditions ( array (
					$actionconditionInner
					//,$conditionProduct2
			) );
	
			$temp->setConditions ( $condition );
			$temp->setActions ( $actioncondition );
			$temp->save();
	
			return true;
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'core/session' )->addError ( Mage::helper ( 'catalog' )->__ ( $e->getMessage () ) );
			return false;
		}
	
	}
	
	
	function  processAllCoupon($reco)
	{
		try
		{
			$name = $reco ['couponcode'];
			if($name==null || trim($name," ")=="")
			{
				return false;
			}
			$description = $name;
			if(!empty($reco ['validfrom']))
			{
				$fromdate = $reco ['validfrom'];
				//echo $fromdate;
			}
			if(!empty($reco ['validto']))
			{
				$todate = $reco ['validto'];
				//echo $todate;
			}
	
			if ($reco ['discounttype'] == 1) {
				$actionType = 'by_percent';
			} else {
				$actionType = 'cart_fixed';
			}
	
			$discount = $reco ['discount'];
			$websiteId = 1;
			$usesPerCoupon = 1;
			$notLogedinGroupId = 0;
	
			//$groupcode=$reco['loyaltygroup'];
			//$groupid = $group=Mage::getModel('customer/group')->load($groupcode, 'customer_group_code')->getCustomerGroupId();
			$groupids = array();
			$groupid = $this->getCustomerGroup( $reco['email'] );
			if($groupid==null || $groupid=="")
			{
				$groupids[] = 1;
				$groupids[] = 0;
			}
			else
			{
				$groupids[] = $groupid;
			}
			$conditionProducs = array ();
			/* foreach ( $reco['sku'] as $value ) {
			 $conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ('sku')->setOperator ( '==' )->setValue ( $value );
			$conditionProducs [] = $conditionProduct;
			} */
				
				
			$shoppingCartPriceRule = Mage::getModel('salesrule/rule');
	
			$temp=$shoppingCartPriceRule->setName ( $name )->setDescription ( $description )->setIsActive ( 1 )->setWebsiteIds ( array (
					$websiteId
			) )->setCustomerGroupIds ( 	$groupids )
			->setSortOrder ( '' )->setSimpleAction ( $actionType )
			->setDiscountAmount ( $discount )
			->setStopRulesProcessing ( 0 )
			->setCoupon_code ( $name )
			->setCouponType ( Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC )
			->setUses_per_customer ( 1)->setUses_per_coupon ( $usesPerCoupon );
				
			if(!empty($fromdate)&& !empty($todate))
			{
				$temp->setFromDate ( $fromdate );
				$temp->setToDate ( $todate );
			}
	
				
		
			/* $conditionProduct2 = Mage::getModel ( 'salesrule/rule_condition_product' )
				-> setType ( 'salesrule/rule_condition_product' )
				->setAttribute ( 'quote_item_row_total' )
				->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] ); 
				$actioncondition = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setConditions ( array ($conditionProduct2	) );
				$temp->setActions ( $actioncondition );		
				//salesrule/rule_condition_product|quote_item_row_total
			*/
			
			
			$conditionProduct2 = Mage::getModel ( 'salesrule/rule_condition_product' )
			-> setType ( 'salesrule/rule_condition_address' )
			->setAttribute ( 'base_subtotal' )
			->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] );	
			//$conditionProductFound = Mage::getModel ( 'salesrule/rule_condition_product_found' )->setAggregator('any')->setConditions ( $conditionProducs );				
			$condition = Mage::getModel ( 'salesrule/rule_condition_combine' )->setConditions ( array (
					$conditionProduct2
			) );
			
			//salesrule/rule_condition_address|base_subtotal
			
			$temp->setConditions ( $condition );			
			$temp->save();	
			
			return true;
			
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'core/session' )->addError ( Mage::helper ( 'catalog' )->__ ( $e->getMessage () ) );
			return false;
		}
	
	}
	
	
	function  processCartCoupon($reco)
	{
		try
		{
			$name = $reco ['couponcode'];
			if($name==null || trim($name," ")=="")
			{
				return false;
			}
			$description = $name;
			if(!empty($reco ['validfrom']))
			{
				$fromdate = $reco ['validfrom'];
				//echo $fromdate;
			}
			if(!empty($reco ['validto']))
			{
				$todate = $reco ['validto'];
				//echo $todate;
			}
	
			if ($reco ['discounttype'] == 1) {
				$actionType = 'by_percent';
			} else {
				$actionType = 'cart_fixed';
			}
	
			$discount = $reco ['discount'];
			$websiteId = 1;
			//$usesPerCoupon = 1;
			$notLogedinGroupId = 0;
	
			//$groupcode=$reco['loyaltygroup'];
			//$groupid = $group=Mage::getModel('customer/group')->load($groupcode, 'customer_group_code')->getCustomerGroupId();
				
			$groupids = $this->getALLCustomerGroup();
			$conditionProducs = array ();
			/* foreach ( $reco['sku'] as $value ) {
			 $conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ('sku')->setOperator ( '==' )->setValue ( $value );
			$conditionProducs [] = $conditionProduct;
			} */
				
				
			$shoppingCartPriceRule = Mage::getModel('salesrule/rule');
	
			$temp=$shoppingCartPriceRule->setName ( $name )->setDescription ( $description )->setIsActive ( 1 )->setWebsiteIds ( array (
					$websiteId
			) )->setCustomerGroupIds ( $groupids )
			->setSortOrder ( '' )->setSimpleAction ( $actionType )
			->setDiscountAmount ( $discount )
			->setStopRulesProcessing ( 0 )
			->setCoupon_code ( $name )
			->setCouponType ( Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC )
			->setUses_per_customer ( 1);
			//->setUses_per_coupon ( $usesPerCoupon );
			
			if(array_key_exists('usespercustomer',$reco))
			{
				$temp->setUses_per_customer ( $reco['usespercustomer']);
			}
			
			if(!empty($fromdate)&& !empty($todate))
			{
				$temp->setFromDate ( $fromdate );
				$temp->setToDate ( $todate );
			}
	
				
		
			/* $conditionProduct2 = Mage::getModel ( 'salesrule/rule_condition_product' )
				-> setType ( 'salesrule/rule_condition_product' )
				->setAttribute ( 'quote_item_row_total' )
				->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] ); 
				$actioncondition = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setConditions ( array ($conditionProduct2	) );
				$temp->setActions ( $actioncondition );		
				//salesrule/rule_condition_product|quote_item_row_total
			*/
			
			
			$conditionProduct2 = Mage::getModel ( 'salesrule/rule_condition_product' )
			-> setType ( 'salesrule/rule_condition_address' )
			->setAttribute ( 'base_subtotal' )
			->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] );	
			//$conditionProductFound = Mage::getModel ( 'salesrule/rule_condition_product_found' )->setAggregator('any')->setConditions ( $conditionProducs );				
			$condition = Mage::getModel ( 'salesrule/rule_condition_combine' )->setConditions ( array (
					$conditionProduct2
			) );
			
			//salesrule/rule_condition_address|base_subtotal
			
			$temp->setConditions ( $condition );			
			$temp->save();	
			
			return true;
			
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'core/session' )->addError ( Mage::helper ( 'catalog' )->__ ( $e->getMessage () ) );
			return false;
		}
	
	}
	
}