<?php

class Retail_Log_IndexController extends Mage_Core_Controller_Front_Action
{	
	
	
	protected function _construct()
	{
		$isAllow = Mage::helper('retail_analytics/data')->raaValidateIP();
		if(!$isAllow) {
			print('Access Denied!');
			$this->_redirect('*/*/ttdstttsdt');
		}
	}
	
    public function indexAction()
    {
    	echo "Retail Log index controller ";    	
    	
    	
    	
    }
    
    
    public function raaAddLogAction()
    {    	 
    	$helper = Mage::helper('retail_log/retaillog');
    	$result = $helper->saveRetailLog($helper->tagName(),$helper->addAction(),"Test log");
    	echo $result;
       
    }
    
    public function raaGetLogAction()
    {
    	try{
    	$helper = Mage::helper('retail_log/retaillog');
    	$data = $helper->getRetailLog();
    	echo json_encode ( $data );
    	}
    	catch ( Exception $e ) {
    		echo json_encode ( $data );
    	}
    }
}