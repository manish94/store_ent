<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'retail_log'
 */
$table = $installer->getConnection()
// The following call to getTable('foo_bar/baz') will lookup the resource for foo_bar (foo_bar_mysql4), and look
// for a corresponding entity called baz. The table name in the XML is foo_bar_baz, so ths is what is created.
->newTable($installer->getTable('retail_log/retaillog'))
->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'identity'  => true,
		'unsigned'  => true,
		'nullable'  => false,
		'primary'   => true,
), 'ID')
->addColumn('tag', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
		'nullable'  => true,
		'type' => 'text',
), 'Tag')
->addColumn('action', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
		'nullable'  => true,
		'type' => 'text',
), 'Action')
->addColumn('message', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
		'nullable'  => false,
		'type' => 'text',
), 'Message')
->addColumn('date', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
		'nullable'  => false,
		'type' => 'timestamp NOT NULL default CURRENT_TIMESTAMP',
), 'Date');
$installer->getConnection()->createTable($table);
$installer->endSetup();