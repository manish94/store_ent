<?php
/**
 * ShopInDev
 *
 * @category    ShopInDev
 * @package     ShopInDev_SuperMinify
 * @copyright   Copyright (c) 2015 ShopInDev
 * @license     http://opensource.org/licenses/GPL-3.0 GNU General Public License (GPL)
 */

class ShopInDev_SuperMinify_Block_Page_Html_Head extends Mage_Page_Block_Html_Head {

	/**
	 * OVERWRITE
	 * Get HEAD HTML with CSS/JS definitions
	 * @return string
	 */
	public function getCssJsHtml(){

		$shouldMergeJs = Mage::helper('superminify')->canDoJsMerge();
		$shouldMergeCss = Mage::helper('superminify')->canDoCssMerge();

		if( !$shouldMergeJs AND !$shouldMergeCss ){
			return parent::getCssJsHtml();
		}

		$staticJsPatterns = '/^(prototype|scriptaculous|varien|mage)\//';
		$lines = array();
		$html = '';
		$html .= '<!-- Optimized using Super Minify -->' . "\n";

		// Separate lines
		foreach( $this->_data['items'] as $item ){

			if( !is_null($item['cond'])
				&& !$this->getData($item['cond']) || !isset($item['name']) ){
				continue;
			}

			$if = !empty($item['if']) ? $item['if'] : '';
			$params = !empty($item['params']) ? $item['params'] : '';

			if( $item['type'] == 'js'
				AND preg_match($staticJsPatterns, $item['name']) ){
				$item['type'] = 'static_js';
			}

			switch( $item['type'] ){
				case 'js':        // js/*.js
				case 'static_js': // js/*.js
				case 'skin_js':   // skin/*/*.js
				case 'js_css':    // js/*.css
				case 'skin_css':  // skin/*/*.css
					$lines[$if][$item['type']][$params][$item['name']] = $item['name'];
				break;
				default:
					$this->_separateOtherHtmlHeadElements($lines, $if, $item['type'], $params, $item['name'], $item);
				break;
			}

		}

		// Prepare HTML
		foreach( $lines as $if => $items ){

			if( empty($items) ){
				continue;
			}

			if( !empty($if) ){

				// Open !IE conditional using raw value
				if( strpos($if, "><!-->") !== false ){
					$html .= $if . "\n";
				}else{
					$html .= '<!--[if '.$if.']>' . "\n";
				}

			}

			// Static and skin css
			$html .= $this->_prepareStaticAndSkinElements(
				'<link rel="stylesheet" type="text/css" href="%s"%s />' . "\n",
				empty($items['js_css']) ? array() : $items['js_css'],
				empty($items['skin_css']) ? array() : $items['skin_css'],
				$shouldMergeCss ? array($this, 'getMergedCssUrl') : null
			);

			// Static Javascripts
			$html .= $this->_prepareStaticAndSkinElements(
				'<script type="text/javascript" src="%s"%s></script>' . "\n",
				empty($items['static_js']) ? array() : $items['static_js'],
				array(),
				$shouldMergeJs ? array($this, 'getMergedJsUrl') : null
			);

			// Skin javascripts
			$html .= $this->_prepareStaticAndSkinElements(
				'<script type="text/javascript" src="%s"%s></script>' . "\n",
				empty($items['js']) ? array() : $items['js'],
				empty($items['skin_js']) ? array() : $items['skin_js'],
				$shouldMergeJs ? array($this, 'getMergedJsUrl') : null
			);

			// Other stuff
			if( !empty($items['other']) ){
				$html .= $this->_prepareOtherHtmlHeadElements($items['other']) . "\n";
			}

			if( !empty($if) ){

				// Close !IE conditional comments correctly
				if( strpos($if, "><!-->") !== false ){
					$html .= '<!--<![endif]-->' . "\n";
				}else{
					$html .= '<![endif]-->' . "\n";
				}

			}

		}

		$html .= '<!-- Optimized using Super Minify -->' . "\n";

		return $html;
	}

	/**
	 * Alias for getMergedCssUrl
	 * @param array $files
	 * @return string
	 */
	public function getMergedCssUrl($files){
		return Mage::getModel('superminify/merge')->getMergedCssUrl($files);
	}

	/**
	 * Alias for getMergedJsUrl
	 * @param array $files
	 * @return string
	 */
	public function getMergedJsUrl($files){
		return Mage::getModel('superminify/merge')->getMergedJsUrl($files);
	}

}
