<?php
/**
 * ShopInDev
 *
 * @category    ShopInDev
 * @package     ShopInDev_SuperMinify
 * @copyright   Copyright (c) 2015 ShopInDev
 * @license     http://opensource.org/licenses/GPL-3.0 GNU General Public License (GPL)
 */

class ShopInDev_SuperMinify_Model_Observer{

	/**
	 * Move JS to Bottom, before closing </body>
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function moveJs($observer){

		$helper = Mage::helper('superminify');

		if( !$helper->canMoveJS() ){
			return;
		}

		$response = $observer->getEvent()->getResponse();

		if( !$response ){
			return;
		}

		$html = $response->getBody();
		$regex = '#(<\!--\[if[^\>]*>\s*<script.*</script>\s*<\!\[endif\]-->)|(<script.*</script>)#isU';
		$regexCloseBody = '#</body>\s*</html>#isU';
		$regexNoMove = '#(<script(.*)?nomove)#isU';
		$append = array();

		preg_match_all($regex, $html, $matches);
		preg_match_all($regexCloseBody, $html, $closeMatches);

		if( !$matches OR !$closeMatches ){
			return;
		}

		foreach( $matches[0] as $match ){

			if( preg_match($regexNoMove, $match) ){
				continue;
			}

			$html = str_replace($match, '', $html);
			$append[] = $match;

		}

		$close = $closeMatches[0][0];
		$html = str_replace($close, '', $html);
		$append[] = $close;

		$html .= implode("\n", $append);

		$response->setBody($html);
	}

}
