<?php
/**
 * ShopInDev
 *
 * @category    ShopInDev
 * @package     ShopInDev_SuperMinify
 * @copyright   Copyright (c) 2015 ShopInDev
 * @license     http://opensource.org/licenses/GPL-3.0 GNU General Public License (GPL)
 */

class ShopInDev_SuperMinify_Model_Merge{

	/**
	 * Merge specified javascript files and return URL to the merged file on success
	 * @param $files
	 * @return string
	 */
	public function getMergedJsUrl($files){

		$isSecure = Mage::app()->getRequest()->isSecure();

		$filename = md5(implode(',', $files)) . '.js';
		$directory = $this->checkMergeDir('js');
		$target = $directory. DS. $filename;

		$this->mergeFiles(
			$files,
			$target,
			null,
			null,
			'js'
		);

		$url = Mage::getBaseUrl('media', $isSecure). 'js/'. $filename;
		$url .= '?t='. filemtime($target);

		return $url;
	}

	/**
	 * Merge specified css files and return URL to the merged file on success
	 * @param $files
	 * @return string
	 */
	public function getMergedCssUrl($files){

		$isSecure = Mage::app()->getRequest()->isSecure();

		$baseMediaUrl = Mage::getBaseUrl('media', $isSecure);
		$hostname = parse_url($baseMediaUrl, PHP_URL_HOST);
		$port = parse_url($baseMediaUrl, PHP_URL_PORT);

		if( !$port ){
			$port = $isSecure ? 443 : 80;
		}

		$filename = md5(implode(',', $files). "|{$hostname}|{$port}"). '.css';
		$directory = $this->checkMergeDir('css');
		$target = $directory. DS. $filename;

		$this->mergeFiles(
			$files,
			$target,
			array($this, 'beforeMergeCss'),
			array($this, 'afterMergeCss'),
			'css'
		);

		$url = Mage::getBaseUrl('media', $isSecure). 'css/' . $filename;
		$url .= '?t='. filemtime($target);

		return $url;
	}

	/**
	 * Remove all merged js/css files
	 * @return bool
	 */
	public function cleanMergedJsCss(){

		$result = (bool) $this->checkMergeDir('js', TRUE);
		$result = (bool) $this->checkMergeDir('css', TRUE) && $result;

		return $result;
	}

	/**
	 * Make sure merger directory exists and writeable
	 * Also can clean it up
	 * @param string $directory
	 * @param bool $cleanup
	 * @return bool
	 */
	public function checkMergeDir($directory, $cleanup = FALSE){

		try {

			$directory = Mage::getBaseDir('media'). DS. $directory;

			if( $cleanup ){
				Varien_Io_File::rmdirRecursive($directory);
			}

			if( !is_dir($directory) ){
				mkdir($directory);
			}

			return is_writeable($directory) ? $directory : TRUE;

		} catch( Exception $e ){
			Mage::logException($e);
		}

		return TRUE;
	}

	/**
	 * Merge files into one file
	 * @param array $files
	 * @param string $target
	 * @param mixed $beforeMergeCallback
	 * @param mixed $afterMergeCallback
	 * @param string $type
	 * @return boolean
	 */
	public function mergeFiles($files, $target, $beforeMergeCallback, $afterMergeCallback, $type){

		$data = '';
		$shouldMerge = FALSE;

		foreach( $files as $key => $file ){

			$file = explode('?', $file);
			$file = $file['0'];

			$files[$key] = $file;

			if( !file_exists($file) ){
				unset($files[$key]);
				continue;
			}

			if( pathinfo($file, PATHINFO_EXTENSION) != $type ){
				unset($files[$key]);
				continue;
			}

			if( !$shouldMerge ){

				if( !file_exists($target) ){
					$shouldMerge = TRUE;
				}else{
					$targetMtime = filemtime($target);
					if( @filemtime($file) > $targetMtime ){
						$shouldMerge = TRUE;
					}
				}

			}

		}

		if( !$shouldMerge ){
			return FALSE;
		}

		if( !count($files) ){
			return FALSE;
		}

		foreach( $files as $file ){

			$content = file_get_contents($file);

			// strip BOM, if any
			if (substr($content, 0, 3) == "\xef\xbb\xbf") {
				$content = substr($content, 3);
			}

			$filePath = str_replace(Mage::getBaseDir(), '', $file);
			$filePath = trim($filePath, '/');

			$contents = "/* ". $filePath. " */\n";
			$contents .= $content;
			$contents .= "\n";

			if( $beforeMergeCallback && is_callable($beforeMergeCallback) ){
				$contents = call_user_func($beforeMergeCallback, $file, $contents);
			}

			$data .= $contents;
		}

		if( $afterMergeCallback && is_callable($afterMergeCallback) ){
			$data = call_user_func($afterMergeCallback, $data);
		}

		if( $type == 'js' AND Mage::helper('superminify')->canDoJsMinify() ){
			$data = Mage::getModel('superminify/minify_js')->compress($data);

		}elseif( $type == 'css' AND Mage::helper('superminify')->canDoCssMinify() ){
			$data = Mage::getModel('superminify/minify_css')->compress($data);
		}

		file_put_contents($target, $data, LOCK_EX);

		return TRUE;
	}

	/**
	 * Before merge css callback function
	 *
	 * @param string $file
	 * @param string $contents
	 * @return string
	 */
	public function beforeMergeCss($file, $contents){

	   $this->_setCallbackFiledirectory($file);

	   $cssImport = '/@import\\s+([\'"])(.*?)[\'"]/';
	   $contents = preg_replace_callback($cssImport, array($this, '_cssMergerImportCallback'), $contents);

	   $cssUrl = '/url\\(\\s*(?!data:)([^\\)\\s]+)\\s*\\)?/';
	   $contents = preg_replace_callback($cssUrl, array($this, '_cssMergerUrlCallback'), $contents);

	   return $contents;
	}

	/**
	 * After merge css callback function
	 *
	 * @param string $contents
	 * @return string
	 */
	public function afterMergeCss($contents){

		$cssImport = '/@import\s+url\(([\'"])(.*?)[\'"]\);/';
		preg_match_all($cssImport, $contents, $matches);

		if( !$matches ){
			return;
		}

		$contents = preg_replace($cssImport, '', $contents);
		$imports = implode("\n", $matches[0]);

		$contents = $imports. "\n". $contents;

		return $contents;
	}

	/**
	 * Set file directory for css file
	 * @param string $file
	 * @return void
	 */
	protected function _setCallbackFiledirectory($file){
	   $file = str_replace(Mage::getBaseDir().DS, '', $file);
	   $this->_callbackFiledirectory = dirname($file);
	}

	/**
	 * Callback function replaces relative links for @import matches in css file
	 * @param array $match
	 * @return string
	 */
	protected function _cssMergerImportCallback($match){

		$quote = $match[1];
		$uri = $this->_prepareUrl($match[2]);

		return "@import url({$quote}{$uri}{$quote})";
	}

	/**
	 * Callback function replaces relative links for url() matches in css file
	 * @param array $match
	 * @return string
	 */
	protected function _cssMergerUrlCallback($match){

		$quote = ($match[1][0] == "'" || $match[1][0] == '"') ? $match[1][0] : '';
		$uri = ($quote == '') ? $match[1] : substr($match[1], 1, strlen($match[1]) - 2);
		$uri = $this->_prepareUrl($uri);

		return "url({$quote}{$uri}{$quote})";
	}

	/**
	 * Prepare url for css replacement
	 * Check for absolute or relative url
	 * @param string $uri
	 * @return string
	 */
	protected function _prepareUrl($uri){

		if( !preg_match('/^https?:/i', $uri) && !preg_match('/^\//i', $uri) ){

			$filedirectory = '';
			$pathParts = explode(DS, $uri);
			$filedirectoryParts = explode(DS, $this->_callbackFiledirectory);
			$store = Mage::app()->getStore();

			if( $store->isAdmin() ){
				$secure = $store->isAdminUrlSecure();
			} else {
				$secure = $store->isFrontUrlSecure() && Mage::app()->getRequest()->isSecure();
			}

			if( 'skin' == $filedirectoryParts[0] ){
				$baseUrl = Mage::getBaseUrl('skin', $secure);
				$filedirectoryParts = array_slice($filedirectoryParts, 1);

			}elseif( 'media' == $filedirectoryParts[0] ){
				$baseUrl = Mage::getBaseUrl('media', $secure);
				$filedirectoryParts = array_slice($filedirectoryParts, 1);

			}else{
				$baseUrl = Mage::getBaseUrl('web', $secure);
			}

			foreach( $pathParts as $key => $part ){
				if( $part == '.' || $part == '..' ){
					unset($pathParts[$key]);
				}
				if( $part == '..' && count($filedirectoryParts) ){
					$filedirectoryParts = array_slice($filedirectoryParts, 0, count($filedirectoryParts) - 1);
				}
			}

			if( count($filedirectoryParts) ){
				$filedirectory = implode('/', $filedirectoryParts).'/';
			}

			$uri = $baseUrl. $filedirectory. implode('/', $pathParts);
		}

		return $uri;
	}

}
