<?php
/**
 * ShopInDev
 *
 * @category    ShopInDev
 * @package     ShopInDev_SuperMinify
 * @copyright   Copyright (c) 2015 ShopInDev
 * @license     http://opensource.org/licenses/GPL-3.0 GNU General Public License (GPL)
 */

/**
 * CSS minifier
 * Please report bugs on https://github.com/matthiasmullie/minify/issues
 * @author Matthias Mullie <minify@mullie.eu>
 * @author Tijs Verkoyen <minify@verkoyen.eu>
 * @copyright Copyright (c) 2012, Matthias Mullie. All rights reserved.
 * @license MIT License
 *
 * Last update: 03/22/2016
 */

class ShopInDev_SuperMinify_Model_Minify_Css{

	/**
	 * Are we "in" a hack? I.e. are some browsers targetted until the next comment?
	 * @var bool
	 */
	protected $_inHack = false;

	/**
	 * Minify the data.
	 * Perform CSS optimizations.
	 * @param string $content
	 * @return string
	 */
	public function compress($content = ''){

		$content = str_replace("\r\n", "\n", $content);
		$content = $this->stripComments($content);
		$content = $this->stripWhitespace($content);
		$content = $this->shortenHex($content);
		$content = $this->shortenZeroes($content);
		$content = $this->stripEmptyTags($content);

		return trim($content);
	}

	/**
	 * Strip comments from the content
	 * @param string $content
	 * @return string
	 */
	protected function stripComments($content){

		// preserve empty comment after '>'
		// http://www.webdevout.net/css-hacks#in_css-selectors
		$content = preg_replace('@>/\\*\\s*\\*/@', '>/*keep*/', $content);

		// preserve empty comment between property and value
		// http://css-discuss.incutio.com/?page=BoxModelHack
		$content = preg_replace('@/\\*\\s*\\*/\\s*:@', '/*keep*/:', $content);
		$content = preg_replace('@:\\s*/\\*\\s*\\*/@', ':/*keep*/', $content);

		// apply callback to all valid comments (and strip out surrounding whitespace)
		$content = preg_replace_callback('@\\s*/\\*([\\s\\S]*?)\\*/\\s*@'
			,array($this, 'stripCommentsCallback'), $content);

		return trim($content);
	}

	/**
	 * Process a comment and return a replacement
	 * @param array $m regex matches
	 * @return string
	 */
	protected function stripCommentsCallback($m){

		$hasSurroundingWs = (trim($m[0]) !== $m[1]);
		$m = $m[1];

		// $m is the comment content w/o the surrounding tokens,
		// but the return value will replace the entire comment.
		if( $m === 'keep' ){
			return '/**/';
		}

		// component of http://tantek.com/CSS/Examples/midpass.html
		if( $m === '" "' ){
			return '/*" "*/';
		}

		// component of http://tantek.com/CSS/Examples/midpass.html
		if( preg_match('@";\\}\\s*\\}/\\*\\s+@', $m) ){
			return '/*";}}/* */';
		}

		// inversion: feeding only to one browser
		if( $this->_inHack ){
			if( preg_match('@
					^/               # comment started like /*/
					\\s*
					(\\S[\\s\\S]+?)  # has at least some non-ws content
					\\s*
					/\\*             # ends like /*/ or /**/
				@x', $m, $n) ){
				// end hack mode after this comment, but preserve the hack and comment content
				$this->_inHack = false;
				return "/*/{$n[1]}/**/";
			}
		}

		// comment ends like \*/
		if( substr($m, -1) === '\\' ){
			// begin hack mode and preserve hack
			$this->_inHack = true;
			return '/*\\*/';
		}

		 // comment looks like /*/ foo */
		if( $m !== '' && $m[0] === '/' ){
			// begin hack mode and preserve hack
			$this->_inHack = true;
			return '/*/*/';
		}

		// a regular comment ends hack mode but should be preserved
		if( $this->_inHack ){
			$this->_inHack = false;
			return '/**/';
		}

		// Issue 107: if there's any surrounding whitespace, it may be important, so
		// replace the comment with a single space
		// remove all other comments
		return $hasSurroundingWs ? ' ' : '';
	}

	/**
	 * Strip whitespace.
	 * @param string $content
	 * @return string
	 */
	protected function stripWhitespace($content){

		// remove leading & trailing whitespace
		$content = preg_replace('/^\s*/m', '', $content);
		$content = preg_replace('/\s*$/m', '', $content);

		// replace newlines with a single space
		$content = preg_replace('/\s+/', ' ', $content);

		// remove whitespace around meta characters
		// inspired by stackoverflow.com/questions/15195750/minify-compress-css-with-regex
		$content = preg_replace('/\s*([\*$~^|]?+=|[{};,>~]|!important\b)\s*/', '$1', $content);
		$content = preg_replace('/([\[(:])\s+/', '$1', $content);
		$content = preg_replace('/\s+([\]\)])/', '$1', $content);
		$content = preg_replace('/\s+(:)(?![^\}]*\{)/', '$1', $content);

		// whitespace around + and - can only be stripped in selectors, like
		// :nth-child(3+2n), not in things like calc(3px + 2px) or shorthands
		// like 3px -2px
		$content = preg_replace('/\s*([+-])\s*(?=[^}]*{)/', '$1', $content);

		// remove semicolon/whitespace followed by closing bracket
		$content = str_replace(';}', '}', $content);

		// remove whitespace around { } and last semicolon in declaration block
		$content = preg_replace('/\\s*{\\s*/', '{', $content);
		$content = preg_replace('/;?\\s*}\\s*/', '}', $content);

		// remove whitespace surrounding semicolons
		$content = preg_replace('/\\s*;\\s*/', ';', $content);

		// remove whitespace around urls
		$content = preg_replace('/
				url\\(      # url(
				\\s*
				([^\\)]+?)  # 1 = the URL (really just a bunch of non right parenthesis)
				\\s*
				\\)         # )
			/x', 'url($1)', $content);

		// remove whitespace between rules and colons
		$content = preg_replace('/
				\\s*
				([{;])              # 1 = beginning of block or rule separator
				\\s*
				([\\*_]?[\\w\\-]+)  # 2 = property (and maybe IE filter)
				\\s*
				:
				\\s*
				(\\b|[#\'"-])        # 3 = first character of a value
			/x', '$1$2:$3', $content);

		// remove whitespace in selectors
		$content = preg_replace_callback('/
				(?:              # non-capture
					\\s*
					[^~>+,\\s]+  # selector part
					\\s*
					[,>+~]       # combinators
				)+
				\\s*
				[^~>+,\\s]+      # selector part
				{                # open declaration block
			/x'
			,array($this, 'stripWhitespaceSelectorsCallback'), $content);

		// remove whitespace in imports
		$content = preg_replace('/@import\\s+url/', '@import url', $content);

		// replace any whitespace involving newlines with a single newline
		$content = preg_replace('/[ \\t]*\\n+\\s*/', "\n", $content);

		// separate common descendent selectors w/ newlines (to limit line lengths)
		$content = preg_replace('/([\\w#\\.\\*]+)\\s+([\\w#\\.\\*]+){/', "$1\n$2{", $content);

		// use newline after 1st numeric value (to limit line lengths).
		// $content = preg_replace('/
		// 	((?:padding|margin|border|outline):\\d+(?:px|em)?) # 1 = prop : 1st numeric value
		// 	\\s+
		// 	/x'
		// 	,"$1\n", $content);

		return trim($content);
	}

	/**
	 * Replace what looks like a set of selectors
	 * @param array $m regex matches
	 * @return string
	 */
	protected function stripWhitespaceSelectorsCallback($m){
		// remove whitespace around the combinators
		return preg_replace('/\\s*([,>+~])\\s*/', '$1', $m[0]);
	}

	/**
	 * Shorthand hex color codes.
	 * #FF0000 -> #F00
	 * @param string $content
	 * @return string
	 */
	protected function shortenHex($content){
		$content = preg_replace('/(?<![\'"])#([0-9a-z])\\1([0-9a-z])\\2([0-9a-z])\\3(?![\'"])/i', '#$1$2$3', $content);
		return $content;
	}

	/**
	 * Shorthand 0 values to plain 0, instead of e.g. -0em.
	 * @param string $content
	 * @return string
	 */
	protected function shortenZeroes($content){

		// reusable bits of code throughout these regexes:
		// before & after are used to make sure we don't match lose unintended
		// 0-like values (e.g. in #000, or in http://url/1.0)
		// units can be stripped from 0 values, or used to recognize non 0
		// values (where wa may be able to strip a .0 suffix)
		$before = '(?<=[:(, ])';
		$after = '(?=[ ,);}])';
		$units = '(em|ex|%|px|cm|mm|in|pt|pc|ch|rem|vh|vw|vmin|vmax|vm)';

		// strip units after zeroes (0px -> 0)
		// NOTE: it should be safe to remove all units for a 0 value, but in
		// practice, Webkit (especially Safari) seems to stumble over at least
		// 0%, potentially other units as well. Only stripping 'px' for now.
		// @see https://github.com/matthiasmullie/minify/issues/60
		$content = preg_replace('/'.$before.'(-?0*(\.0+)?)(?<=0)px'.$after.'/', '\\1', $content);

		// strip 0-digits (.0 -> 0)
		$content = preg_replace('/'.$before.'\.0+'.$units.'?'.$after.'/', '0\\1', $content);

		// strip trailing 0: 50.10 -> 50.1, 50.10px -> 50.1px
		$content = preg_replace('/'.$before.'(-?[0-9]+\.[0-9]+)0+'.$units.'?'.$after.'/', '\\1\\2', $content);

		// strip trailing 0: 50.00 -> 50, 50.00px -> 50px
		$content = preg_replace('/'.$before.'(-?[0-9]+)\.0+'.$units.'?'.$after.'/', '\\1\\2', $content);

		// strip leading 0: 0.1 -> .1, 01.1 -> 1.1
		$content = preg_replace('/'.$before.'(-?)0+([0-9]*\.[0-9]+)'.$units.'?'.$after.'/', '\\1\\2\\3', $content);

		// strip negative zeroes (-0 -> 0) & truncate zeroes (00 -> 0)
		$content = preg_replace('/'.$before.'-?0+'.$units.'?'.$after.'/', '0\\1', $content);

		return $content;
	}

	/**
	 * Strip comments from source code.
	 * @param string $content
	 * @return string
	 */
	protected function stripEmptyTags($content){
		return preg_replace('/(^|\})[^\{\}]+\{\s*\}/', '\\1', $content);
	}

}