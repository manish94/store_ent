<?php
/**
 * ShopInDev
 *
 * @category    ShopInDev
 * @package     ShopInDev_SuperMinify
 * @copyright   Copyright (c) 2015 ShopInDev
 * @license     http://opensource.org/licenses/GPL-3.0 GNU General Public License (GPL)
 */

class ShopInDev_SuperMinify_Helper_Data extends Mage_Core_Helper_Abstract {

	const XML_PATH_MERGE_CSS = 'system/superminify/merge_css';
	const XML_PATH_MINIFY_CSS = 'system/superminify/minify_css';
	const XML_PATH_MERGE_JS = 'system/superminify/merge_js';
	const XML_PATH_MINIFY_JS = 'system/superminify/minify_js';
	const XML_PATH_MOVE_JS = 'system/superminify/move_js';

	/**
	 * Return if can do CSS merge
	 * @return boolean
	 */
	public function canDoCssMerge(){
		return (boolean) Mage::getStoreConfig(self::XML_PATH_MERGE_CSS);
	}

	/**
	 * Return if can do CSS minification
	 * @return boolean
	 */
	public function canDoCssMinify(){
		return (boolean) Mage::getStoreConfig(self::XML_PATH_MINIFY_CSS);
	}

	/**
	 * Return if can do JS merge
	 * @return boolean
	 */
	public function canDoJsMerge(){
		return (boolean) Mage::getStoreConfig(self::XML_PATH_MERGE_JS);
	}

	/**
	 * Return if can do JS minification
	 * @return boolean
	 */
	public function canDoJsMinify(){
		return (boolean) Mage::getStoreConfig(self::XML_PATH_MINIFY_JS);
	}

	/**
	 * Return if can move JS to bottom
	 * @return boolean
	 */
	public function canMoveJS(){
		return (boolean) Mage::getStoreConfig(self::XML_PATH_MOVE_JS);
	}

}
