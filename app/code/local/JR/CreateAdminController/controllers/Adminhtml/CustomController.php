<?php

class JR_CreateAdminController_Adminhtml_CustomController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('mycustomtab')
            ->_title($this->__('Index Action'));
           $this->_addContent($this->getLayout()->createBlock('core/template')->setTemplate('test/test.phtml'));
        // my stuff
        $this->renderLayout();
    }
    public function ajxchrtAction()
	 {
	$keyword = $this->getRequest()->getParam('data1');
	$category =  Mage::getModel('catalog/category')->getCollection()
                            ->addAttributeToSelect('*');
							$s_flag=0;				
     foreach ($category as $cat){
            $entity_id = $cat->getId();
            $name = $cat->getName();
            $url_key = $cat->getUrlKey();
            $url_path = $cat->getUrlPath();
            $skin_url = $cat->getThumbnail();
			$page_url = $cat->getUrl(); 
			if(stripos($name,$keyword) !== FALSE){
			
				$src = Mage::getBaseUrl('media');
				$imgsrc = $src."/catalog/category/".$skin_url;
?>
				<li onclick='fill("<?php echo $name; ?>"); fill1("<?php echo $entity_id; ?>"); fill2("<?php echo $imgsrc; ?>"); fill3("<?php echo $page_url; ?>")'><?php echo $name; ?></li>
				 <?php
				 $s_flag=0;
				  break;
			} else {
				$s_flag=1;
			}
			
    } //for each 
	if($s_flag==1)
	{
		echo "Sorry, No match found.";
	}
	}
 public function ajxlodAction()
 {
 $name = $this->getRequest()->getParam('data2');
 $isrc = $this->getRequest()->getParam('data3');
 $id = $this->getRequest()->getParam('data4');
 $page_url = $this->getRequest()->getParam('data5');
 $position = $this->getRequest()->getParam('data6');
  $resource = Mage::getSingleton('core/resource');
  $readConnection = $resource->getConnection('core_read');
       $writeConnection = $resource->getConnection('core_write');
	   $result = $readConnection->query("select * from custom_entsto_chart where cat_id=".$id."");
	  $row = $result->fetchAll();
	   if (!empty($row))  {
	   echo "Category already available";
	   }
	   else
	   {
	   $rsq = $writeConnection->query("SELECT max(position) as position FROM custom_entsto_chart");
	  $max_pos = $rsq->fetchAll();
	  foreach($max_pos as $pos)
	  {
	// echo $pos['position'];
	  $fin_pos = $pos['position']+1;
	$result1 = $writeConnection->query("Insert into custom_entsto_chart (cat_id, cat_name, image, url, position)VALUES(".$id.", '".$name."', '".$isrc."','".$page_url."',".$fin_pos.")");
	  echo "Category Added Successfully";
	  
}	  }	   
 }
 public function delrecAction()
 {
 $id = $this->getRequest()->getParam('data5');
 $resource = Mage::getSingleton('core/resource');
 $writeConnection = $resource->getConnection('core_write');
	   $result2 = $writeConnection->query("Delete from custom_entsto_chart where cat_id=".$id."");
	   echo "Category removed successfully";
 }
 public function updteAction()
 {
 $idof = $this->getRequest()->getParam('data6');
  $order = $this->getRequest()->getParam('data7');
 $resource = Mage::getSingleton('core/resource');
 $writeConnection = $resource->getConnection('core_write');
 $result2 = $writeConnection->query("UPDATE custom_entsto_chart SET position=".$order." where cat_id=".$idof."");
	   echo "Category reordering done successfully";
 }

}
?>