<?php
$client = new SoapClient('http://entsto.com/dev/api/v2_soap/?wsdl');
$session = $client->login('retailinsights', 'retail123');
$result = $client->catalogCategoryCreate($session, 2, array(
    'name' => 'Category name 2',
    'is_active' => 1,
    'position' => 1,
    'available_sort_by' => array('position'),
    'custom_design' => null,
    'custom_apply_to_products' => null,
    'custom_design_from' => null,
    'custom_design_to' => null,
    'custom_layout_update' => null,
    'default_sort_by' => 'position',
    'description' => 'Category description',
    'display_mode' => null,
    'is_anchor' => 0,
    'landing_page' => null,
    'meta_description' => 'Category meta description',
    'meta_keywords' => 'Category meta keywords',
    'meta_title' => 'Category meta title',
    'page_layout' => 'two_columns_left',
    'url_key' => 'url-key',
    'include_in_menu' => 1,
));
var_dump ($result);

?>