<?php

class Retail_Analytics_Helper_Customerreco extends Mage_Core_Helper_Abstract
{
		
	public function saveCustomerReco($data) {
		$customerreco = Mage::getModel('retail_analytics/customerreco');
		$customerreco->setCustomerid($data['from']);
		$customerreco->setEmail($data['email']);
		$customerreco->setMap($data['map']);
		$customerreco->setWeight($data['weight']);
		$customerreco->setCreated(now());
		$customerreco->setModified(now());
		$customerreco->save();
	}	
	
	public function addCustomerReco($data) {
	
		try {
			foreach ( $data as $row ) {
				$this->saveCustomerReco( $row );
			}
			
			return true;
		}			
		catch ( Exception $e ) {
			return false;
		}
	}	

	
	
	
}