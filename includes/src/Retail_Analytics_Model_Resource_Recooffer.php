<?php
class Retail_Analytics_Model_Resource_Recooffer extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('retail_analytics/recooffer', 'id');
    }
}