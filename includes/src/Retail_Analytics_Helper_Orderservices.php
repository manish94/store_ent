<?php
class Retail_Analytics_Helper_Orderservices extends Mage_Core_Helper_Abstract {
	
	public function raaOrderById($id) {
		$data=array();
		try {
			$order= Mage::getModel ( 'sales/order' )->load($id);
			if ($order) {
				$order_id = $order->getData ( 'entity_id' );
				
			if($order_id !=null)
			{	
				$orderArray = array ();
				$orderArray ['entity_id'] = $order->getData ( 'entity_id' );
				$orderArray ['order_id'] = $order->getData ( 'entity_id' );
				$orderArray ['created_at'] = $order->getData ( 'created_at' );
				$orderArray ['updated_at'] = $order->getData ( 'updated_at' );
				$orderArray ['status'] = $order->getData ( 'status' );
				$orderArray ['subtotal'] = $order->getData ( 'subtotal' );
				$orderArray ['base_subtotal'] = $order->getData ( 'base_subtotal' );
				$orderArray ['grand_total'] = $order->getData ( 'grand_total' );
				$orderArray ['base_grand_total'] = $order->getData ( 'base_grand_total' );
				$orderArray ['payment_method'] = $order->getPayment()->getData('method');
				$orderArray ['coupon_code'] = $order->getData ( 'coupon_code' );
				$orderArray ['increment_id'] = $order->getData ( 'increment_id' );
				$orderArray ["store_id"] = $order['store_id'];
				$orderData = Mage::helper ( 'core' )->jsonEncode($order->getData());
				$orderArray ['order_data'] =  Mage::helper ( 'core' )->jsonDecode($orderData);
	
				$orderArray ['customer_id'] = $order->getData ( 'customer_id' );
				$orderArray ['email'] = $order->getData ( 'customer_email' );
				$orderArray ['customer_firstname'] = $order->getData ( 'customer_firstname' );
				$orderArray ['customer_middlename'] = $order->getData ( 'customer_middlename' );
				$orderArray ['customer_lastname'] = $order->getData ( 'customer_lastname' );
				$orderArray ['customer_dob'] = $order->getData ( 'customer_dob' );
				$orderArray ['customer_group_id'] = $order->getData ( 'customer_group_id' );
				$orderArray ['customer_is_guest'] = $order->getData ( 'customer_is_guest' );
				$orderArray ['customer_prefix'] = $order->getData ( 'customer_prefix' );
				$orderArray ['customer_suffix'] = $order->getData ( 'customer_suffix' );
				$orderArray ['customer_taxvat'] = $order->getData ( 'customer_taxvat' );
				$orderArray ['customer_note'] = $order->getData ( 'customer_note' );
	
				/**currency type*/
				$orderArray ['global_currency_code']=$order->getData ( 'global_currency_code' );
				$orderArray ['base_currency_code']=$order->getData ( 'base_currency_code' );
				$orderArray ['order_currency_code']=$order->getData ( 'order_currency_code' );
				$orderArray ['store_currency_code']=$order->getData ( 'store_currency_code' );
	
	
				$billingAddress = Mage::helper ( 'core' )->jsonEncode($order->getBillingAddress());
				$shippingAddress = Mage::helper ( 'core' )->jsonEncode($order->getShippingAddress());
				$orderArray ['billing_address'] = Mage::helper ( 'core' )->jsonDecode($billingAddress);
				$orderArray ['shipping_address'] = Mage::helper ( 'core' )->jsonDecode($shippingAddress);
	
	
				$orderItem = array ();
				$items = $order->getAllItems ();
				foreach ( $items as $item ) {
						
					$orderItem['product_id'] = $item->getData ( 'product_id' );
					$orderItem ['sku'] = $item->getData ( 'sku' );
					$orderItem ['quantity'] = $item->getData ( 'qty_ordered' );
					$orderItem ['price'] = $item->getData ( 'price_incl_tax' );
					$orderItem ['discount'] = $item->getData ( 'discount_amount' );
					$orderItem ['base_price']=$item->getData ( 'base_price' );
					$orderItem ['base_discount']=  $item->getData ( 'base_discount_amount' );
					$orderItem ['original_price']=$item->getData ( 'original_price' );
					$orderItem ['base_original_price']=$item->getData ( 'base_original_price' );
	
						
					$order = array_merge($orderArray, $orderItem);
					$data[] = $order;
				}
			}
		}
			return $data;
		} catch ( Exception $e ) {
			return $data;
		}
	}
	
	public function raaOrders($lastId, $limit) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		try {
				
			$data_collection = Mage::getModel ( 'sales/order' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
					"gt" => $lastId
			) )->addAttributeToSelect ( "*" );
			$data_collection->getSelect ()->limit ( $limit );
				
			if ($data_collection->count () > 0) {
				$lastId = $data_collection->getLastItem ()->getId ();
				foreach ( $data_collection as $order ) {
						
					$items = $order->getAllVisibleItems();
					foreach ( $items as $item ) {
						$orderArray = array ();
						$orderArray ['entity_id'] = $order->getData ( 'entity_id' );
						$orderArray ['order_id'] = $order->getData ( 'entity_id' );
						$orderArray ['created_at'] = $order->getData ( 'created_at' );
						$orderArray ['updated_at'] = $order->getData ( 'updated_at' );
						$orderArray ['status'] = $order->getData ( 'status' );
						$orderArray ['store_id'] = $order->getData ( 'store_id' );
	
						$orderArray ['increment_id'] = $order->getData ( 'increment_id' );
						$orderArray ['subtotal'] = $order->getData ( 'subtotal' );
						$orderArray ['base_subtotal'] = $order->getData ( 'base_subtotal' );
						$orderArray ['grand_total'] = $order->getData ( 'grand_total' );
						$orderArray ['base_grand_total'] = $order->getData ( 'base_grand_total' );
						$orderArray ['payment_method'] = $order->getPayment()->getData('method');
						$orderArray ['coupon_code'] = $order->getData ( 'coupon_code' );
	
						$orderData = Mage::helper ( 'core' )->jsonEncode($order->getData());
						$orderArray ['order_data'] =  Mage::helper ( 'core' )->jsonDecode($orderData);
	
						$orderArray ['customer_id'] = $order->getData ( 'customer_id' );
						$orderArray ['email'] = $order->getData ( 'customer_email' );
						$orderArray ['customer_firstname'] = $order->getData ( 'customer_firstname' );
						$orderArray ['customer_middlename'] = $order->getData ( 'customer_middlename' );
						$orderArray ['customer_lastname'] = $order->getData ( 'customer_lastname' );
						$orderArray ['customer_dob'] = $order->getData ( 'customer_dob' );
						$orderArray ['customer_group_id'] = $order->getData ( 'customer_group_id' );
						$orderArray ['customer_is_guest'] = $order->getData ( 'customer_is_guest' );
						$orderArray ['customer_prefix'] = $order->getData ( 'customer_prefix' );
						$orderArray ['customer_suffix'] = $order->getData ( 'customer_suffix' );
						$orderArray ['customer_taxvat'] = $order->getData ( 'customer_taxvat' );
						$orderArray ['customer_note'] = $order->getData ( 'customer_note' );
	
						$orderArray ['global_currency_code']=$order->getData ( 'global_currency_code' );
						$orderArray ['base_currency_code']=$order->getData ( 'base_currency_code' );
						$orderArray ['order_currency_code']=$order->getData ( 'order_currency_code' );
						$orderArray ['store_currency_code']=$order->getData ( 'store_currency_code' );
	
						$billingAddress = Mage::helper ( 'core' )->jsonEncode($order->getBillingAddress());
						$shippingAddress = Mage::helper ( 'core' )->jsonEncode($order->getShippingAddress());
						$orderArray ['billing_address'] = Mage::helper ( 'core' )->jsonDecode($billingAddress);
						$orderArray ['shipping_address'] = Mage::helper ( 'core' )->jsonDecode($shippingAddress);
	
						$orderArray ['product_id'] = $item->getData ( 'product_id' );
						$orderArray ['sku'] = $item->getData ( 'sku' );
						$orderArray ['quantity'] = $item->getData ( 'qty_ordered' );
						$orderArray ['price'] = $item->getData ( 'price_incl_tax' );
						$orderArray ['discount'] = $item->getData ( 'discount_amount' );
						$orderArray ['base_price']=$item->getData ( 'base_price' );
						$orderArray ['base_discount']=  $item->getData ( 'base_discount_amount' );
						$orderArray ['original_price']=$item->getData ( 'original_price' );
						$orderArray ['base_original_price']=$item->getData ( 'base_original_price' );
	
						/* if($parent = $item->getParentItem()){
							$parent_sku = $parent->getProduct()->getSku();
							if ($parent_sku != null || $parent_sku != "")
							{
								$orderArray ['sku'] = $parent_sku;
							}
						} */
	
						$prod = Mage::getModel( 'catalog/product' )->load ( $orderArray ['product_id']);
						$categoryIds = $prod->getCategoryIds();
						if (isset($categoryIds[0])){
							$_categoryids = array();
							$parentCategory="";
								
							$orderArray["category_info"] = array();
							foreach ( $categoryIds as $_category ) {
								$category = Mage::getModel('catalog/category')->load($_category);
								$cat_path = $category->getPath();
								$orderArray["category_info"][] = $cat_path;
	
								$childCategories = $category->getChildrenCount();
								$parentCategory=$_category;
								if($childCategories == "0")
								{
									$_categoryids[] = $_category;
								}
							}
							if(count($_categoryids)==0 && $parentCategory != "" ){
								$_categoryids[] = $parentCategory;
							}
							if(count($_categoryids) > 0)
							{
								$orderArray ["category_id"] = $_categoryids[0];
								//$orderArray ['root_category_id'] = Mage::helper('retail_analytics/categoryservices')->getRootCatId($orderArray ["category_id"] );
							}
						}
	
						$storeId = $order->getStoreId();
						$orderArray ['store_id'] = $storeId;
						$orderArray ['website_id'] = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
						$data [] = $orderArray;
					}
				}
	
				$retObj ['data'] = $data;
				$retObj ['lastid'] = $lastId;
				if ($data_collection->count () < $limit) {
					$retObj ['islast'] = true;
				} else {
					$retObj ['islast'] = false;
				}
			} else {
				$retObj ['lastid'] = $lastId;
				$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
				
			return $retObj;
		} catch ( Exception $e ) {
				
			$retObj ['lastid'] = - 1;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}

	public function raaOrdersByDate($lastdate,$lastid,$limit) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		$RowCount = 0;
		try {
			$data_collection = Mage::getModel ( 'sales/order' )->getCollection ();
			if($lastid==0)
			{
				$data_collection = $data_collection->addAttributeToFilter('updated_at', array('gteq' => $lastdate))
				->addAttributeToFilter('entity_id', array('gt' => $lastid))
				->addAttributeToSelect ( "*" )
				->addAttributeToSort("updated_at","ASC")
				->addAttributeToSort("entity_id","ASC");
				$data_collection->getSelect ()->limit ( $limit );
				$RowCount = $data_collection->count ();
					
			}
			else
			{
				$RowCount = Mage::getModel ( 'sales/order' )->getCollection ()
				->addAttributeToFilter('updated_at', array('eq' => $lastdate))
				->addAttributeToFilter('entity_id', array('gt' => $lastid))
				->count ();
				$data_collection = $data_collection->addAttributeToFilter('updated_at', array('eq' => $lastdate))
				->addAttributeToFilter('entity_id', array('gt' => $lastid))
				->addAttributeToSelect ( "*" )
				->addAttributeToSort("entity_id","ASC");
				$data_collection->getSelect ()->limit ( $limit );
			}
	
			if ($data_collection->count () > 0) {
				$lastdatesend = $data_collection->getLastItem ()->getUpdatedAt();
				$lastidsend=$data_collection->getLastItem()->getId();
	
				foreach ( $data_collection as $order ) {
	
					$items = $order->getAllVisibleItems();
					foreach ( $items as $item ) {
						$orderArray = array ();
						$orderArray ['entity_id'] = $order->getData ( 'entity_id' );
						$orderArray ['order_id'] = $order->getData ( 'entity_id' );
						$orderArray ['created_at'] = $order->getData ( 'created_at' );
						$orderArray ['updated_at'] = $order->getData ( 'updated_at' );
						$orderArray ['status'] = $order->getData ( 'status' );
						$orderArray ['subtotal'] = $order->getData ( 'subtotal' );
						$orderArray ['base_subtotal'] = $order->getData ( 'base_subtotal' );
						$orderArray ['grand_total'] = $order->getData ( 'grand_total' );
						$orderArray ['base_grand_total'] = $order->getData ( 'base_grand_total' );
						$orderArray ['payment_method'] = $order->getPayment()->getData('method');
						$orderArray ['coupon_code'] = $order->getData ( 'coupon_code' );
						$orderArray ['increment_id'] = $order->getData ( 'increment_id' );
						$orderData = Mage::helper ( 'core' )->jsonEncode($order->getData());
						$orderArray ['order_data'] =  Mage::helper ( 'core' )->jsonDecode($orderData);
	
						$orderArray ['customer_id'] = $order->getData ( 'customer_id' );
						$orderArray ['email'] = $order->getData ( 'customer_email' );
						$orderArray ['customer_firstname'] = $order->getData ( 'customer_firstname' );
						$orderArray ['customer_middlename'] = $order->getData ( 'customer_middlename' );
						$orderArray ['customer_lastname'] = $order->getData ( 'customer_lastname' );
						$orderArray ['customer_dob'] = $order->getData ( 'customer_dob' );
						$orderArray ['customer_group_id'] = $order->getData ( 'customer_group_id' );
						$orderArray ['customer_is_guest'] = $order->getData ( 'customer_is_guest' );
						$orderArray ['customer_prefix'] = $order->getData ( 'customer_prefix' );
						$orderArray ['customer_suffix'] = $order->getData ( 'customer_suffix' );
						$orderArray ['customer_taxvat'] = $order->getData ( 'customer_taxvat' );
						$orderArray ['customer_note'] = $order->getData ( 'customer_note' );
	
						/**currency type*/
						$orderArray ['global_currency_code']=$order->getData ( 'global_currency_code' );
						$orderArray ['base_currency_code']=$order->getData ( 'base_currency_code' );
						$orderArray ['order_currency_code']=$order->getData ( 'order_currency_code' );
						$orderArray ['store_currency_code']=$order->getData ( 'store_currency_code' );
	
	
	
						$billingAddress = Mage::helper ( 'core' )->jsonEncode($order->getBillingAddress());
						$shippingAddress = Mage::helper ( 'core' )->jsonEncode($order->getShippingAddress());
						$orderArray ['billing_address'] = Mage::helper ( 'core' )->jsonDecode($billingAddress);
						$orderArray ['shipping_address'] = Mage::helper ( 'core' )->jsonDecode($shippingAddress);
	
						$orderArray ['product_id'] = $item->getData ( 'product_id' );
						$orderArray ['sku'] = $item->getData ( 'sku' );
						$orderArray ['quantity'] = $item->getData ( 'qty_ordered' );
						$orderArray ['price'] = $item->getData ( 'price_incl_tax' );
						$orderArray ['discount'] = $item->getData ( 'discount_amount' );
						$orderArray ['base_price']=$item->getData ( 'base_price' );
						$orderArray ['base_discount']=  $item->getData ( 'base_discount_amount' );
						$orderArray ['original_price']=$item->getData ( 'original_price' );
						$orderArray ['base_original_price']=$item->getData ( 'base_original_price' );
	
						/* if($parent = $item->getParentItem()){
							$parent_sku = $parent->getProduct()->getSku();
							if ($parent_sku != null || $parent_sku != "")
							{
								$orderArray ['sku'] = $parent_sku;
							}
						} */
	
						$prod = Mage::getModel( 'catalog/product' )->load ( $orderArray ['product_id']);
						$categoryIds = $prod->getCategoryIds();
						if (isset($categoryIds[0])){
							$_categoryids = array();
							$parentCategory="";
								
							$orderArray["category_info"] = array();
							foreach ( $categoryIds as $_category ) {
								$category = Mage::getModel('catalog/category')->load($_category);
								$cat_path = $category->getPath();
								$orderArray["category_info"][] = $cat_path;
	
								$childCategories = $category->getChildrenCount();
								$parentCategory=$_category;
								if($childCategories == "0")
								{
									$_categoryids[] = $_category;
								}
							}
								
							if(count($_categoryids)==0 && $parentCategory != "" ){
								$_categoryids[] = $parentCategory;
							}
							if(count($_categoryids) > 0)
							{
								$orderArray ["category_id"] = $_categoryids[0];
								//$orderArray ['root_category_id'] = Mage::helper('retail_analytics/categoryservices')->getRootCatId($orderArray ["category_id"] );
							}
						}
	
						$storeId = $order->getStoreId();
						$orderArray ['store_id'] = $storeId;
						$orderArray ['website_id'] = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
						$data [] = $orderArray;
					}
				}
	
				$retObj ['data'] = $data;
				$retObj ['lastdate'] = $lastdatesend;
				if($lastdate != $lastdatesend  )
				{
					$lastidsend=0;
				}
				else if($RowCount<$limit)
				{
					$lastidsend=0;
					$duration = 1;
					$dateinsec = strtotime($lastdatesend);
					$lastdatesend = $dateinsec+$duration;
					$lastdatesend = date('Y-m-d H:i:s',$lastdatesend);
					$retObj ['lastdate'] = $lastdatesend;
				}
				$retObj ['lastid'] = $lastidsend;
				if ($data_collection->count () < $limit && $lastid==0)
				{
					$retObj ['islast'] = true;
				}
				else
				{
					$retObj ['islast'] = false;
				}
			} else {
				$duration = 1;
				$dateinsec = strtotime($lastdate);
				$lastdatesend = $dateinsec+$duration;
				$lastdatesend = date('Y-m-d H:i:s',$lastdatesend);
				$retObj ['lastdate'] = $lastdatesend;
				$retObj ['lastid'] = 0;
				if($lastid>0)
				{
					$retObj ['islast'] = false;
				}
				else
					$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
	
			return $retObj;
		} catch ( Exception $e ) {
			$retObj ['lastid'] = -1;
			$retObj ['lastdate'] = $lastdate;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}
	
	function raaOrdersByReverseDate($maxdate,$lastdate,$lastid,$limit)
	{

		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		$RowCount = 0;
		try {
			$data_collection = Mage::getModel ( 'sales/order' )->getCollection ();
			if($lastid==0)
			{
				$data_collection = $data_collection
				->addAttributeToFilter('updated_at', array('gteq' => $maxdate))
				->addAttributeToFilter('updated_at', array('lteq' => $lastdate))				
				->addAttributeToSelect ( "*" )
				->addAttributeToSort("updated_at","DESC")
				->addAttributeToSort("entity_id","DESC");
				$data_collection->getSelect ()->limit ( $limit );
				$RowCount = $data_collection->count ();
					
			}
			else
			{
				$RowCount = Mage::getModel ( 'sales/order' )->getCollection ()
				->addAttributeToFilter('updated_at', array('gteq' => $maxdate))
				->addAttributeToFilter('updated_at', array('eq' => $lastdate))
				->addAttributeToFilter('entity_id', array('lt' => $lastid))
				->count ();
				$data_collection = $data_collection
				->addAttributeToFilter('updated_at', array('gteq' => $maxdate))
				->addAttributeToFilter('updated_at', array('eq' => $lastdate))
				->addAttributeToFilter('entity_id', array('lt' => $lastid))
				->addAttributeToSelect ( "*" )
				->addAttributeToSort("entity_id","DESC");
				$data_collection->getSelect ()->limit ( $limit );
			}
	
			if ($data_collection->count () > 0) {
				$lastdatesend = $data_collection->getLastItem ()->getUpdatedAt();
				$lastidsend=$data_collection->getLastItem()->getId();
		
				foreach ( $data_collection as $order ) {
		
					$items = $order->getAllVisibleItems();
					foreach ( $items as $item ) {
						$orderArray = array ();
						$orderArray ['entity_id'] = $order->getData ( 'entity_id' );
						$orderArray ['order_id'] = $order->getData ( 'entity_id' );
						$orderArray ['created_at'] = $order->getData ( 'created_at' );
						$orderArray ['updated_at'] = $order->getData ( 'updated_at' );
						$orderArray ['status'] = $order->getData ( 'status' );
						$orderArray ['subtotal'] = $order->getData ( 'subtotal' );
						$orderArray ['base_subtotal'] = $order->getData ( 'base_subtotal' );
						$orderArray ['grand_total'] = $order->getData ( 'grand_total' );
						$orderArray ['base_grand_total'] = $order->getData ( 'base_grand_total' );
						$orderArray ['payment_method'] = $order->getPayment()->getData('method');
						$orderArray ['coupon_code'] = $order->getData ( 'coupon_code' );
						$orderArray ['increment_id'] = $order->getData ( 'increment_id' );
						$orderData = Mage::helper ( 'core' )->jsonEncode($order->getData());
						$orderArray ['order_data'] =  Mage::helper ( 'core' )->jsonDecode($orderData);
		
						$orderArray ['customer_id'] = $order->getData ( 'customer_id' );
						$orderArray ['email'] = $order->getData ( 'customer_email' );
						$orderArray ['customer_firstname'] = $order->getData ( 'customer_firstname' );
						$orderArray ['customer_middlename'] = $order->getData ( 'customer_middlename' );
						$orderArray ['customer_lastname'] = $order->getData ( 'customer_lastname' );
						$orderArray ['customer_dob'] = $order->getData ( 'customer_dob' );
						$orderArray ['customer_group_id'] = $order->getData ( 'customer_group_id' );
						$orderArray ['customer_is_guest'] = $order->getData ( 'customer_is_guest' );
						$orderArray ['customer_prefix'] = $order->getData ( 'customer_prefix' );
						$orderArray ['customer_suffix'] = $order->getData ( 'customer_suffix' );
						$orderArray ['customer_taxvat'] = $order->getData ( 'customer_taxvat' );
						$orderArray ['customer_note'] = $order->getData ( 'customer_note' );
		
						/**currency type*/
						$orderArray ['global_currency_code']=$order->getData ( 'global_currency_code' );
						$orderArray ['base_currency_code']=$order->getData ( 'base_currency_code' );
						$orderArray ['order_currency_code']=$order->getData ( 'order_currency_code' );
						$orderArray ['store_currency_code']=$order->getData ( 'store_currency_code' );
		
		
		
						$billingAddress = Mage::helper ( 'core' )->jsonEncode($order->getBillingAddress());
						$shippingAddress = Mage::helper ( 'core' )->jsonEncode($order->getShippingAddress());
						$orderArray ['billing_address'] = Mage::helper ( 'core' )->jsonDecode($billingAddress);
						$orderArray ['shipping_address'] = Mage::helper ( 'core' )->jsonDecode($shippingAddress);
		
						$orderArray ['product_id'] = $item->getData ( 'product_id' );
						$orderArray ['sku'] = $item->getData ( 'sku' );
						$orderArray ['quantity'] = $item->getData ( 'qty_ordered' );
						$orderArray ['price'] = $item->getData ( 'price_incl_tax' );
						$orderArray ['discount'] = $item->getData ( 'discount_amount' );
						$orderArray ['base_price']=$item->getData ( 'base_price' );
						$orderArray ['base_discount']=  $item->getData ( 'base_discount_amount' );
						$orderArray ['original_price']=$item->getData ( 'original_price' );
						$orderArray ['base_original_price']=$item->getData ( 'base_original_price' );
		
						/* if($parent = $item->getParentItem()){
						 $parent_sku = $parent->getProduct()->getSku();
						if ($parent_sku != null || $parent_sku != "")
						{
						$orderArray ['sku'] = $parent_sku;
						}
						} */
		
						$prod = Mage::getModel( 'catalog/product' )->load ( $orderArray ['product_id']);
						$categoryIds = $prod->getCategoryIds();
						if (isset($categoryIds[0])){
							$_categoryids = array();
							$parentCategory="";
		
							$orderArray["category_info"] = array();
							foreach ( $categoryIds as $_category ) {
								$category = Mage::getModel('catalog/category')->load($_category);
								$cat_path = $category->getPath();
								$orderArray["category_info"][] = $cat_path;
		
								$childCategories = $category->getChildrenCount();
								$parentCategory=$_category;
								if($childCategories == "0")
								{
									$_categoryids[] = $_category;
								}
							}
		
							if(count($_categoryids)==0 && $parentCategory != "" ){
								$_categoryids[] = $parentCategory;
							}
							if(count($_categoryids) > 0)
							{
								$orderArray ["category_id"] = $_categoryids[0];
								//$orderArray ['root_category_id'] = Mage::helper('retail_analytics/categoryservices')->getRootCatId($orderArray ["category_id"] );
							}
						}
		
						$storeId = $order->getStoreId();
						$orderArray ['store_id'] = $storeId;
						$orderArray ['website_id'] = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
						$data [] = $orderArray;
					}
				}
		
				$retObj ['data'] = $data;
				$retObj ['lastdate'] = $lastdatesend;
				if($lastdate != $lastdatesend  )
				{
					$lastidsend=0;
				}
				else if($RowCount<$limit)
				{
					$lastidsend=0;
					$duration = 1;
					$dateinsec = strtotime($lastdatesend);
					$lastdatesend = $dateinsec - $duration;
					$lastdatesend = date('Y-m-d H:i:s',$lastdatesend);
					$retObj ['lastdate'] = $lastdatesend;
				}
				$retObj ['lastid'] = $lastidsend;
				if ($data_collection->count () < $limit && $lastid==0)
				{
					$retObj ['islast'] = true;
				}
				else
				{
					$retObj ['islast'] = false;
				}
			} else {
				$duration = 1;
				$dateinsec = strtotime($lastdate);
				$lastdatesend = $dateinsec - $duration;
				$lastdatesend = date('Y-m-d H:i:s',$lastdatesend);
				$retObj ['lastdate'] = $lastdatesend;
				$retObj ['lastid'] = 0;
				if($lastid>0)
				{
					$retObj ['islast'] = false;
				}
				else
					$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
		
			return $retObj;
		} catch ( Exception $e ) {
			$retObj ['lastid'] = -1;
			$retObj ['lastdate'] = $lastdate;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
		
	}
	
	public function getOrderDetails($id) {
		try {
			$raaHelper = Mage::helper ( 'retail_analytics' );
			if ($raaHelper->isModuleEnabled ()) {
				$data = array ();
	
				$currencyModel = Mage::getModel('directory/currency');
				$currencies = $currencyModel->getConfigAllowCurrencies();
				$baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
				$defaultCurrencies = $currencyModel->getConfigBaseCurrencies();
				$rates=$currencyModel->getCurrencyRates($defaultCurrencies, $currencies);
	
	
				$order =  Mage::getModel ( 'sales/order' )->load($id);
	
	
				$currencyArray = array ("base_currency_code"=>$order['base_currency_code'],
						"store_currency_code"=>$order['store_currency_code'],
						"rate_format"=>"MUL",
						"rates"=>$rates);
					
				$data ["currency"] = $currencyArray;
	
				$customerArray = array ();
				$customerArray ["customer_id"] = $order->getCustomerId();
				$customerArray ["email"] = $order->getCustomerEmail ();
				$customerArray ["firstname"] = $order->getCustomerFirstname ();
				$customerArray ["lastname"] = $order->getCustomerLastname ();
				$data ["customer"] = $customerArray;
	
	
				$productsArray = array ();
				$productArray = array ();
				$productArray["product_id"] = "-2";
				$productArray["entity_id"] = $order->getId();
				$productArray["order_id"] = $order->getId();
				$productArray["increment_id"] = $order->getIncrementId();
				$productArray["sub_total"] = $order->getSubtotal();
				$productArray["grand_total"] = $order->getGrandTotal();
				$productArray ['base_subtotal'] = $order['base_subtotal'];
				$productArray ['base_grand_total'] = $order['base_grand_total'];
				$productArray["coupon_code"] = $order['coupon_code'];
				$productArray["status"] = $order->getStatus();
				$productArray["created_at"] =  $order->getCreatedAt();
				$productArray ["store_id"] = $order->getStoreId();
				$productsArray[] = $productArray;
	
				$ordered_items = $order->getAllItems();
				Foreach($ordered_items as $item){
					$productArray = array ();
					$productArray["product_id"] = $item->getProductId();
					$productArray["sku"] = $item->getSku();
					$productArray["name"] = $item->getName();
					$productArray["quantity"] = $item->getQtyOrdered();
					$productArray["price"] = $item->getPriceInclTax();
					$productArray["discount"] = $item->getDiscountAmount();
					$productArray["producttypeid"] = $item->getProductType();
					$productsArray[] = $productArray;
				}
	
				$infoArray = array ("order_id"=>$order->getId(),"increment_id"=>$order->getIncrementId(),"created_at"=>$order->getCreatedAt());
				$orderArray = array ("products"=>$productsArray,"info"=>$infoArray);
				$data ["order"] = $orderArray;
			}
	
			return $data;
		} catch ( Exception $e ) {
			return $data;
		}
	
	}
	
	public function raaOrderSummary($from, $to) {
		$retObj = array ();
		$data = array ();
		try {
			$to = $to." 23:59:59";
			$order = Mage::getModel('sales/order')->getCollection()
			->addAttributeToFilter('created_at', array('from'  => $from, 'to' => $to));
			$orderTotals = $order->getColumnValues('base_subtotal');
			$totalSum = array_sum($orderTotals);
			$data["order_count"] = $order->count();
			$data["order_sum"] = $totalSum;
			$orderstatus = Mage::getModel('sales/order_status')->getResourceCollection()->getData();
			$data1 = $data;
			foreach ($orderstatus as $status)
			{
				$data2 = array();
				$orders =  Mage::getModel('sales/order')->getCollection()
				->addAttributeToFilter('created_at', array('from'  => $from, 'to' => $to))
				->addAttributeToFilter('status',$status["status"]);
				$ordersTotals = $orders->getColumnValues('base_subtotal');
				$data2["status"] = $status["status"];
				$data2["order_sum"] = array_sum($ordersTotals);;
				$data2["order_count"] = $orders->count();
				$data1['status'] = $data2;
				$data3[] = $data2;
				$data1['status'] = $data3;
			}
	
			$retObj["data"] = $data1;
			return $retObj;
		} catch ( Exception $e ) {
			 
			return $retObj;
		}
	}

	public function raaWholeOrders($lastId, $limit) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		try {
	
			$data_collection = Mage::getModel ( 'sales/order' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
					"gt" => $lastId
			) )->addAttributeToSelect ( "*" );
			$data_collection->getSelect ()->limit ( $limit );
	
			if ($data_collection->count () > 0) {
				$lastId = $data_collection->getLastItem ()->getId ();
				foreach ( $data_collection as $order ) {
	
					$items = $order->getAllItems ();
					foreach ( $items as $item ) {
						$orderArray = array ();
						$orderArray ['entity_id'] = $order->getData ( 'entity_id' );
						$orderArray ['order_id'] = $order->getData ( 'entity_id' );
						$orderArray ['created_at'] = $order->getData ( 'created_at' );
						$orderArray ['updated_at'] = $order->getData ( 'updated_at' );
						$orderArray ['status'] = $order->getData ( 'status' );
						$orderArray ['store_id'] = $order->getData ( 'store_id' );
	
						$orderArray ['customer_id'] = $order->getData ( 'customer_id' );
						$orderArray ['email'] = $order->getData ( 'customer_email' );
						$orderArray ['customer_firstname'] = $order->getData ( 'customer_firstname' );
						$orderArray ['customer_middlename'] = $order->getData ( 'customer_middlename' );
						$orderArray ['customer_lastname'] = $order->getData ( 'customer_lastname' );
						$orderArray ['customer_dob'] = $order->getData ( 'customer_dob' );
						$orderArray ['customer_group_id'] = $order->getData ( 'customer_group_id' );
						$orderArray ['customer_is_guest'] = $order->getData ( 'customer_is_guest' );
						$orderArray ['customer_prefix'] = $order->getData ( 'customer_prefix' );
						$orderArray ['customer_suffix'] = $order->getData ( 'customer_suffix' );
						$orderArray ['customer_taxvat'] = $order->getData ( 'customer_taxvat' );
						$orderArray ['customer_note'] = $order->getData ( 'customer_note' );
	
						$orderArray ['global_currency_code']=$order->getData ( 'global_currency_code' );
						$orderArray ['base_currency_code']=$order->getData ( 'base_currency_code' );
						$orderArray ['order_currency_code']=$order->getData ( 'order_currency_code' );
						$orderArray ['store_currency_code']=$order->getData ( 'store_currency_code' );
	
	
						$billingAddress = Mage::helper ( 'core' )->jsonEncode($order->getBillingAddress());
						$shippingAddress = Mage::helper ( 'core' )->jsonEncode($order->getShippingAddress());
						$orderArray ['billing_address'] = Mage::helper ( 'core' )->jsonDecode($billingAddress);
						$orderArray ['shipping_address'] = Mage::helper ( 'core' )->jsonDecode($shippingAddress);
	
						$orderArray ['item'] = (array) $item;
						$data [] = $orderArray;
					}
				}
	
				$retObj ['data'] = $data;
				$retObj ['lastid'] = $lastId;
				if ($data_collection->count () < $limit) {
					$retObj ['islast'] = true;
				} else {
					$retObj ['islast'] = false;
				}
			} else {
				$retObj ['lastid'] = $lastId;
				$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
	
			return $retObj;
		} catch ( Exception $e ) {
	
			$retObj ['lastid'] = - 1;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}
}