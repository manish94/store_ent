<?php
/**
 * Adminhtml base helper
 *
 * @category   Mageworks
 * @package    Mageworks_Core
 * @author     mageworks kumar <mageworksnsscoe@gmail.com>
 */
class Retail_Coupon_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * module tag name.
	 */
	public $tagName = 'ra_campaign';
	
	public function tagName() {
		return $this->tagName;
	}
}