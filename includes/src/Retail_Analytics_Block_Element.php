<?php
/**
 * Magento 
 */
class Retail_Analytics_Block_Element extends Mage_Core_Block_Template
{
    /**
     * Default id assigned to the element if none is set in the layout xml.
     */
    const DEFAULT_ID = 'raaMissingDivIdParameter';
    
    /**
     * Default class assigned to the element if none is set in the layout xml.
     */
    const DEFAULT_CLASS = 'raaMissingDivClassParameter';

    /**
     * Render HTML placeholder element for the product recommendations if the module is enabled for the current store.
     *
     * @return string
     */
    protected function _toHtml()
    {
    	$raaHelper = Mage::helper('retail_analytics');
        if (!$raaHelper->isModuleEnabled()) {
            return '';
        }
        
        $isonline = $raaHelper->getRaaConfig('isonline');
         if ($isonline == "false") {
	        return '';
	      } 
	      
        return parent::_toHtml();
    }

    /**
     * Return the id of the element. If none is defined in the layout xml, then set a default one.
     *
     * @return string
     */
    public function getElementId()
    {
        $id = $this->getDivId();
        if ($id === null) {
            $id = self::DEFAULT_ID;
        }
        return $id;
    }
    
    /**
     * Return the class of the element. If none is defined in the layout xml, then set a default one.
     *
     * @return string
     */
    public function getElementClass()
    {
    	$class = $this->getDivClass();
    	if ($class === null) {
    		$class = self::DEFAULT_CLASS;
    	}
    	return $class;
    }
}
