<?php

class Retail_Coupon_Helper_Coupon extends Mage_Core_Helper_Abstract
{
	
	public function paramsAction()
	{
		foreach($this->getRequest()->getParams() as $key=>$value) {
			//$array=Mage::helper('core')->jsonDecode($value);
			$array = Mage::helper('retail_analytics')->raaJsonDecode($value);
			var_export($array);
			$this->processRecoCoupon($array);
	
		}
	}
	
	/*The function for creating customer.*/
	public function addCustomer($customerArray)
	{
		try{
			$websiteId = Mage::app()->getWebsite()->getId();
			$store = Mage::app()->getStore();
				
			$taxClass = Mage::getModel ( 'tax/class' )->getCollection ()->addFieldToFilter ( 'class_name', 'Retail Customer' );//loads customer TaxClass
			$taxClassName = $taxClass->getData ();//Returns Taxclassname
			$taxId = $taxClassName [0] ['class_id'];//Returns TaxId
			$customer = Mage::getModel("customer/customer");
			$customer->setWebsiteId($websiteId)
			->setStoreId ( '1' )
			->setFirstname($customerArray['firstname'])
			->setLastname($customerArray['lastname'])
			->setEmail($customerArray['email'])
			->setPassword($customerArray['password'])
			->setIsActive ( '1' );
				
			if(array_key_exists ('custkey' ,$customerArray )){
				$customer->setCustkey($customerArray['custkey']);
			}
				
			$code=$customerArray['loyaltygroup'];
	
			$groupid =Mage::getModel('customer/group')->load($code, 'customer_group_code')->getCustomerGroupId();//check if the Customergroup exists or not.
			if(!$groupid)
			{
				$this->addCustomerGroup($code);//Fetch the addCustomerGroup function whic in turn sets customer group
			}
			$groupid =Mage::getModel('customer/group')->load($code, 'customer_group_code')->getCustomerGroupId();//Gets the newly created Customergroup id
				
			$customer->setGroupId($groupid);//assigns the newly Customergroup to customer.
				
			$customer->save();//Creates Customer
			//$customer->setConfirmation(null); //confirmation needed to register?
			//$customer->save(); //yes, this is also needed
			//$customer->sendNewAccountEmail(); //send confirmation email to customer?
			return true;
		}
		catch (Exception $e) {
			//Zend_Debug::dump($e->getMessage());
			return false;
		}
	
	}
	
	/*The Below function creates new Customer Group.*/
	public function addCustomerGroup($code)
	{
		//$code = 'deepakgroup';
		$taxClass = Mage::getModel ( 'tax/class' )->getCollection ()->addFieldToFilter ( 'class_name', 'Retail Customer' );
		$taxClassName = $taxClass->getData ();
		$taxId = $taxClassName [0] ['class_id'];
	
		$collection = Mage::getModel('customer/group')->getCollection() //get a list of groups
		->addFieldToFilter('customer_group_id', $code);// filter by group code
		$group = Mage::getModel('customer/group'); //load the first group with the required code - this may not be neede but it's a good idea in order to make magento dispatch all events and call all methods that usually calls when loading an object
	
		$group->setCode($code); //set the code
		$group->setTaxClassId($taxId); //set tax class
		$group->save(); //save group
		return 'true';
	}
	
	/*The Below function updates existingCustomer Group.*/
	public function updateGroup($customergroupArray)
	{
		try{
			$customer_email = $customergroupArray['email'];
			$groupName =$customergroupArray['loyaltygroup'];
	
			$groupid =Mage::getModel('customer/group')->load($groupName, 'customer_group_code')->getCustomerGroupId();//check if the Customergroup exists or not.
			if(!$groupid)
			{
				$this->addCustomerGroup($groupName);//Fetch the addCustomerGroup function whic in turn sets customer group
			}
			$groupid =Mage::getModel('customer/group')->load($groupName, 'customer_group_code')->getCustomerGroupId();//Gets the newly created Customergroup id
				
			$customers = Mage::getModel("customer/customer")->getCollection()->addAttributeToSelect("*")->addFieldToFilter('email',$customer_email)->getData();
			$customer_id = $customers[0]['entity_id'];
	
			$customer = Mage::getModel("customer/customer")->load($customer_id);
			$customer->setGroupId($groupid);
			$customer->save();
			return true;
		}
		catch (Exception $e) {
			//Zend_Debug::dump($e->getMessage());
			return false;
		}
	}
	
	/*The Below function check existing Customer and change  Group id required.*/
	public function checkCustomerGroup($customerArray,$current_groupid)
	{
		try
		{
	
			$email = $customerArray['email'];
			$customer = Mage::getModel("customer/customer")->getCollection()->addFieldToFilter('email', $email)->getData();
	
			if($customer != null)
			{
				$old_groupId =$customer[0]['group_id'];
	
				if($old_groupId != $current_groupid){
					$this->updateGroup($customerArray);
					return $old_groupId;
				}
				else{
					return 0;
				}
	
			}
			else{
				//$this->addCustomer($customerArray);
				return 0;
			}
	
		}
		catch(Exception $e)
		{
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			return -1;
		}
	}
	
	function getCustomerGroup($customer_email)
	{
		/* $customergroupmodel = Mage::getModel("customer/group");
		$customergroup=$customergroupmodel->getCollection()->addFieldToFilter('customer_group_code', $customergroup)->getData();
		var_export($customergroup); */
		
		$customer = Mage::getModel("customer/customer")->getCollection()->addAttributeToSelect("*")->addFieldToFilter('email',$customer_email)->getData();
    	$groupid = $customer[0]['group_id'];    
		return $groupid = ($groupid == null || $groupid == "") ? - 1 : $groupid;
	
	}
	
	function getALLCustomerGroup(){
		$customergroupmodel = Mage::getModel("customer/group");
		$customergroups=$customergroupmodel->getCollection()->getData();
		//var_export($customergroups);
		$groupids=array();
		foreach ( $customergroups as $group){
			$groupid= $group['customer_group_id'];
			$groupids[]=$groupid;
		}
		return $groupids;
	
	}
	
	public function getAttrValue($attr,$value)
	{
	
		$attribute_code = $attr;
		$attribute_details = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attribute_code);
		$attribute = $attribute_details->getData();		
		$options = $attribute_details->getSource()->getAllOptions();
		Foreach($options as $option){
			if($option["label"] == $value ){
				return $option["value"];
			}
		}
		return NULL;
	}
	
	/*The below function creates sku conditions and actions*/
	public function  processCoupon($reco)
	{
		try
		{
			
			$name = $reco ['couponcode'];
			if($name==null || trim($name," ")=="")
			{
				return false;
			}
			$description = $reco['campaign'];
			$fromdate = $reco ['validfrom'];
			$todate = $reco ['validto'];
	
			if ($reco ['discounttype'] == 1) {
				$actionType = 'by_percent';
			} else {
				$actionType = 'cart_fixed';
			}
				
			$discount = $reco ['discount'];
			$websiteId = 1;
			$customres=$reco['customers'];
			$usesPerCoupon =count($customres);
			
			$notLogedinGroupId = 0;
			$groupid = -1;
			$groupcode = "";
			if(array_key_exists("loyaltygroup",$reco))
			{
				$groupcode=$reco['loyaltygroup'];
				$groupid = Mage::getModel('customer/group')->load($groupcode, 'customer_group_code')->getCustomerGroupId();
				
			}
			else 
			{
				$groupid = $this->getCustomerGroup($customres[0]['email'] );
			}			
		    /* if($groupid == 0)
		    {
		    	$this->addCustomerGroup($groupcode);//Fetch the addCustomerGroup function whic in turn sets customer group
		    	$groupid = Mage::getModel('customer/group')->load($groupcode, 'customer_group_code')->getCustomerGroupId();
		    } */
		    $groupids = array();
		    if($groupid!=-1)		  
		    	$groupids[] = $groupid;		    	
		   	$groupids[] = 0;
			$conditionProducs = array ();
			$atrribute=$reco['attribute'];	
			if(strtolower($atrribute)== 'sku')
			{
				foreach ( $reco [$atrribute] as $value ) {
					$conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( 'sku' )->setOperator ( '==' )->setValue ( $value );
					$conditionProducs [] = $conditionProduct;
				}
			}
			else {
				$model = Mage::getModel('catalog/product');
				foreach ( $reco [$atrribute] as $value ) {
					$tempProd = $model->load($value);
					$tempSku=$tempProd->getSku ();
					$conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( 'sku' )->setOperator ( '==' )->setValue ( $tempSku );
					$conditionProducs [] = $conditionProduct;
				}
			}
			
			$shoppingCartPriceRule = Mage::getModel('salesrule/rule');
			
			$shoppingCartPriceRule->setName ( $name )->setDescription ( $description )->setIsActive ( 1 )->setWebsiteIds ( array (
					$websiteId
			) )->setCustomerGroupIds (
					$groupids
			 )->setSortOrder ( '' )->setSimpleAction ( $actionType )
			->setDiscountAmount ( $discount )
			->setStopRulesProcessing ( 0 )
			->setCoupon_code ( $name )
			->setFromDate ( $fromdate )
			->setToDate ( $todate )
			->setCouponType ( Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC )
			->setUses_per_customer(1)->setUses_per_coupon ( $usesPerCoupon );
				
			$conditionProductFound = Mage::getModel ( 'salesrule/rule_condition_product_found' )->setAggregator('any')->setConditions ( $conditionProducs );
				
			$conditionCart = Mage::getModel ( 'salesrule/rule_condition_product' )
			-> setType ( 'salesrule/rule_condition_address' )
			->setAttribute ( 'base_subtotal' )
			->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] );
			
			$condition = Mage::getModel ( 'salesrule/rule_condition_combine' )->setConditions ( array (
					$conditionProductFound,$conditionCart
			) );
				
			$actionconditionInner = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setAggregator('any')->setConditions ( $conditionProducs );
	
			// if using row total than use this rule otherwise uncomment below statement.
			/* $conditionProduct2 = Mage::getModel ( 'salesrule/rule_condition_product' )
			-> setType ( 'salesrule/rule_condition_product' )
			->setAttribute ( 'quote_item_row_total' )
			->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] ); */
			
			$actioncondition = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setConditions ( array (
					$actionconditionInner
					//,$conditionProduct2
			) );
			
			$shoppingCartPriceRule->setConditions ( $condition );
			$shoppingCartPriceRule->setActions ( $actioncondition );
			$shoppingCartPriceRule->save();
			/* Manage customer if thet exists or changing group*/
			
			//try{
			//$changedGroups =array();
			/* $helper = Mage::helper ('retail_customer/customer');
			foreach($customres as $customer)
			{
				$oldgroupid = $helper->checkCustomerGroup($customer,$groupid);
				if($oldgroupid  > 0)
				{
					$changedGroups[]= $oldgroupid ;
				}
			} */
			/* 
			foreach($customres as $customer)
			{
				$oldgroupid = $this->checkCustomerGroup($customer,$groupid);
				if($oldgroupid  > 0)
				{
					$changedGroups[]= $oldgroupid ;
				}
			} */
			/* if(count($changedGroups) > 0)
			{
				$this->updateCoupon($changedGroups,$groupid);
			}
			} catch ( Exception $e ) {} */
			
			// put ra log
			//$couponDataHelper = Mage::helper('retail_coupon/data');
			//$retaillogHelper = Mage::helper('retail_log/retaillog');			
			//$retaillogHelper->saveRetailLog($couponDataHelper->tagName(),$retaillogHelper->addAction(),"coupon code : ". $name. " added");
			
			return true;
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'core/session' )->addError ( Mage::helper ( 'catalog' )->__ ( $e->getMessage () ) );
			return false;
		}
	
	}
	
	function updateCoupon($GroupArray,$currentGroup){
	
		$current_date = Mage::getModel('core/date')->date('Y-m-d');
		$ruleIds = array();
		$rules = Mage::getResourceModel('salesrule/rule_collection')->addFieldToFilter('to_date', array('gteq' => $current_date))->load();
		
		foreach ($rules as $rule)
		{
			 
			
			
			$customer_ids = $rule->getCustomerGroupIds();
		    $findFlag=false;
			foreach ($customer_ids as $groupId)
			{
				
				$findFlag=false;
				if(in_array($groupId,$GroupArray)){
					$findFlag=true;
					break;
				}
			}
			
			if($findFlag == true)
			{
				try{
					if( !in_array( $currentGroup,$customer_ids)){	
						$customer_ids[]=$currentGroup;
												
						$mrule=Mage::getModel('salesrule/rule')->load( $rule->getId());						
						$mrule->setCustomerGroupIds($customer_ids);						
						$mrule->save();
						
						// put ra log
						$couponCode =$mrule->getCoupon_code();
						$couponDataHelper = Mage::helper('retail_coupon/data');
						$retaillogHelper = Mage::helper('retail_log/retaillog');
						$retaillogHelper->saveRetailLog($couponDataHelper->tagName(),$retaillogHelper->updateAction(), " customer group id ". $currentGroup." add to : coupon code : ".$couponCode);
					}
			  
				}
				catch ( Exception $e ){
					echo 'Error : ' .$e->getMessage ();
				}
				
			}
			
							
		}
		
	}
	
	
	//	$customer_ids = $rule->getCustomerGroupIds();
	 
	//		$customer_ids[]="4";
	//		var_dump($customer_ids);
	//	    $rule->setCustomerGroupIds ($customer_ids);
	//	    $rule->save();
	
	
	
	/************** Unused Old function ****************************/
	
	
	// adding coupon for recommendations.
	function processRecoCoupon($reco)
	{
		try {
			
			$name = $reco ['couponcode'];
			$description = $reco ['utilitytype'];
			$fromdate = $reco ['validfrom'];
			$todate = $reco ['validto'];
			
			if ($reco['discounttype'] == 1) {
				$actionType = 'by_percent';
			} else {
				$actionType = 'by_fixed';
			}
			
			$discount = $reco ['discount'];
			$websiteId = 1;
			$usesPerCoupon = 1;
			
			$customerGroupId = $this->getCustomerGroup( $reco['customer'] );
			
			$conditionProducs = array ();
			
			foreach ( $reco['feature'] as $key => $value ) {
				$attrvalue=null;
				$key = strtolower($key);
				if($key =='sku'){
					$attrvalue = $value;
				}
				else{
					$attrvalue = $this->getAttrValue ( $key, $value );
				}
				
				
				$conditionProduct = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( $key )->setOperator ( '==' )->setValue ( $attrvalue );
				
				$conditionProducs [] = $conditionProduct;
			}
			
			if(array_key_exists('pricerange',$reco))
			{
				$conditionProduct2 = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( 'price' )->setOperator ( '>=' )->setValue ( $reco ['pricerange'] [0] );
				$conditionProducs [] = $conditionProduct2;
				
				$conditionProduct3 = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( 'price' )->setOperator ( '<=' )->setValue ( $reco ['pricerange'] [1] );
				$conditionProducs [] = $conditionProduct3;
			}
			
			$shoppingCartPriceRule = Mage::getModel ( 'salesrule/rule' );
			
			$shoppingCartPriceRule->setName ( $name )->setDescription ( $description )->setIsActive ( 1 )->setWebsiteIds ( array (
					$websiteId 
			) )->setCustomerGroupIds ( array (
					$customerGroupId 
			) )->setFromDate ( $fromdate )->setToDate ( $todate )->setSortOrder ( '' )->setSimpleAction ( $actionType )->setDiscountAmount ( $discount )->setStopRulesProcessing ( 0 )->setCoupon_code ( $name )->setCouponType ( Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC )->setUses_per_customer ( $reco ['usagelimit'] )->setUses_per_coupon ( $usesPerCoupon );
			
			//$tempconditions=Mage::getModel ( 'salesrule/rule_condition_combine' )->setConditions ( array ($conditionProducs ));
			
			$conditionProductFound = Mage::getModel ( 'salesrule/rule_condition_product_found' )->setValue(1)->setAggregator('any')->
						setConditions($conditionProducs);
			
			$condition = Mage::getModel ( 'salesrule/rule_condition_combine' )->setConditions ( array (
					$conditionProductFound 
			) );
			
			$actioncondition = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setConditions ( $conditionProducs );
			
			$shoppingCartPriceRule->setConditions ( $condition );
			$shoppingCartPriceRule->setActions ( $actioncondition );
			$shoppingCartPriceRule->save();
			
			$this->saveRetailCoupon( $reco['customer'] , $name , $description);
			
			return true;
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'core/session' )->addError ( Mage::helper ( 'catalog' )->__ ( $e->getMessage () ) );
			return false;
		}
	}
	
	//Process Item Coupon for one customer one item
	function processCustItemCoupon($reco){
		try {
			$name = $reco ['couponcode'];
			$description = $reco ['utilitytype'];
			$fromdate = $reco ['validfrom'];
			$todate = $reco ['validto'];
				
			if ($reco ['discounttype'] == 1) {
				$actionType = 'by_percent';
			} else {
				$actionType = 'by_fixed';
			}
				
			$discount = $reco ['discount'];
			$websiteId = 1;
			$usesPerCoupon = 1;
				
			$customerGroupId = $this->getCustomerGroup ( $reco ['customer'] );
			$shoppingCartPriceRule = Mage::getModel ( 'salesrule/rule' );
				
			$shoppingCartPriceRule->setName ( $name )->setDescription ( $description )->setIsActive ( 1 )->setWebsiteIds ( array (
					$websiteId
			) )->setCustomerGroupIds ( array (
					$customerGroupId
			) )->setFromDate ( $fromdate )->setToDate ( $todate )->setSortOrder ( '' )->setSimpleAction ( $actionType )->setDiscountAmount ( $discount )->setStopRulesProcessing ( 0 )->setCoupon_code ( $name )->setCouponType ( Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC )->setUses_per_customer ( $reco ['usagelimit'] )->setUses_per_coupon ( $usesPerCoupon );;
				
			$conditionProduct1 = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( 'sku' )->setOperator ( '==' )->setValue ( $reco ['item']);
				
			$conditionProduct2 = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( 'quote_item_row_total' )->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] );
				
			$conditionProductFound = Mage::getModel ( 'salesrule/rule_condition_product_found' )->setConditions ( array (
					$conditionProduct1
					//,$conditionProduct2
			) );
				
			$condition = Mage::getModel ( 'salesrule/rule_condition_combine' )->setConditions ( array (
					$conditionProductFound
			) );
				
			$actioncondition = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setConditions ( array (
					$conditionProduct1,
					$conditionProduct2
			) );
				
			$shoppingCartPriceRule->setConditions( $condition );
			$shoppingCartPriceRule->setActions ( $actioncondition );
			$shoppingCartPriceRule->save ();
			$this->saveRetailCoupon( $reco['customer'] , $name , $description);
			return true;
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'core/session' )->addError ( Mage::helper ( 'catalog' )->__ ( $e->getMessage () ) );
			return false;
		}
	}
	
	// adding coupon for all customers of a single items
	function processItemCoupon($reco){
		try {
			$name = $reco ['couponcode'];
			$description = $reco ['utilitytype'];
			$fromdate = $reco ['validfrom'];
			$todate = $reco ['validto'];
				
			if ($reco ['discounttype'] == 1) {
				$actionType = 'by_percent';
			} else {
				$actionType = 'by_fixed';
			}
				
			$discount = $reco ['discount'];
			$websiteId = 1;
				
			$customerGroupIds = $this->getALLCustomerGroup();
			$shoppingCartPriceRule = Mage::getModel ( 'salesrule/rule' );
				
			$shoppingCartPriceRule->setName ( $name )->setDescription ( $description )->setIsActive ( 1 )->setWebsiteIds ( array (
					$websiteId
			) )->setCustomerGroupIds ( $customerGroupIds )->setFromDate ( $fromdate )->setToDate ( $todate )->setSortOrder ( '' )->setSimpleAction ( $actionType )->setDiscountAmount ( $discount )->setStopRulesProcessing ( 0 )->setCoupon_code ( $name )->setCouponType ( Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC )->setUses_per_customer ( $reco ['usagelimit'] );
				
			$conditionProduct1 = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( 'sku' )->setOperator ( '==' )->setValue ( $reco ['item']);
				
			$conditionProduct2 = Mage::getModel ( 'salesrule/rule_condition_product' )->setType ( 'salesrule/rule_condition_product' )->setAttribute ( 'quote_item_row_total' )->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] );
				
			$conditionProductFound = Mage::getModel('salesrule/rule_condition_product_found')->setConditions ( array (
					$conditionProduct1
					//,$conditionProduct2
			) );
				
			$condition = Mage::getModel ( 'salesrule/rule_condition_combine' )->setConditions ( array (
					$conditionProductFound
			) );
				
			$actioncondition = Mage::getModel ( 'salesrule/rule_condition_product_combine' )->setConditions ( array (
					$conditionProduct1,
					$conditionProduct2
			) );
				
			$shoppingCartPriceRule->setConditions( $condition );
			$shoppingCartPriceRule->setActions ( $actioncondition );
			$shoppingCartPriceRule->save ();
			$this->saveRetailCoupon( $reco['customer'] , $name , $description);
			return true;
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'core/session' )->addError ( Mage::helper ( 'catalog' )->__ ( $e->getMessage () ) );
			return false;
		}
	}
	
	// adding receipt for single customer.
	function processReceiptCoupon($reco){
		try {
			$name = $reco ['couponcode'];
			$description = $reco ['utilitytype'];
			$fromdate = $reco ['validfrom'];
			$todate = $reco ['validto'];
				
			if ($reco ['discounttype'] == 1) {
				$actionType = 'by_percent';
			} else {
				//$actionType = 'by_fixed';
				$actionType = 'cart_fixed';
			}
				
			$discount = $reco ['discount'];
			$websiteId = 1;
			$usesPerCoupon = 1;
				
			$customerGroupId = $this->getCustomerGroup ( $reco ['customer'] );
				
			$shoppingCartPriceRule = Mage::getModel ( 'salesrule/rule' );
				
			$shoppingCartPriceRule->setName ( $name )->setDescription ( $description )->setIsActive ( 1 )->setWebsiteIds ( array (
					$websiteId
			) )->setCustomerGroupIds ( array (
					$customerGroupId
			) )->setFromDate ( $fromdate )->setToDate ( $todate )->setSortOrder ( '' )->setSimpleAction ( $actionType )->setDiscountAmount ( $discount )->setStopRulesProcessing ( 0 )->setCoupon_code ( $name )->setCouponType ( Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC )->setUses_per_customer ( $reco ['usagelimit'] )->setUses_per_coupon ( $usesPerCoupon );;
				
			$condition = Mage::getModel ( 'salesrule/rule_condition_address' )->setType ( 'salesrule/rule_condition_address' )->setAttribute ( 'base_subtotal' )->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] );
				
			$shoppingCartPriceRule->getConditions()->addCondition($condition );
			$shoppingCartPriceRule->save ();
				
			$this->saveRetailCoupon( $reco['customer'] , $name , $description);
			return true;
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'core/session' )->addError ( Mage::helper ( 'catalog' )->__ ( $e->getMessage () ) );
			return false;
		}
	}
	
		
	// adding coupon for receipt for all customers.
	function processALLReceiptCoupon($reco){
		try {
			$name = $reco ['couponcode'];
			$description = $reco ['utilitytype'];
			$fromdate = $reco ['validfrom'];
			$todate = $reco ['validto'];
			
			if ($reco ['discounttype'] == 1) {
				$actionType = 'by_percent';
			} else {
				//$actionType = 'by_fixed';
				$actionType = 'cart_fixed';
			}
			
			$discount = $reco ['discount'];
			$websiteId = 1;
			
			$customerGroupIds = $this->getALLCustomerGroup();
			
			$shoppingCartPriceRule = Mage::getModel ( 'salesrule/rule' );
			
			$shoppingCartPriceRule->setName ( $name )->setDescription ( $description )->setIsActive ( 1 )->setWebsiteIds ( array (
					$websiteId 
			) )->setCustomerGroupIds ( $customerGroupIds )->setFromDate ( $fromdate )->setToDate ( $todate )->setSortOrder ( '' )->setSimpleAction ( $actionType )->setDiscountAmount ( $discount )->setStopRulesProcessing ( 0 )->setCoupon_code ( $name )->setCouponType ( Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC )->setUses_per_customer ( $reco ['usagelimit'] );
			
			$condition = Mage::getModel ( 'salesrule/rule_condition_address' )->setType ( 'salesrule/rule_condition_address' )->setAttribute ( 'base_subtotal' )->setOperator ( '>=' )->setValue ( $reco ['minamtbuy'] );
			
			$shoppingCartPriceRule->getConditions ()->addCondition ( $condition );
			$shoppingCartPriceRule->save();
			$this->saveRetailCoupon( $reco['customer'] , $name , $description);
			return true;
		} catch ( Exception $e ) {
			Mage::getSingleton ( 'core/session' )->addError ( Mage::helper ( 'catalog' )->__ ( $e->getMessage () ) );
			return false;
		}
	}
		
		
	function saveRetailCoupon($email, $couponcode, $recotype)
	{
		$retailcoupon = Mage::getModel("retail_coupon/retailcoupon");
		$retailcoupon->setEmail ( $email );
		$retailcoupon->setCouponcode ( $couponcode );
		$retailcoupon->setRecotype ( $recotype );		
		$retailcoupon->setStatus ( 'Active' );
		$retailcoupon->setCreated ( now() );
		$retailcoupon->save();
	}
	
		
}