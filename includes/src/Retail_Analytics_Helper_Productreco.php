<?php

class Retail_Analytics_Helper_Productreco extends Mage_Core_Helper_Abstract
{
		
	public function saveProductReco($data) {
		$productreco = Mage::getModel('retail_analytics/productreco');
		$productreco->setProductmap($data['from']);
		$productreco->setMap($data['map']);
		$productreco->setWeight($data['weight']);
		$productreco->setCreated(now());
		$productreco->setModified(now());
		$productreco->save();
	}	
	
	public function addProductReco($data) {
	
		try {
			foreach ( $data as $row ) {
				$this->saveProductReco( $row );
			}
			
			return true;
		}			
		catch ( Exception $e ) {
			return false;
		}
	}	

	public function getProductRecoLastItem() {
		try{
	
			$data = array();
			$data_collection = Mage::getModel ( 'retail_analytics/productreco' )->getCollection ();
			$data = $data_collection->getLastItem()->getData();
			return $data;
		}
		catch ( Exception $e ) {
			echo json_encode ( $data );
		}
	}
	
	public function getRecoProductsById($productid) {
				
		try{
			$data = array();
			$productIdies = array();
			$map="";
			$data_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'productid', array ($productid) );
			 
			if ($data_collection->count () > 0) {
				$map = $data_collection->getLastItem()->getMap();
		
				$reco_collection = Mage::getModel ( 'retail_analytics/productreco' )->getCollection ()->addFieldToFilter ( 'Productmap', array ($map));
				if ($reco_collection->count () > 0) {
					$map = $reco_collection->getLastItem()->getMap();
						
					$map_collection = Mage::getModel ( 'retail_analytics/productmap' )->getCollection ()->addFieldToFilter ( 'map', array ($map));
					if ($map_collection->count () > 0) {
							
						foreach ( $map_collection as $map ) {
							$id = $map->getProductid();
							if($id != $productid){
								$productIdies[] = $id;
							}
						}
							
						$product_collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addFieldToFilter ( 'entity_id', array (
								"in" => $productIdies
						) )->addAttributeToSelect ( "*" );
							
					foreach ( $product_collection as $product ) {
						$data[] = $product->getData();
					}
					}
				}
			}
		
			return $data;
		}
		catch ( Exception $e ) {
			echo json_encode ( $data );
		}
	}
	
	
}