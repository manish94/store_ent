<?php
class Retail_Log_Helper_Retaillog extends Mage_Core_Helper_Abstract
{	
	
	/**
	 * add action.
	 */
	public $addAction = 'add';
	/**
	 * update action.
	 */
	public $updateAction = 'update';
	/**
	 * delete action.
	 */
	public $deleteAction = 'delete';
	/**
	 * module tag name.
	 */
	public $tagName = 'ra_tag';
	
	public function addAction() {
		return $this->addAction;
	}	
	public function updateAction() {
		return $this->updateAction;
	}	
	public function deleteAction() {
		return $this->deleteAction;
	}
	
	public function tagName() {
		return $this->tagName;
	}
	
	
	public function saveRetailLog($tag, $action ,$message) {
		try {
			$retaillog = Mage::getModel('retail_log/retaillog');		
			$retaillog->setTag($tag);
			$retaillog->setAction($action);
			$retaillog->setMessage($message);
			$retaillog->setDate(now());		
			$retaillog->save();
			return true;
		} catch ( Exception $ex ) {
			return false;
		}
	}
	
	public function getRetailLog() {
		$data = array ();
		try {
			$retaillogs = Mage::getModel('retail_log/retaillog')->getCollection();
			foreach ($retaillogs as $retaillog)
			{
				$data[] = $retaillog->getData();				
			}
		
			return $data;
		} catch ( Exception $ex ) {
			return $data;
		}
	}
}