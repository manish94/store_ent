<?php
class Retail_Analytics_Helper_Data2 extends Mage_Core_Helper_Abstract {
	
	
	
	public function sendEmail($emailto,$emailtoname) {
		
		try {	
		
        
	       // This is the template name from your etc/config.xml 
			$template_id = 'raa_email_template';
			
			// Who were sending to...
		
			
			// Load our template by template_id
			$emailTemplate = Mage::getModel('core/email_template')->loadDefault($template_id);
			
			// Here is where we can define custom variables to go in our email template!
			$email_template_variables = array(
			    'customer_name' => $emailtoname
			    // Other variables for our email template.
			);
			
			// I'm using the Store Name as sender name here.
			//$sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
			$sender_name = "raa test1";
			// I'm using the general store contact here as the sender email.
			//$sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
			$sender_email = "raatest@gmail.com";
			$emailTemplate->setSenderName($sender_name);
			$emailTemplate->setSenderEmail($sender_email); 
			
			//Send the email!
			$emailTemplate->send($emailto, $emailtoname, $email_template_variables);
			return true;
		} catch ( Exception $ex ) {
			$errorMessage = $e->getMessage();
            return $errorMessage;
		}
	}
	
	public function executeReadQuery($query)
	{			
		//database read adapter 
		$read = Mage::getSingleton('core/resource')->getConnection('core_read'); 	
			
		 /* 
			 query e.g: select * from raa_config where id=1 limit 1;	 		
		 */				
		
		$result = $read->fetchAll($query);			
		
		$data = array("query"=>$query,"result"=>$result);
		return $data;
			
	}
	
	public function executeWriteQuery($table,$data)
	{
		//database write adapter 
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			
		/* 	data e.g :
			$data = array(
					array('cal_1'=>'value','cal_2'=>'value','cal_3'=>'value'),
					array('cal_1'=>'value','cal_2'=>'value','cal_3'=>'value')
			);
		 */
		
		$result = $write->insertMultiple($table,$data);
	
		$data = array("table"=>$table,"result"=>$result);
		return $data;
			
	}
	
	public function executeDeleteQuery($table,$query)
	{
		//database write adapter
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		
		//query e.g : id=1;
		$result = $write->delete($table,$query);
	
		$data = array("table"=>$table,"query"=>$query,"result"=>$result);
		return $data;
			
	}
	
	public function addCmsBlock($identifier,$title,$content) {
		
		try {
			
			Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);	
							
			$cmsBlock = Mage::getModel('cms/block')->load($identifier);
			if ($cmsBlock->isObjectNew()) {
				$cmsBlock->setTitle($title)
				->setContent($content)
				->setIdentifier($identifier)
				->setStores(0)
				->setIsActive(true);
			} else {
				$cmsBlock->setTitle($title)
				->setContent($content)
				->setStores(0)
				->setIsActive(true);
			}
			
			$cmsBlock->save();
			return true;
			
		} catch ( Exception $ex ) {
			return false;
		}
	}
	
	public function getCmsBlock($identifier) {
		$data = array ();
		try {
				
				$cmsBlock = Mage::getModel('cms/block')->load($identifier);
				if (!$cmsBlock->isObjectNew()) {
					$data['identifier'] =  $identifier;
					$data['title'] = $cmsBlock->getTitle();
					$data['content'] =  $cmsBlock->getContent();					
				}
				
			return $data;	
				
		} catch ( Exception $ex ) {
			return $data;
		}
	}
	
	public function deleteCmsBlock($identifier) {
		
		try {
	
			$cmsBlock = Mage::getModel('cms/block')->load($identifier);
			if (!$cmsBlock->isObjectNew()) {
				$cmsBlock->delete();
			}
	
			return true;
	
		} catch ( Exception $ex ) {
			return false;
		}
	}
	
	
	public function getCmsBlockContent($identifier) {
		
		$data;
		try {
	
			$cmsBlock = Mage::getModel('cms/block')->load($identifier);
			if (!$cmsBlock->isObjectNew()) {				
				$data =  $cmsBlock->getContent();
			}
	
			return $data;
	
		} catch ( Exception $ex ) {
			return $data;
		}
	}
	
	public function getCurrencyData() {
		$data = array ();
		try {
				
			$currencyModel = Mage::getModel('directory/currency');
			$currencies = $currencyModel->getConfigAllowCurrencies();
			$baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
			$defaultCurrencies = $currencyModel->getConfigBaseCurrencies();
			$rates=$currencyModel->getCurrencyRates($defaultCurrencies, $currencies);
				
			$data["base_currency_code"] = $baseCurrencyCode;
			$data["store_currency_code"] = $defaultCurrencies;
			$data["rates"] = $rates;	
			
			return $data;
		} catch ( Exception $ex ) {
			return $data;
		}
	}
	
	public function raaStoreCounts() {
		$data = array ();
		try {
			
			$customers = Mage::getModel ( 'customer/customer' )->getCollection ();
			$data["Customer"] = $customers->getSize();	
			$products = Mage::getModel ( 'catalog/product' )->getCollection ();
			$data["Product"] = $products->getSize();
			$orders = Mage::getModel ( 'sales/order' )->getCollection ();
			$data["Order"] = $orders->getSize();
			
			return $data;
		} catch ( Exception $ex ) {
			return $data;
		}
	}
	
	public function raaGetStores() {
		$dataArray = array ();
		try {
			foreach ( Mage::app ()->getWebsites () as $website ) {
				$data = array ();
				$data ["website"] = $website ["name"];
				$data ["websiteid"] = $website ["website_id"];
				$data ["websitecode"] = $website ["code"];
				$data ["store"] = array ();
				foreach ( $website->getGroups () as $store ) {
					$storedata = array ();
					$storedata ["storename"] = $store ["name"];
					$storedata ["storeid"] = $store ["group_id"];
					$storedata ["rootcategoryid"] = $store ["root_category_id"];
					$storedata ["view"] = array ();
					$stores = $store->getStores ();
					foreach ( $stores as $storeview ) {
						$viewdata = array ();
						$viewdata ["viewname"] = $storeview ["name"];
						$viewdata ["viewid"] = $storeview ["store_id"];
						$viewdata ["isactive"] = $storeview ["is_active"];
						$viewdata ["viewcode"] = $storeview ["code"];
						$viewdata ["homeurl"] = $storeview->getHomeUrl();
						$storedata ["view"] [] = $viewdata;
					}
					$data ["store"] [] = $storedata;
				}
				$dataArray [] = $data;
			}
			return $dataArray;
		} catch ( Exception $e ) {
			return $dataArray;
		}
	}
			
	public function raaGetAjaxScript($url) {
		$tempScript= "";
		
	try {
		
		$raaHelper = Mage::helper('retail_analytics');
		$raa_appid = $raaHelper->getAccount();
		$raa_serverurl = $raaHelper->getServer();
		
		$tempScript = '	
		raa_service_message = document.getElementById("raa-service-message");
		var windowLocation = window.location.href;			
		var xhr = new XMLHttpRequest();
		var url =	(window.location.protocol == "https:")?"https://'.$raaHelper->getServer().':8443" : "http://'.$raaHelper->getServer().':8080";		
		url +=  "/'.$url.$raa_appid.'";							
		xhr.open("get",url, true);
		xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xhr.onreadystatechange = function () {	
			if (xhr.readyState == 4)
			{
				raa_service_message.innerHTML = "Press browser back button to continue.";
				if (xhr.status == 200 || xhr.status == 304)
				{
					if(xhr.responseText == true || xhr.responseText == "true")
					{
						alert("Press browser back button to continue.");
					}
					else
					{
						alert("Could not connecting to Retail Automata Server");
					}
									
				}
			}
		};					        			
				
		xhr.send()';
		
		return $tempScript;
		
		} catch ( Exception $e ) {
			return $tempScript;
		}
	}
	
	public function getRaaImageURL($product)
    {
    	$product->getData();
    	
    	$SmallImage =  $product->getSmallImage();
		if($SmallImage == null || $SmallImage == "" || $SmallImage == 'no_selection' )
		{
			$Image = $product->getImage();
			$thumnailImage = $product->getThumbnail();
			if($Image!=null && $Image!="" && $Image != 'no_selection')
				return $product->getMediaConfig()->getMediaUrl($Image);
			else if($thumnailImage!=null && $thumnailImage!="" && $thumnailImage != 'no_selection')
				return $product->getMediaConfig()->getMediaUrl($thumnailImage);
			else
				return  $product->getImageUrl();
		}
		else
		{
			return $product->getMediaConfig()->getMediaUrl($SmallImage);
		}
    }
          
    public function raaBestSellingProducts($fromDate=null,$period='month')
    {
    	$retObj = array ();
    	$data = array ();    	
    	$data2 = array ();
    	try {
    		
    		$date = new Zend_Date();
    		$toDate = $date->getDate()->get('Y-MM-dd');
    		//$fromDate = $date->subMonth(1)->getDate()->get('Y-MM-dd');    		
	    	$storeId    = Mage::app()->getStore()->getId();
	    	
	    	$totalsCollection = Mage::getResourceModel('sales/report_bestsellers_collection')    	
	    	->setPeriod($period);
	    	//->setDateRange($fromDate, $toDate);
	    	//->addStoreFilter($storeId)
	    	//->setAggregatedColumns($this->_getAggregatedColumns())
	    	//->isTotals(true);
	    	
	    	  	
	    	if($fromDate != null){
	    		$totalsCollection = $totalsCollection->setDateRange($fromDate, $toDate);
	    	}    
	    	
	    	$productQty = array();
	    	
	    	foreach ($totalsCollection as $item) {
	    		$productArray = array();
	    		
	    		$productArray['productid'] = $item->getData('product_id');
	    		$productArray['name'] =  $item->getData('product_name');
	    		$productArray['sku'] =  $item->getData('sku');
	    		
	    		if($productArray['sku'] == null || $productArray['sku'] == "")
	    		{
	    			$prod = Mage::getModel('catalog/product')->load($productArray['productid']);
	    			if($prod) {
	    				$productArray['sku'] = $prod->getSku();
	    			}
	    		}
	    		    		
	    		
		    	if (array_key_exists($productArray['productid'], $productQty)) {
				    $productQty[$productArray['productid']] = intval( intval($productQty[$productArray['productid']]) + intval($item->getData('qty_ordered')));
				}
				else {
					$productQty[$productArray['productid']] = intval($item->getData('qty_ordered'));
					$data2[] = $productArray;
				}		
						
				//$productArray['ordered_qty'] = $productQty[$productArray['name']];
	    		//$productArray['ordered_qty'] = $item->getData('qty_ordered');	    	    				
	    		
			}    		
	    	
			
			foreach ($data2 as $row)
			{
				$tempArray = $row;
				$tempArray['ordered_qty'] = $productQty[$tempArray['productid']];					
				$data[] = $tempArray;
			} 
			
			$productQtyArray = array();
			foreach ($data as $key => $row)
			{
			    $productQtyArray[$key] = $row['ordered_qty'];
			}
			
			array_multisort($productQtyArray, SORT_DESC, $data);
			
    		$retObj["data"] = $data;
    		return $retObj;
    	
    	} catch ( Exception $e ) {
    		 
    		return $retObj;
    	}
    }
    
    public function getModelSize($model) {
    	 
    	$data = array ();
    
    	try {
    		 
    		$data_collection = Mage::getModel (  'retail_analytics/'.$model )->getCollection ();  
    		$data = array("model"=>$model,"size"=>$data_collection->getSize());
    		return $data;
    	} catch ( Exception $ex ) {
    		return $data;
    	}
    }
    
    public function getModelData($model,$limit,$key,$value) {    	
		$data = array ();			
    	try {
    			
    		$data_collection = Mage::getModel (  'retail_analytics/'.$model )->getCollection ();  
    		
    		if($key != "")
    		{
    			$data_collection->addFieldToFilter ( $key, array ($value) );
    		}  
    				    		  		
    		if($limit > 0)
    		{
    			$data_collection->getSelect ()->limit ( $limit );
    		}
    		
    		if ($data_collection->count () > 0) {				
				foreach ( $data_collection as $modelRow ) {
					$data[] = $modelRow->getData();
				}				
				
    		}    			
    		return $data;
    	} catch ( Exception $ex ) {    		
			return $data;
    	}
    }    
      
    
    public function removeModelData($model,$limit,$key,$value) {
    	
    	try {
    		 
    		$data_collection = Mage::getModel (  'retail_analytics/'.$model )->getCollection ();
    		
    		if($key != "")
    		{
    			$data_collection->addFieldToFilter ( $key, array ($value) );
    		}
    		
    		if($limit > 0)
    		{
    			$data_collection->getSelect ()->limit ( $limit );
    		}
    
    		if ($data_collection->count () > 0) {
    			foreach ( $data_collection as $modelRow ) {
    				$id =  $modelRow->getData('id');
    				$dataModel = Mage::getModel (  'retail_analytics/'.$model )->load($id);
    				$dataModel->delete();
    			}    
    		}
    		return true;
    	} catch ( Exception $ex ) {
    		return false;
    	}
    }
    
   
}