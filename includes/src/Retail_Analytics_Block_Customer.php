<?php
/** 
 */
class Retail_Analytics_Block_Customer extends Mage_Customer_Block_Account_Dashboard
{
    /**
     * Render customer info as hidden meta data if the customer is logged in,
     * the module is enabled for the current store and the "collect_email_addresses" option is enabled.
     *
     * @return string
     */
    protected function _toHtml()
    {
    	$raaHelper = Mage::helper('retail_analytics');
        if (!$this->helper('customer')->isLoggedIn()  || !$raaHelper->isModuleEnabled()) {
            return '';
        }
        
       /*  $isonline = $raaHelper->getRaaConfig('isonline');
        if ($isonline == "false" || $isonline == false) {
        	return '';
        } */
        
        return parent::_toHtml();
    }
}
