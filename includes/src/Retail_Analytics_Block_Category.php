<?php
/**
 * Magento 
 */
class Retail_Analytics_Block_Category extends Mage_Core_Block_Template
{
    /**
     * Cached category string.
     *
     * @var string
     */
    protected $_category;

    /**
     * Render category string as hidden meta data if the module is enabled for the current store.
     *
     * @return string
     */
    protected function _toHtml()
    {
        $raaHelper = Mage::helper('retail_analytics');
        if (!$raaHelper->isModuleEnabled()) {
            return '';
        }
        
        /* $isonline = $raaHelper->getRaaConfig('isonline');
        if ($isonline == "false") {
        	return '';
        } */
        
        return parent::_toHtml();
    }

    /**
     * Return the current product category string for the category tagging.
     *
     * @return string
     */
    public function getCategory()
    {
        /* if (!$this->_category) {
            $category = Mage::registry('current_category');
            $this->_category = Mage::helper('retail_analytics')->buildCategoryString($category);
        }
        return $this->_category; */
    	
    	$category = Mage::registry('current_category');
    	$this->_category = $category->getId();
    	return $this->_category; 
    }
}
