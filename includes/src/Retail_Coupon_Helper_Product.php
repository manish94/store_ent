<?php

class Retail_Coupon_Helper_Product extends Mage_Core_Helper_Abstract
{
	public function operatorToValue($operator)
	{
		switch($operator)
		{
			case '==' :
				return "eq";
			case '>=' :
				return "gt";
			case '<=' :
				return "lt";
			default :
				return null;
		}
	
	}
	
	public function processcondition($con){
		$outcond=$con['conditions'];
		
		$innCondition=	$outcond[0]['conditions'];
		
		$RetArry= array();
		//Zend_Debug::dump($outcond,$label = 'outcond : ', $echo = true);
		
		
		foreach ($innCondition as $rule){
			//var_dump($rule);
			
			$operatorValue =$this->operatorToValue($rule['operator']);
			$tempArray=array();
			$tempArray['attribute']=$rule['attribute'];
			$tempArray['operator']=$operatorValue ;
			$tempArray['value']=$rule['value'];
			
			if($tempArray['value']== null || $tempArray['value']=="")
			{
				return null;
			}
			$RetArry[]=$tempArray;
			//var_dump($tempArray);
			//var_dump($RetArry);
	
		}
		//var_dump($RetArry);
		return $RetArry;
			
	}
	
		
	public function getConditionProduct($conArray){
			
		$collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->setOrder('sku', 'desc');		
		$productColls = array();
		foreach ($conArray as $condition) {
			$outArray=array();
			$innerArray=array();
			$innerArray['attribute']=$condition['attribute'];
			$innerArray[$condition['operator']]=$condition['value'];
			$outArray[]=$innerArray;
			$collection->addFieldToFilter($outArray);
		}
		
		return $collection;
		
		
	}

	public function processRule($id)
	{
		$productColls = null;
		try {		
			$shoppingCartPriceRule = Mage::getModel('salesrule/rule')->load($id);
			
		    $ruleCondition=$shoppingCartPriceRule->getConditions();
		    
			$condArray=$this->processcondition($ruleCondition);	
			//var_dump($condArray);
			if($condArray != null && $condArray !="")
			{	
				$productColls = $this->getConditionProduct($condArray);
			}
		}
		catch (Exception $e)
		{
		}
		return $productColls ;
		
	}
	
/* 	public  function getcondAction(){
		//$this->getcondArray();
		$this->processRule(45);
	} */
}