<?php

class Retail_Analytics_Helper_Customernonreco extends Mage_Core_Helper_Abstract
{
		
	public function saveCustomerNonReco($data) {
		$customernonreco = Mage::getModel('retail_analytics/customernonreco');
		$customernonreco->setCustomerid($data['from']);
		$customernonreco->setEmail($data['email']);
		$customernonreco->setMap($data['map']);
		$customernonreco->setWeight($data['weight']);
		$customernonreco->setCreated(now());
		$customernonreco->setModified(now());
		$customernonreco->save();
	}	
	
	public function addCustomerNonReco($data) {
	
		try {
			foreach ( $data as $row ) {
				$this->saveCustomerNonReco( $row );
			}
			return true;
		}			
		catch ( Exception $e ) {
			return false;
		}
	}	

	
	
}