<?php
class Retail_Analytics_Model_Resource_Recentlyviewed_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {
	
	protected function _construct()
	{
		$this->_init('retail_analytics/recentlyviewed');
	}
	
	public function addAttributeToSort($attribute, $dir=�asc�)
	{
		if (!is_string($attribute)) {
			return $this;
		}
		$this->setOrder($attribute, $dir);
		return $this;
	}
}