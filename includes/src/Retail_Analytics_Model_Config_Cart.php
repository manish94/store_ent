<?php

class Retail_Analytics_Model_Config_Cart extends Mage_Core_Model_Config_Data
{
	
	public function toOptionArray()
	{
		return array(
				//array('value'=>'', 'label'=>''),
				array('value'=>'12', 'label'=>'Related'),
				array('value'=>'13', 'label'=>'Upsell'),	
				array('value'=>'22', 'label'=>'Reco Self'),
				array('value'=>'23', 'label'=>'Reco Noneself')				
		);
	}
	 /**
     * Save object data.
     * Validates that the account is set.
     *
     * @return Nosto_Tagging_Model_Config_Account
     */
    public function save()
    {
        $cart = $this->getValue();
        /* if (empty($cart)) {
            Mage::throwException(Mage::helper('retail_analytics')->__('Show cart recommendations is required.'));
        } */

        return parent::save();
    }
}
