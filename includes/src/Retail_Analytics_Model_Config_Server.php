<?php

class Retail_Analytics_Model_Config_Server extends Mage_Core_Model_Config_Data
{
	/**
	 * Save object data.
	 * Validates that the server is set and does not include the protocol.
	 *
	 * @return Nosto_Tagging_Model_Config_Server
	 */
	public function save()
	{
		$server = $this->getValue();

		if (empty($server)) {
			Mage::throwException(Mage::helper('retail_analytics')->__('Server URL is required.'));
		}

		 $pattern = '@^https?://@i';
		if (preg_match($pattern, $server)) {
			Mage::throwException(
				Mage::helper('retail_analytics')->__(
					'The server address should not include the protocol (http:// or https://).'
				)
			);
		} 

		return parent::save();
	}
}
