<?php

class Retail_Analytics_OnsiteController extends Mage_Core_Controller_Front_Action {
	
	protected function _construct()
	{
		$isAllow = Mage::helper('retail_analytics/data')->raaValidateIP();
		if(!$isAllow) {
			print('Access Denied!');
			$this->_redirect('*/*/ttdstttsdt');
		}
	}
	
	public function indexAction() {			
			
		echo 'hello Retail_Analytics Onsite index called.';		
		$ProductFilter = Mage::helper('retail_analytics/onsite')->getProductFilter();
		var_dump($ProductFilter);
	}
	
	public function raaAddCmsBlockAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			$identifier = $params ['identifier'];	
			$title = $params ['title'];
			$content = $params ['content'];		
			$helper = Mage::helper('retail_analytics/data2');
			$result = $helper->addCmsBlock($identifier,$title,$content);
			echo $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaGetCmsBlockAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			$identifier = $params ['identifier'];			
			$helper = Mage::helper('retail_analytics/data2');
			$data = $helper->getCmsBlock($identifier);			
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	public function raaDeleteCmsBlockAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			$identifier = $params ['identifier'];
			$helper = Mage::helper('retail_analytics/data2');
			$result = $helper->deleteCmsBlock($identifier);
			echo $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaGetCmsBlockContentAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			$identifier = $params ['identifier'];
			$helper = Mage::helper('retail_analytics/data2');
			$data = $helper->getCmsBlockContent($identifier);			
			echo $data;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	
	public function raaAddProductMapAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			//$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);
			$helper = Mage::helper('retail_analytics/productmap');
			$result = $helper->addProductMap($data);			
			echo $result;	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAddProductMap2Action() {
		try{
			$params = $this->getRequest ()->getParams ();
			$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );			
			$helper = Mage::helper('retail_analytics/productmap');
			$result = $helper->addProductMap($data);
			echo $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAddProductMap3Action() {
		try{
			$params = $this->getRequest ()->getParams ();
			$data = json_decode( $params ['request'] );
			$helper = Mage::helper('retail_analytics/productmap');
			$result = $helper->addProductMap($data);
			echo $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAddProductRecoAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			//$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);
			$helper = Mage::helper('retail_analytics/productreco');
			$result = $helper->addProductReco($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
		
	public function raaAddProductReco2Action() {
		try{
			$params = $this->getRequest ()->getParams ();
			$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$helper = Mage::helper('retail_analytics/productreco');
			$result = $helper->addProductReco($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}

	public function raaAddProductReco3Action() {
		try{
			$params = $this->getRequest ()->getParams ();
			$data = json_decode( $params ['request'] );
			$helper = Mage::helper('retail_analytics/productreco');
			$result = $helper->addProductReco($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	public function raaAddCustomerRecoAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			//$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);
			$helper = Mage::helper('retail_analytics/customerreco');
			$result = $helper->addCustomerReco($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAddCustomerReco2Action() {
		try{
			$params = $this->getRequest ()->getParams ();
			$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$helper = Mage::helper('retail_analytics/customerreco');
			$result = $helper->addCustomerReco($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAddCustomerReco3Action() {
		try{
			$params = $this->getRequest ()->getParams ();
			$data = json_decode( $params ['request'] );
			$helper = Mage::helper('retail_analytics/customerreco');
			$result = $helper->addCustomerReco($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAddCustomerNonRecoAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			//$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);
			$helper = Mage::helper('retail_analytics/customernonreco');
			$result = $helper->addCustomerNonReco($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAddCustomerNonReco2Action() {
		try{
			$params = $this->getRequest ()->getParams ();
			$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$helper = Mage::helper('retail_analytics/customernonreco');
			$result = $helper->addCustomerNonReco($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAddCustomerNonReco3Action() {
		try{
			$params = $this->getRequest ()->getParams ();
			$data = json_decode( $params ['request'] );
			$helper = Mage::helper('retail_analytics/customernonreco');
			$result = $helper->addCustomerNonReco($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	public function raaAddOnSiteConfigAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			//$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);
			$helper = Mage::helper('retail_analytics/onsiteconfig');
			$result = $helper->addOnsiteconfig($data);
			echo $result; 
			
			
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	

	public function raaUpdateOnsiteConfigAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$pageid = $params ['pageid'];
			$enable = $params ['enable'];
			$helper = Mage::helper('retail_analytics/onsiteconfig');
			$result = $helper->updateOnsiteconfig($pageid,$enable);
			echo $result;
	
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	public function raaRemoveOnsiteConfigAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			//$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);
			$helper = Mage::helper('retail_analytics/onsiteconfig');
			$result = $helper->removeOnsiteconfig($data);
			echo $result;		
				
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaRemoveAllOnsiteConfigAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			//$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);
			$helper = Mage::helper('retail_analytics/onsiteconfig');
			$result = $helper->addOnsiteconfig($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAddRecoOfferAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			//$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);
			$helper = Mage::helper('retail_analytics/recooffer');
			$result = $helper->addRecooffer($data);
			echo $result;
				
				
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaRemoveRecoOfferAction() {
		try{
			
			$params = $this->getRequest()->getParams ();
			$campaign = $params ['campaign'];			
			$helper = Mage::helper('retail_analytics/recooffer');
			$result = $helper->removeRecooffer($campaign);
			echo $result;		
		
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaGetPageViewAction() {
		try{
				
				$params = $this->getRequest()->getParams ();
				$lastdate = (isset($params ['lastdate']))?$params ['lastdate']:"0";
				$helper = Mage::helper('retail_analytics/pageviewed');
				$data = $helper->getPageViewed($lastdate);
				echo json_encode ( $data );
			}
			catch ( Exception $e ) {
				echo $e->getMessage();
			}
	}
	
	public function raaGetModelSizeAction() {
		try{
	
			$params = $this->getRequest()->getParams ();
			$model = $params ['model'];
			$helper = Mage::helper('retail_analytics/data2');
			$data = $helper->getModelSize($model);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaGetModelDataAction() {
		try{
	
			$params = $this->getRequest()->getParams ();
			$model = $params ['model'];		
			$limit = (isset($params ['limit']))?$params ['limit']:0;
			$key = (isset($params ['key']))?$params ['key']:"";
			$value = (isset($params ['value']))?$params ['value']:"";
			
			$helper = Mage::helper('retail_analytics/data2');
			$data = $helper->getModelData($model,$limit,$key,$value);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaGetBrowsingDataAction() {
		try{
	
			$params = $this->getRequest()->getParams ();
			$lastdate = (isset($params ['lastdate']))?$params ['lastdate']:"";
			$limit = (isset($params ['limit']))?$params ['limit']:0;	
			$email = (isset($params ['email']))?$params ['email']:"";
			$ip = (isset($params ['ip']))?$params ['ip']:"";
			$internalip = (isset($params ['internalip']))?$params ['internalip']:"";
			
			$helper = Mage::helper('retail_analytics/recentlyviewed');
			$data = $helper->getBrowsingData($lastdate,$limit,$email,$ip,$internalip);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaGetBrowsingDataByDateAction() {
		try{
	
			$params = $this->getRequest()->getParams ();
			$lastdate = $params ['lastdate'];
			$lastid = $params ['lastid'];
			$limit = $params ['limit'];				
			$ipenable = (isset($params ['ipenable']))?$params ['ipenable']:false;
			
			$helper = Mage::helper('retail_analytics/recentlyviewed');
			$data = $helper->getBrowsingByDate($lastdate,$lastid,$limit,$ipenable);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	public function raaRemoveBrowsingDataAction() {
		try{
	
			$params = $this->getRequest()->getParams ();			
			$lastdate = (isset($params ['lastdate']))?$params ['lastdate']:"";
			$limit = (isset($params ['limit']))?$params ['limit']:0;	
			$email = (isset($params ['email']))?$params ['email']:"";
			$ip = (isset($params ['ip']))?$params ['ip']:"";
			$internalip = (isset($params ['internalip']))?$params ['internalip']:"";
			
			$helper = Mage::helper('retail_analytics/recentlyviewed');
			$result = $helper->removeBrowsingData($lastdate,$limit,$email,$ip,$internalip);
			echo  $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaRemoveModelDataAction() {
		try{
	
			$params = $this->getRequest()->getParams ();
			$model = $params ['model'];
			$limit = (isset($params ['limit']))?$params ['limit']:0;
			$key = (isset($params ['key']))?$params ['key']:"";
			$value = (isset($params ['value']))?$params ['value']:"";
			
			$helper = Mage::helper('retail_analytics/data2');
			$result = $helper->removeModelData($model,$limit,$key,$value);
			echo  $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}	
	
}
