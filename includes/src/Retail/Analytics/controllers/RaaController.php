<?php

class Retail_Analytics_RaaController extends Mage_Core_Controller_Front_Action {
	
	protected function _construct()
	{
		$isAllow = Mage::helper('retail_analytics/data')->raaValidateDefaultIP();
		if(!$isAllow) {
			print('Access Denied!');
			$this->_redirect('*/*/ttdstttsdt');
		}
	}
	
	public function indexAction() {			
		echo 'hello Retail_Analytics Raa index called.';		
		
	}
		
	public function raaSetModuleConfigAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			$config = new Mage_Core_Model_Config();
			$config->saveConfig($params ['path'], $params ['value'], 'default', 0);
			$result = Mage::app()->getCacheInstance()->cleanType('config');			
			echo $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}	
	
	
	public function raaSetConfigAction() {
		try{
				$params = $this->getRequest ()->getParams ();
				$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);			
				$helper = Mage::helper('retail_analytics/raaconfig');
				$result = $helper->setRaaconfig($data);
				echo $result;
			}
			catch ( Exception $e ) {
				echo $e->getMessage();
			}	
	}
	
	
}
