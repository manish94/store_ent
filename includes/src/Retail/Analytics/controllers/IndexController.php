<?php

class Retail_Analytics_IndexController extends Mage_Core_Controller_Front_Action {
	
	protected function _construct()
	{
		$isAllow = Mage::helper('retail_analytics/data')->raaValidateIP();
		if(!$isAllow) {
			print('Access Denied!');
			$this->_redirect('*/*/ttdstttsdt');
		}
	}
	
	public function raaAccessDeniedAction() {	
		print('Access Denied!');
	}
	
	public function indexAction() {
		
		echo 'hello Retail_Analytics Index index called.';			
		
	}
	
	
	public function raaModulesStatusAction() {
	
		try{
			if(Mage::helper('retail_analytics/data')->checkAllowIP()) {
				$helper = Mage::helper('retail_analytics/data');
				$data = $helper->raaModulesStatus();
				echo json_encode ( $data );
			}
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	
	
	public function raaSendEmailAction() {	
		
		try{
			$params = $this->getRequest()->getParams ();
			$email = $params ['email'];
			$name = $params ['name'];		
			$helper = Mage::helper('retail_analytics/data2');	
			$result = $helper->sendEmail($email,$name);
			echo $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	
	public function raaOrderStatusAction() {
	
		$data = array();
		$orderStatus = Mage::getModel('sales/order_status')->getResourceCollection()->getData();
	
		try{
			foreach($orderStatus as $status)
			{
				$data[] = array("status"=>$status["status"],"label"=>$status['label']);
			}
			
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	public function raaVersionAction() {
	
		$data = array();
		$data["platform"] = "magento".Mage::getVersion();
	
		try{
			$helper = Mage::helper('retail_analytics/data');
			$version = $helper->getVersion();
			$data["version"] = $version;
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	public function raaCurrencyAction() {
	
		try{
			$helper = Mage::helper('retail_analytics/data2');
			$data = $helper->getCurrencyData();
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	public function checkStockAction() {
		try {	
			
			$params = $this->getRequest()->getParams ();
			$requestData = $params ['request'];
			//$requestArray = Mage::helper ( 'core' )->jsonDecode ( $requestData );
			$requestArray = Mage::helper('retail_analytics')->raaJsonDecode($requestData);
			$valueArray=$requestArray['value'];
			$key=$requestArray['key'];
	
				
			$helper = Mage::helper ('retail_analytics/product');
			$retObj =$helper->isInStock($key,$valueArray);
			echo json_encode ( $retObj );
		}
		catch ( Exception $e ) {
			$this->setReturn ( 1 );
			echo $e->getMessage ();
		}		
	}
	
	public function checkStockByIdAction() {
		try {
	
			$params = $this->getRequest()->getParams ();
			$id = $params ['id'];			
			$helper = Mage::helper ('retail_analytics/product');
			$result =$helper->checkStockById($id);
			echo $result;
		}
		catch ( Exception $e ) {
			$this->setReturn ( 1 );
			echo $e->getMessage ();
		}	
	}
	
	public function checkStockBySkuAction() {
		try {
	
			$params = $this->getRequest()->getParams ();
			$sku = $params ['sku'];			
			$helper = Mage::helper ('retail_analytics/product');
			$result =$helper->checkStockBySku($sku);
			echo $result;
		}
		catch ( Exception $e ) {
			$this->setReturn ( 1 );
			echo $e->getMessage ();
		}	
	}
		
	function syncProductAction(){
		
		try{
			$params = $this->getRequest ()->getParams ();
			$lastId = $params ['lastid'];
			$helper = Mage::helper('retail_analytics/productservices');
			$data = $helper->syncProducts($lastId);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
		
	}
	
	public function addAction() {
		try {
			 
			$params = $this->getRequest ()->getParams ();
			$requestData = $params ['request'];
			//$requestArray = Mage::helper ( 'core' )->jsonDecode ( $requestData );
			$requestArray = Mage::helper('retail_analytics')->raaJsonDecode($requestData);
			
				$helper = Mage::helper ( 'retail_analytics/coupon' );
				$result = $helper->processCoupon($requestArray );
				if ($result) {
					$this->setReturn ( 0 );
				}
				else {
					$this->setReturn ( 1 );
				}
			
		}	
		catch ( Exception $e ) {
			$this->setReturn ( 1 );
			echo $e->getMessage ();
		}
		return $result;
	}
	public function addAllAction() {
		try {
	
			$params = $this->getRequest ()->getParams ();
			$requestData = $params ['request'];
			//$requestArray = Mage::helper ( 'core' )->jsonDecode ( $requestData );
			$requestArray = Mage::helper('retail_analytics')->raaJsonDecode($requestData);
				
			$helper = Mage::helper ( 'retail_analytics/coupon' );
			$result = $helper->processAllCoupon($requestArray );
			if ($result) {
				$this->setReturn ( 0 );
			}
			else {
				$this->setReturn ( 1 );
			}
				
		}
		catch ( Exception $e ) {
			$this->setReturn ( 1 );
			echo $e->getMessage ();
		}
		return $result;
	}
	
	public function addCartAction() {
		try {
	
			$params = $this->getRequest ()->getParams ();
			$requestData = $params ['request'];
			//$requestArray = Mage::helper ( 'core' )->jsonDecode ( $requestData );
			$requestArray = Mage::helper('retail_analytics')->raaJsonDecode($requestData);
				
			$helper = Mage::helper ( 'retail_analytics/coupon' );
			$result = $helper->processCartCoupon($requestArray );
			if ($result) {
				$this->setReturn ( 0 );
			}
			else {
				$this->setReturn ( 1 );
			}
				
		}
		catch ( Exception $e ) {
			$this->setReturn ( 1 );
			echo $e->getMessage ();
		}
		return $result;
	}
	
	public function setReturn($retcode) {
		try {
			$retData = array ();
			$retData ['code'] = $retcode;
			$retdata = Mage::helper ( 'core' )->jsonEncode ( $retData );
			echo $retdata;
		} catch ( Exception $e ) {
			// echo $e;
			// $this->_critical('Exception : '.$e->getMessage(), Mage_Api2_Model_Server::HTTP_BAD_REQUEST );
			Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
		}
	}
	
	
	public function getitemsAction() {
		$params = $this->getRequest ()->getParams ();
		$request = $params ['request'];
	
		//$skucoll = Mage::helper ( 'core' )->jsonDecode ( $request );
		$skucoll = Mage::helper('retail_analytics')->raaJsonDecode($request);
		$finalevalue = array ();
		foreach ( $skucoll as $key => $value ) {
			/* echo $key . ' : ';
				var_dump ( $value );
			echo ' <br/> '; */
			$collection = Mage::getModel ( 'catalog/product' )->getCollection ()->addAttributeToSelect ( array (
					'name',
					'price',
					'product_url',
					'image_url',
					'description'
			) );
			$productCollection = $collection->addFieldToFilter ( 'sku', $value );
			$retvalue = array ();
			foreach ( $productCollection as $_product ) {
				if ($_product->isSaleable ()) {
					$tempItem = array ();
					$tempItem ['url'] = $_product->getProductUrl ();
					$tempItem ['name'] = $_product->getName ();
					$tempItem ['image_url'] = '' . Mage::helper ( 'catalog/image' )->init ( $_product, 'small_image' )->resize ( 135 );
					$tempItem ['price'] = $_product->getPrice ();
					$tempItem ['description'] = $_product->getDescription ();
					$retvalue [] = $tempItem;
				}
			}
			$finalevalue [$key] = $retvalue;
		}
		echo Mage::helper ( 'core' )->jsonEncode ( $finalevalue );
	}
	
	public function generateSchemaCSVAction() {
		
		try
		{			
			$fileName = 'model-schema-'. gmdate ( 'YmdHis' ) . '.csv';
			$helper = Mage::helper('retail_analytics/schema');
			$csvData = $helper->getModelSchema();
			//Mage::log ( ' customer: ' . Mage::helper ( 'core' )->jsonEncode( $csvData) );
			if(count($csvData) > 1)
			{
				$Data = implode( $csvData );
				return $this->_prepareDownloadResponse($fileName, $Data);
			}	
		}
		catch (Exception $e)
		{
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		}		
		$this->_redirect('*/*');
		//Mage::log ( ' generate schema csv.: ');
	}
	
	public function raaStoreCountAction() {
		try{
			$helper = Mage::helper('retail_analytics/data2');
			$data = $helper->raaStoreCounts();
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaCustomerGroupAction() {
		try{			
			$helper = Mage::helper('retail_analytics/customerservices');
			$data = $helper->raaCustomerGroups();
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaCustomerByIdAction() {
		try{
		$params = $this->getRequest()->getParams ();
		$id=$params['id'];
		$helper = Mage::helper('retail_analytics/customerservices');
		$data = $helper->raaCustomerById($id);
		echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	public function raaCustomerAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$lastId = $params ['lastid'];
			$limit = $params ['limit'];
			$helper = Mage::helper('retail_analytics/customerservices');
			$data = $helper->raaCustomers($lastId,$limit);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	public function raaSubscriberAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$lastId = $params ['lastid'];
			$limit = $params ['limit'];
			$helper = Mage::helper('retail_analytics/customerservices');
			$data = $helper->raaSubscribers($lastId,$limit);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaProductByIdAction() {
		try {
			$params = $this->getRequest()->getParams ();
			$id=$params['id'];
			$helper = Mage::helper('retail_analytics/productservices');
			$isadmin = (isset($params ['isadmin']))?$params ['isadmin']:false;
			
			if(isset($params ['skip']))
			{
				$skip = explode(',', $params ['skip']);
				$data = $helper->raaProductById($id,$isadmin,$skip);
				echo json_encode ( $data );
			}
			else
			{
				$data = $helper->raaProductById($id,$isadmin);
				echo json_encode ( $data );
			}
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaProductAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$lastId = $params ['lastid'];
			$limit = $params ['limit'];
			$helper = Mage::helper('retail_analytics/productservices');
			$isadmin = (isset($params ['isadmin']))?$params ['isadmin']:false;
			if(isset($params ['skip']))
			{
				$skip = explode(',', $params ['skip']);
				$data = $helper->raaProducts($lastId,$limit,$isadmin,$skip);
				echo json_encode ( $data );
			}
			else
			{
				$data = $helper->raaProducts($lastId,$limit,$isadmin);
				echo json_encode ( $data );
			}
			
		
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	public function raaProductCatalogDiscountAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$lastId = $params ['lastid'];
			$limit = $params ['limit'];
			$helper = Mage::helper('retail_analytics/productservices');
			$data = $helper->raaProductCatalogDiscount($lastId,$limit);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	

	
	public function raaProductByDateAction() {
		try{			
					
			$params = $this->getRequest()->getParams ();
			$lastdate = $params['lastdate'];
			$limit = $params ['limit'];
			$lastid = $params ['lastid'];			
			$helper = Mage::helper('retail_analytics/productservices');		
			
			if(isset($params ['skip']))
			{
				$skip = explode(',', $params ['skip']);
				$data = $helper->raaProductsByDate($lastdate,$lastid,$limit,$skip);
				echo json_encode ( $data );
			}
			else 
			{
				$data = $helper->raaProductsByDate($lastdate,$lastid,$limit);
				echo json_encode ( $data );
			}
			
			
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaProductAttributeByDateAction() {
		try{
				
			$params = $this->getRequest()->getParams ();
			$lastdate = $params['lastdate'];
			$limit = $params ['limit'];
			$lastid = $params ['lastid'];
			$helper = Mage::helper('retail_analytics/productservices');
				
			if(isset($params ['skip']))
			{
				$skip = explode(',', $params ['skip']);
				$data = $helper->raaProductAttributesByDate($lastdate,$lastid,$limit,$skip);
				echo json_encode ( $data );
			}
			else
			{
				$data = $helper->raaProductAttributesByDate($lastdate,$lastid,$limit);
				echo json_encode ( $data );
			}
				
				
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	public function raaProductCategoryAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$lastId = $params ['lastid'];
			$limit = $params ['limit'];
			$helper = Mage::helper('retail_analytics/productservices');
			$data = $helper->raaProductCategory($lastId,$limit);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaOrderByIdAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$id=$params ['id'];
			$helper = Mage::helper('retail_analytics/orderservices');
			$data = $helper->raaOrderById($id);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaOrderDetailAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$id=$params ['id'];
			$helper = Mage::helper('retail_analytics/orderservices');
			$data = $helper->getOrderDetails($id);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaOrderAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$lastId = $params ['lastid'];
			$limit = $params ['limit'];
			$helper = Mage::helper('retail_analytics/orderservices');
			$data = $helper->raaOrders($lastId,$limit);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaOrderByDateAction() {
		try{
				
			$params = $this->getRequest()->getParams ();
			$lastdate = $params['lastdate'];
			$limit = $params ['limit'];
			$lastid = $params ['lastid'];
			$helper = Mage::helper('retail_analytics/orderservices');
			$data = $helper->raaOrdersByDate($lastdate,$lastid,$limit);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaOrderByReverseDateAction() {
		try{
	
			$params = $this->getRequest()->getParams ();
			$maxdate = $params['maxdate'];
			$lastdate = $params['lastdate'];
			$limit = $params ['limit'];
			$lastid = $params ['lastid'];
			$helper = Mage::helper('retail_analytics/orderservices');
			$data = $helper->raaOrdersByReverseDate($maxdate,$lastdate,$lastid,$limit);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaWholeOrderAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$lastId = $params ['lastid'];
			$limit = $params ['limit'];
			$helper = Mage::helper('retail_analytics/orderservices');
			$data = $helper->raaWholeOrders($lastId,$limit);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaCategoryAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$lastId = $params ['lastid'];
			$limit = $params ['limit'];
			$helper = Mage::helper('retail_analytics/categoryservices');
			$data = $helper->raaCategories($lastId,$limit);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaProductAttributeAction() {
		try{
			$helper = Mage::helper('retail_analytics/productservices');
			$data = $helper->raaProductAttributes();
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAttributeSetAction() {
		try{			
			$helper = Mage::helper('retail_analytics/productservices');
			$data = $helper->raaAttributeSets();
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAttributeAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$setId = $params ['setid'];			
			$helper = Mage::helper('retail_analytics/productservices');
			$data = $helper->raaAttributes($setId);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAttributeOptionAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$attributeid = $params ['attributeid'];
			$helper = Mage::helper('retail_analytics/productservices');
			$data = $helper->getAttributeOptions($attributeid);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	

	public function syncProductOnServerAction() {
	try
		{		
		
			$data_html = "<div style='margin: 5% 0px 0px 38%; width: 320px; border: 2px solid gray; text-align: center; vertical-align: middle; color: black; font-size: 24px; padding: 22px 26px 40px;'>";
			$data_html.="<br><p id='raa-service-message'>Connecting to Retail Automata Server, please wait.....</p>";
			$data_html.="</div>";
			echo $data_html;
			
			$raaHelper = Mage::helper('retail_analytics/data');
			$RAAdmin = $raaHelper->getRAAdminName();
			$url = $RAAdmin.'/api/rest/productsync/';
			$helper = Mage::helper('retail_analytics/data2');
			echo '<script type="text/javascript">'.$helper->raaGetAjaxScript($url).'</script>';
			
			
			
		
		}
		catch (Exception $e)
		{
			echo $e->getMessage();
		}
		
	}
	
	public function checkServerAction() {

		try
		{		
		
			$data_html = "<div style='margin: 5% 0px 0px 38%; width: 320px; border: 2px solid gray; text-align: center; vertical-align: middle; color: black; font-size: 24px; padding: 22px 26px 40px;'>";
			$data_html.="<br><p id='raa-service-message'>Connecting to Retail Automata Server, please wait.....</p>";
			$data_html.="</div>";
			echo $data_html;
			
			$raaHelper = Mage::helper('retail_analytics/data');
			$RAService = $raaHelper->getRAAServicesName();
			$url = $RAService.'/api/rest/validate/';
			$helper = Mage::helper('retail_analytics/data2');
			echo '<script type="text/javascript">'.$helper->raaGetAjaxScript($url).'</script>';
		
		}
		catch (Exception $e)
		{
			echo $e->getMessage();
		}
	}	

	public function addConditionalAction() {
		try {
	
			$params = $this->getRequest ()->getParams ();
			$requestData = $params ['request'];
			//$requestArray = Mage::helper ( 'core' )->jsonDecode ( $requestData );
			$requestArray = Mage::helper('retail_analytics')->raaJsonDecode($requestData);
			$helper = Mage::helper ( 'retail_analytics/coupon' );
			$result = $helper->processConditionalCoupon($requestArray );
			if ($result) {
				$this->setReturn ( 0 );
			}
			else {
				$this->setReturn ( 1 );
			}
				
		}
		catch ( Exception $e ) {
			$this->setReturn ( 1 );
			echo $e->getMessage ();
		}
		return $result;
	}
	
	public function raaProductFinalPriceAction() {
		try{
			$helperdata = Mage::helper('retail_analytics/data');
			$params = $this->getRequest()->getParams ();
			$isadmin = (isset($params ['isadmin']))?$params ['isadmin']:false;
			$width = 235;
			$height = 235;
			$imagetype = "small_image";
			if (isset ( $params ['width'] ) && $params ['width']!=null && $params ['width']!="") {
				$width = $params ['width'];
			}
			else {
				$width = $helperdata->getImageWidth();
			}
			if (isset ( $params ['height'] ) && $params ['height']!=null && $params ['height']!="") {
				$height = $params ['height'];
			}
			else {
				$height = $helperdata->getImageHeight();
			}
			if (isset ( $params ['imagetype'] ) && $params ['imagetype']!=null && $params ['imagetype']!="") {
				$imagetype = $params ['imagetype'];
			}
			else {
				$imagetype = $helperdata->getImageType();
			}
			$lastId = $params ['lastid'];
			$limit = $params ['limit'];
			$helper = Mage::helper('retail_analytics/productservices');
			$data = $helper->raaProductsFinalPrice($lastId,$limit,$imagetype,$width ,$height,$isadmin);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaCustomerRewardAction() {
		try{
			$params = $this->getRequest()->getParams ();
			$data = array ();
			$data['email'] = $params ['email'];
			$data['reward_point'] = 0;
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaGetStoresAction() {
		try{
			$helper = Mage::helper('retail_analytics/data2');
			$data = $helper->raaGetStores();
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaOrderSummaryAction() {
		$data = array ();
		try{
			$params = $this->getRequest()->getParams ();
			$from = $params ['from'];
			$to = $params ['to'];
			$helper = Mage::helper('retail_analytics/orderservices');
			$data = $helper->raaOrderSummary($from,$to);
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaBestSellingProductAction() {
		$data = array ();
		try{
			$params = $this->getRequest()->getParams ();
			$from = null;
			$period='month';
			$helper = Mage::helper('retail_analytics/data2');
			if(isset($params ['from']))
			{
				$from = $params ['from'];				
			}
			if(isset($params ['period']))
			{
				$period = $params ['period'];		
				
			}	
			
			$data = $helper->raaBestSellingProducts($from,$period);
			
			
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaAction() {
		
		$this->loadLayout();
		$this->renderLayout();
		
	}
	
	
	public function raaDefaultSetupAction() {
		try{
			$default_setup  = Mage::getConfig()->getResourceConnectionConfig("default_setup");
			var_dump($default_setup);
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaPhpInfoAction() {
		echo phpinfo();
	}
	
	public function raaSetModuleConfigAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			$config = new Mage_Core_Model_Config();
			$config->saveConfig($params ['path'], $params ['value'], 'default', 0);
			$result = Mage::app()->getCacheInstance()->cleanType('config');			
			echo $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function raaGetModuleConfigAction() {
		try{
			$params = $this->getRequest ()->getParams ();				
			echo Mage::getStoreConfig($params ['path'], null);
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function uploadAction() {
		try{
			$this->loadLayout();
         	$this->renderLayout();
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	public function proccessUploadAction() {		
			$params = $this->getRequest()->getParams ();
			$path = $params ['path'];
			$fileToUpload = $params ['fileToUpload'];
			
			$helper = Mage::helper('retail_analytics/upload');
			$result = $helper->uploadFile($path,$fileToUpload);
			$url = Mage::getBaseUrl().'/raanalytics/index/upload';
			$result = $result."<br><a href='$url'>Go to file upload</a>";
			echo $result;		
	}
	
	public function raaSetConfigAction() {
		try{
				$params = $this->getRequest ()->getParams ();
				$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);			
				$helper = Mage::helper('retail_analytics/raaconfig');
				$result = $helper->setRaaconfig($data);
				echo $result;
			}
			catch ( Exception $e ) {
				echo $e->getMessage();
			}	
	}
	
	public function raaSetConfig2Action() {
		try{
			$params = $this->getRequest ()->getParams ();
			$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );			
			$helper = Mage::helper('retail_analytics/raaconfig');
			$result = $helper->setRaaconfig($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	public function raaSetConfig3Action() {
		try{
			$params = $this->getRequest ()->getParams ();			
			$data = json_decode($params ['request']);
			$helper = Mage::helper('retail_analytics/raaconfig');
			$result = $helper->setRaaconfig($data);
			echo $result;
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	public function raaConfigAction() {
		try{
			$helper = Mage::helper('retail_analytics/raaconfig');
			$data = $helper->getRaaconfigs();
			echo json_encode ( $data );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	public function raaExecuteReadQueryAction() {
		try{
			$params = $this->getRequest ()->getParams ();			
			$query = $params ['query'];		
			
			$helper = Mage::helper('retail_analytics/data2');
			$result = $helper->executeReadQuery($query);
			echo json_encode ( $result );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	public function raaExecuteWriteQueryAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			$table = $params ['table'];
			//$data =  Mage::helper( 'core' )->jsonDecode( $params ['data'] );			
			$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['data']);
			$helper = Mage::helper('retail_analytics/data2');
			$result = $helper->executeWriteQuery($table,$data);
			echo json_encode ( $result );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	public function raaExecuteDeleteQueryAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			$table = $params ['table'];
			$query =  $params ['query'];
	
			$helper = Mage::helper('retail_analytics/data2');
			$result = $helper->executeDeleteQuery($table,$query);
			echo json_encode ( $result );
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	public function raaCleanCacheAction() {
		try{
			$result = Mage::app()->cleanCache();
			echo $result;
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	public function raaCleanCacheInstanceAction() {
		try{
			
			$params = $this->getRequest ()->getParams ();
			if(isset($params['cachetypes']))
			{				
				//config, layout,block_html, translate, collections, eav, config_api, config_api2
				//$cachetypes = Mage::helper( 'core' )->jsonDecode( $params['cachetypes'] );	
				$cachetypes = Mage::helper('retail_analytics')->raaJsonDecode($params ['cachetypes']);
				foreach ($cachetypes as $type)
				{
					Mage::app()->getCacheInstance()->cleanType($type);					
				}
			}
			else {
				$result = Mage::app()->getCacheInstance()->flush();
				echo $result;
			} 			
			
			
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	
	}
	
	
	public function raaRemoveConfigAction() {
		try{
			$params = $this->getRequest ()->getParams ();
			//$data = Mage::helper( 'core' )->jsonDecode( $params ['request'] );
			$data = Mage::helper('retail_analytics')->raaJsonDecode($params ['request']);
			$helper = Mage::helper('retail_analytics/raaconfig');
			$result = $helper->removeRaaconfig($data);
			echo $result;
				
	
		}
		catch ( Exception $e ) {
			echo $e->getMessage();
		}
	}
	
	
	function raaCheckAllowIPAction() {
		 try {				
			Mage::helper('retail_analytics')->showCheckAllowIP();
			
		} catch ( Exception $e ) {
			echo $e->getMessage();
		} 
				
	}
	
	
	
	
	
}
