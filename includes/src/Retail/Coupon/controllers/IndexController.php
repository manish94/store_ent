<?php

class Retail_Coupon_IndexController extends Mage_Core_Controller_Front_Action
{	
	
	protected function _construct()
	{
		$isAllow = Mage::helper('retail_analytics/data')->raaValidateIP();
		if(!$isAllow) {
			print('Access Denied!');
			$this->_redirect('*/*/ttdstttsdt');
		}
	}
	
    public function indexAction()
    {
    	echo "coupon index ";
    	
    	$ruleIds = array();
    	$rules = Mage::getResourceModel('salesrule/rule_collection')->addFieldToFilter('is_active',true)
    	  ->addFieldToFilter('from_date', "12/01/2014")->load();
    	
    	foreach ($rules as $rule)
    	{
    	    echo " code : ". $rule->getCode();
    	//	$customer_ids = $rule->getCustomerGroupIds();
    	  
    //		$customer_ids[]="4";
    //		var_dump($customer_ids);
    //	    $rule->setCustomerGroupIds ($customer_ids);
    //	    $rule->save();
    		foreach ($customer_ids as $groupId) 
    		{
    			echo "group Id : ".$groupId;
    		}
    		 
    	}
    	
    	return $ruleIds;
    	
    	/*header('Access-Control-Allow-Origin: *');
    	header('Content-type:application/json');
    	$jsonData = array();
    
    	if (Mage::getSingleton('customer/session')->isLoggedIn()) {    	
    		
    		$customerArray = array();
    		$customer = Mage::getSingleton('customer/session')->getCustomer();
    		$customerArray['fullname'] = $customer->getName();
    		$customerArray['firstname'] = $customer->getFirstname();
    		$customerArray['lastname'] = $customer->getLastname();
    		$customerArray['email'] = $customer->getEmail();      		
    		$this->getResponse()->setBody(json_encode($customerArray));
    		
    		$products = array();
    		$quote = Mage::getSingleton('checkout/session')->getQuote();
    		$cartItems = $quote->getAllVisibleItems();
    		foreach ($cartItems as $item)
    		{
    			$productArray = array();
    			$productId = $item->getProductId();
    			$product = Mage::getModel('catalog/product')->load($productId);
    			$productArray['id'] = $productId;
    			$productArray['price'] = $product->getPrice();  
    			$productArray['sku'] = $product->getSku();    	
    			$productArray['url'] = $product->getProductUrl();
    			$productArray['image'] = Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getImage());    			
    			
    			$products[$productId] = $productArray;    		
    		}
    		
    		$jsonData['customers'] = $customerArray;
    		$jsonData['products'] = $products;
    		$jsonData['message'] = '1 customers is logged in';
    		
    	}
    	else {
    		$jsonData['message'] = '0 customers logged in';    		
    	}
    	
    	$this->getResponse()->setBody(json_encode($jsonData));*/
    	
    
    } 	
	
    public function addAction() {
    	try {
    			
    		$params = $this->getRequest ()->getParams ();
    		$requestData = $params ['request'];    	
    		//$requestArray = Mage::helper ( 'core' )->jsonDecode ( $requestData );
    		$requestArray = Mage::helper('retail_analytics')->raaJsonDecode($requestData);
    		//$helper = Mage::helper ( 'retail_auth/auth' );
    		$retvalue = true;
    		//$callbackUrl = Mage::getBaseUrl ();
    		//$retvalue = $helper->getAuth ( $requestArray ['key'], $requestArray ['secret'], $callbackUrl );
    			
    		if ($retvalue == true) {
    			$raa_data = $requestArray ['data'];
    			$helper = Mage::helper ( 'retail_coupon/coupon' );    			
    			$result = $helper->processCoupon ( $raa_data );       						
    			if ($result) {
    				$this->setReturn ( 0 );
    			}
    			else {
    				$this->setReturn ( 3 );
    			}
    		} else {
    			$this->setReturn ( 2 );
    		}
    	}
    
    	catch ( Exception $e ) {
    		$this->setReturn ( 1 );
    		echo $e->getMessage ();
    	}
    	return $result;
    }
    
    
    public function viewAction()
    {
    	$params = $this->getRequest()->getParams();
    	echo 'id : '.$params['id'];
    	
    	$salesrule = Mage::getModel("salesrule/rule");    
    	$salesrule->load($params['id']);
    	var_dump($salesrule);
    }
    
   
	
   public function bulkinsertAction()
    {
		$params = $this->getRequest ()->getParams ();
		$couponData = $params ['request'];
		var_dump ( $couponData );
		//$couponArrayBulk = Mage::helper ( 'core' )->jsonDecode ( $couponData );
		$couponArrayBulk = Mage::helper('retail_analytics')->raaJsonDecode($couponData);
		$result = false;
		$helper = Mage::helper ( 'retail_coupon/coupon' );
		
		foreach ( $couponArrayBulk as $couponArray ) {
			
			switch ($couponArray ['coupontype']) {
				
				case 'Map' :
					$result = $helper->processRecoCoupon( $couponArray );
					break;
				
				case 'Receipt' :
					if ($couponArray ['customer'] == 'ALL')
						$result = $helper->processALLReceiptCoupon( $couponArray );
					else
						$result = $helper->processReceiptCoupon( $couponArray );
					break;
				
				case 'Item' :
					if ($couponArray ['customer'] == 'ALL')
						$result = $helper->processItemCoupon( $couponArray );
					else
						$result = $helper->processCustItemCoupon( $couponArray );
					break;
			}
		}
		return $result;
	}
	
	/**
	 * Delete Coupon action
	 */
	public function deleteAction()
	{
		$params = $this->getRequest()->getParams();
		$couponcode = $params['couponcode'];
		//var_dump($couponcode);
	    
		$shoppingCartPriceRules = Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('code',$couponcode) ;//loadByAttribute('coupon_code',$couponcode);
		
		foreach ($shoppingCartPriceRules as $rule) {
			try {
				$id=$rule->getId();
				$delRule=Mage::getModel ( 'salesrule/rule')->load($id);
			    $delRule->delete();
			} catch (Exception $e) {
				//$this->_getSession()->addError($e->getMessage());
				echo 'false';
			}
			//echo 'true';
		}
		echo 'true';
	}
	
	
	public function addGroupAction()
	{
		$params = $this->getRequest ()->getParams ();
		$couponData = $params ['request'];
		//$couponArray = Mage::helper ( 'core' )->jsonDecode ( $couponData );
		$couponArray = Mage::helper('retail_analytics')->raaJsonDecode($couponData);
		$helper = Mage::helper ( 'retail_coupon/coupon' );
		$flag=$helper->processCoupon( $couponArray );
	    if($flag)
	    {
	    	echo "coupon with code". $couponArray['couponcode']."created succesfully";
	    }
	
		else
		 {
		 echo "false";	
		}
		 
		 
	}
	public function setReturn($retcode) {
		try {
			$retData = array ();
			$retData ['code'] = $retcode;
			$retdata = Mage::helper ( 'core' )->jsonEncode ( $retData );
			echo $retdata;
		} catch ( Exception $e ) {
			// echo $e;
			// $this->_critical('Exception : '.$e->getMessage(), Mage_Api2_Model_Server::HTTP_BAD_REQUEST );
			Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
		}
	}
	
}