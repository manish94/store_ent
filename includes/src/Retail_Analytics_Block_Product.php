<?php
/**
 * Magento 
 */
class Retail_Analytics_Block_Product extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Product "in stock" tagging string.
     */
    const PRODUCT_IN_STOCK = 'InStock';

    /**
     * Product "out of stock" tagging string.
     */
    const PRODUCT_OUT_OF_STOCK = 'OutOfStock';
    
    /**
     * Render product info as hidden meta data if the module is enabled for the current store.
     * If it is a "bundle" product with fixed price type, then do not render. These are not supported due to their
     * child products not having prices available.
     *
     * @return string
     */
    protected function _toHtml()
    {
    	$raaHelper = Mage::helper('retail_analytics');
        $product = $this->getProduct();
        if (!$raaHelper->isModuleEnabled()
            || ($product->getTypeId() === Mage_Catalog_Model_Product_Type::TYPE_BUNDLE
                && (int)$product->getPriceType() === Mage_Bundle_Model_Product_Price::PRICE_TYPE_FIXED)
        ) {
            return '';
        }      
        
        $raaHelper->setRas();        
    	$isretargeting = $raaHelper->getRaaConfig('isretargeting');
        if ($isretargeting == "false") {
        	return '';
        } 
        
        return parent::_toHtml();
    }

    /**
     * Return tagging data for the product availability.
     *
     * @param Mage_Catalog_Model_Product $product
     *
     * @return string
     */
    public function getProductAvailability($product)
    {
        if ($product instanceof Mage_Catalog_Model_Product && $product->isAvailable()) {
            return self::PRODUCT_IN_STOCK;
        } else {
            return self::PRODUCT_OUT_OF_STOCK;
        }
    }

    /**
     * Return array of categories for the product.
     * The items in the array are strings combined of the complete category path to the products own category.
     *
     * Structure:
     * array (
     *     /Electronics/Computers
     * )
     *
     * @param Mage_Catalog_Model_Product $product
     *
     * @return array
     */
    public function getProductCategories($product)
    {
        $data = array();

        if ($product instanceof Mage_Catalog_Model_Product) {
            $categoryCollection = $product->getCategoryCollection();
            foreach ($categoryCollection as $category) {
                $categoryString = Mage::helper('retail_analytics')->buildCategoryString($category);
                if (!empty($categoryString)) {
                    $data[] = $categoryString;
                }
            }
        }

        return $data;
    }
    
    /**
     * Return array of reco server attributes for the product.
     * The items in the array are strings combined of the complete category path to the products own category.
     *
     * Structure:
     * array (
     *     /Electronics/Computers
     * )   
     * @return array
     */
    public function getRecoServerAttributes($product)
    {
    	$data = array();    	    	
    	$raaHelper = Mage::helper('retail_analytics');
    	$attributes = explode(",", $raaHelper->getAttributes());    	
    	foreach ($attributes as $attribute){    		
    		if($product->offsetExists($attribute))
    		{
    			$attributeValue = $product->getAttributeText($attribute);
    			if($attributeValue != null && $attributeValue != "")
    			{
    				$data[] = array('value'=>$attributeValue, 'code'=>$attribute);
    			}
    		}
    	}
    	   	
    	return $data;
    }
	
	public function getRaaImageURL($product)
    {
    	//$product = Mage::getModel( 'catalog/product' )->load($product_id)->addAttributeToSelect ( "*" );
    	$product->getData();
    	$SmallImage =  $product->getSmallImage();
    	
		if($SmallImage == null || $SmallImage == "" || $SmallImage == 'no_selection' )
		{
			$Image = $product->getImage();
			$thumnailImage = $product->getThumbnail();
			if($Image!=null && $Image!="" && $Image != 'no_selection')
				return $product->getMediaConfig()->getMediaUrl($Image);
			else if($thumnailImage!=null && $thumnailImage!="" && $thumnailImage != 'no_selection')
				return $product->getMediaConfig()->getMediaUrl($thumnailImage);
			else
				return  $product->getImageUrl();
		}
		else
		{
			return $product->getMediaConfig()->getMediaUrl($SmallImage);
		}
    }
    
    public function getProductCacheImageURL($product)
    {
    	$helperdata = Mage::helper('retail_analytics/data');
    	$width = $helperdata->getImageWidth();
    	$height = $helperdata->getImageHeight();
    	$imagetype = "small_image";
    	return (string)Mage::helper('catalog/image')->init($product, $imagetype)->resize($width,$height);    	
    }
    
    
    public function getProductPrice($product)
    {
    	$raaPriceHelper = Mage::helper('retail_analytics/price');
		$taxHelper = Mage::helper('tax');
		$price = $raaPriceHelper->getFormattedPrice($taxHelper->getPrice($product, $product->getPrice($product), true));
		return $price;
    }
    
    public function getProductSpecialPrice($product)
    {
    	$raaPriceHelper = Mage::helper('retail_analytics/price');
    	$taxHelper = Mage::helper('tax');
    	$special_price = $raaPriceHelper->getFormattedPrice($taxHelper->getPrice($product, $raaPriceHelper->getProductFinalPrice($product), true));
    	return $special_price;
    }
    
    public function getProductDiscountPercent($product)
    {
    	$discountPercent = 0;
    	$price= $this->getPrice($product);
    	$special_price = $this->getSpecialPrice($product);
		if ($special_price != $price) {
			$discountPercent = (($price - $special_price) * 100)/$price;
		}		
		return $discountPercent;
    }
    
    public function getProductWebsiteIds($product)
    {
    	return Mage::helper ( 'core' )->jsonEncode ($product->getWebsiteIds());
    }
    
    public function getProductStoreIds($product)
    {
    	return Mage::helper ( 'core' )->jsonEncode ($product->getStoreIds());
    }
    
    public function getProductChildids($product)
    {
    	$child = array();
    	if ($product->getTypeID() == "configurable"){
    	$childids = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($product->getId());
			if(count($childids) > 0)
			{
				//$productArray ["product_child"] = array_keys($childids[0]);
				$productArray ["product_child"] = array();
				foreach ($childids[0] as $key => $value)
				{
					$child[] = $value;
				}
			}
    	}
    	
    	return Mage::helper ( 'core' )->jsonEncode ($child);
    }
    
    public function getProductCategoryIds($product)
    {
    	$dataArray = array();	
	    $categoryIds = $product->getCategoryIds();
		if (isset($categoryIds[0])){
			$_categoryids = array();			
			$parentCategory="";
			foreach ( $categoryIds as $_category ) {							
				$category = Mage::getModel('catalog/category')->load($_category);
				$childCategories = $category->getChildrenCount();
				$parentCategory = $_category;
				if($childCategories == "0")
				{								
					$_categoryids[] = $_category;								
				}
			}
			if(count($_categoryids)==0 && $parentCategory != "" ){
				$_categoryids[] = $parentCategory;
			}
			if(count($_categoryids) > 0)
			{
				$dataArray ["category_id"] = $_categoryids[0];
				$dataArray ["category_ids"] = Mage::helper ( 'core' )->jsonEncode( $_categoryids);				
				$dataArray ['root_category_id'] = Mage::helper('retail_analytics/data2')->getRootCatId($dataArray["category_id"]);
			}
		}
		
		return $dataArray;
    }
}
