<?php
/**
 * Magento 
 */
class Retail_Analytics_Block_Onsitecategory extends Mage_Core_Block_Template
{
    
    /**
     * Render category string as hidden meta data if the module is enabled for the current store.
     *
     * @return string
     */
    protected function _toHtml()
    {
        $raaHelper = Mage::helper('retail_analytics');
        if (!$raaHelper->isModuleEnabled()) {
            return '';
        }
        
    	$isonline = $raaHelper->getRaaConfig('isonline');
    	if ($isonline == "" || $isonline == "true") {
        	return '';
        }
        
        return parent::_toHtml();
    }

  
    
    /**
     * Return the id of the element. If none is defined in the layout xml, then set a default one.
     *
     * @return string
     */
    public function getElementId()
    {
    	return $this->getDivId();
    }
}
