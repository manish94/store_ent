<?php

class Retail_Analytics_Helper_Recentlyviewed extends Mage_Core_Helper_Abstract
{
		
	public function saveRecentlyViewed($productid) {
		
		$raaHelper = Mage::helper ( 'retail_analytics/data' );
		$ip = $raaHelper->getExternalIP();		
		
		$internalip = $raaHelper->getInternalIp();
		
				 
		$ras = "";	
		$params = Mage::app()->getRequest ()->getParams ();	
		if (isset($params['ras'])) 
		{
			$ras = $params['ras'];	
		} 
		else {			
			$ras = $raaHelper->getRas();			
		}
		
		$recentlyViewedModel = Mage::getModel ( 'retail_analytics/recentlyviewed' );
		$customerid = null;
		$email = null;
		
		if(Mage::getSingleton('customer/session')->isLoggedIn()) {
			$customerData = Mage::getSingleton('customer/session')->getCustomer();
			$customerid = $customerData->getId();
			$email = $customerData->getEmail();
		}		
	
		$isiptocustomer = $raaHelper->getRaaconfig('isiptocustomer');
		 if($isiptocustomer == "true" && $email != null) {
			
			$customerData = Mage::getSingleton('customer/session')->getCustomer();
			$customerid = $customerData->getId();
			$email = $customerData->getEmail();			
			
			$data_collection = $recentlyViewedModel->getCollection ()->addFieldToFilter ( 'ip', array ($ip) )->addFieldToFilter ( 'internalip', array ($internalip) )->addFieldToFilter( 'email', array('null' => true) );
			foreach ($data_collection as $data_row){
				$recentlyviewed = Mage::getModel('retail_analytics/recentlyviewed')->load($data_row->getData('id'));
				if($this->checkRecentlyViewed($recentlyviewed->getProductid(), $email, $ip,$internalip)) {					
					$recentlyviewed->delete();					
				} 
				else {
					$recentlyviewed->setEmail($email);
					$recentlyviewed->setCustomerid($customerid);
					$recentlyviewed->setIp($ip);
					$recentlyviewed->setInternalip($internalip);
					$recentlyviewed->setRas($ras);
					$recentlyviewed->setStatus('Active');
					$recentlyviewed->setCreated(now());
					$recentlyviewed->setModified(now());
					$currentcurrencycode = Mage::app()->getStore()->getCurrentCurrencyCode();
					$recentlyviewed->setStorecurrencycode($currentcurrencycode);
					$recentlyviewed->save();
				}
			}			
			
		} 		
		
		
		if(!$this->checkRecentlyViewed($productid, $email, $ip,$internalip)) {
			
			$recentlyviewed = Mage::getModel('retail_analytics/recentlyviewed');
			$recentlyviewed->setProductid($productid);			
			$recentlyviewed->setEmail($email);
			$recentlyviewed->setCustomerid($customerid);
			$recentlyviewed->setIp($ip);
			$recentlyviewed->setInternalip($internalip);
			$recentlyviewed->setRas($ras);
			$recentlyviewed->setStatus('Active');
			$recentlyviewed->setCreated(now());
			$recentlyviewed->setModified(now());
			$currentcurrencycode = Mage::app()->getStore()->getCurrentCurrencyCode();
			$recentlyviewed->setStorecurrencycode($currentcurrencycode);
			$recentlyviewed->save();
		}
		else
		{		
			
			$data_collection = Mage::getModel('retail_analytics/recentlyviewed')->getCollection ()->addFieldToFilter ( 'productid', array ($productid) )->addFieldToFilter ( 'email', array ($email) );
			if ($data_collection->count () > 0) {
				
    			$recentlyviewed = $data_collection->getFirstItem();
    			$recentlyviewed->setStatus('Active');
    			$recentlyviewed->setCreated(now());
    			$recentlyviewed->setModified(now());
    			$currentcurrencycode = Mage::app()->getStore()->getCurrentCurrencyCode();
    			$recentlyviewed->setStorecurrencycode($currentcurrencycode);
    			$recentlyviewed->save();
    			
			}				
		}	
		
		$brlimit = $raaHelper->getRaaconfig('brlimit');
		$brlimit = ($brlimit == "")?5:$brlimit;
		
		$data_collection = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ();
		if($email == null)
		{
			$data_collection = $data_collection->addFieldToFilter ( 'ip', array ($ip) )->addFieldToFilter ( 'internalip', array ($internalip) )->addFieldToFilter( 'email', array('null' => true) )
			->addAttributeToSort("modified","ASC");
		}
		else {
			$data_collection = $data_collection->addFieldToFilter ( 'email', array ($email) )
			->addAttributeToSort("modified","ASC");
		}				
			
		if($data_collection->count() > $brlimit)
		{
			$recentlyviewed = Mage::getModel('retail_analytics/recentlyviewed')->load($data_collection->getFirstItem()->getId());
			$recentlyviewed->delete();
		}
	}	
	
	
	public function checkRecentlyViewed($productid, $email, $ip,$internalip) {
		$data_collection = array();
		if($email == null)
		{	
			$data_collection = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ()->addFieldToFilter ( 'productid', array ($productid) )->addFieldToFilter ( 'ip', array ($ip) )->addFieldToFilter ( 'internalip', array ($internalip) )->addFieldToFilter( 'email', array('null' => true) );
		}
		else {
			$data_collection = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ()->addFieldToFilter ( 'productid', array ($productid) )->addFieldToFilter ( 'email', array ($email) );
		}
		if (count($data_collection) > 0) {
			return true;	
		} else {
			return false;
		}
		
	}
	
	public function removeBrowsingData($lastdate,$limit,$email,$ip,$internalip) {
	
		try {
	
			$data_collection = Mage::getModel (  'retail_analytics/recentlyviewed')->getCollection ();
	
			if($lastdate != "")
			{
				$data_collection->addFieldToFilter ( 'modified', array('lteq' => $lastdate) );
			}
			if($email != "")
			{
				$data_collection->addFieldToFilter ( 'email', array ($email) );
			}
			if($ip != "")
			{
				$data_collection->addFieldToFilter ( 'ip', array ($ip) );
			}
			if($internalip != "")
			{
				$data_collection->addFieldToFilter ( 'internalip', array ($internalip) );
			}
	
			
	
			if($limit > 0)
			{
				$data_collection->getSelect ()->limit ( $limit );
			}
	
	
			if ($data_collection->count () > 0) {
				foreach ( $data_collection as $modelRow ) {
					$id =  $modelRow->getData('id');
					$dataModel = Mage::getModel (  'retail_analytics/recentlyviewed')->load($id);
					$dataModel->delete();
				}
			}
			return true;
		} catch ( Exception $ex ) {
			return false;
		}
	}
	
	public function getBrowsingData($lastdate,$limit,$email,$ip,$internalip) {
		$data = array ();
		try {
			 
			$data_collection = Mage::getModel (  'retail_analytics/recentlyviewed' )->getCollection ();
	
			if($lastdate != "")
			{
				$data_collection->addFieldToFilter ( 'modified', array('gt' => $lastdate) );
			}
			if($email != "")
			{
				$data_collection->addFieldToFilter ( 'email', array ($email) );
			}
			if($ip != "")
			{
				$data_collection->addFieldToFilter ( 'ip', array ($ip) );
			}
			if($internalip != "")
			{
				$data_collection->addFieldToFilter ( 'internalip', array ($internalip) );
			}	
		
	
			if($limit > 0)
			{
				$data_collection->getSelect ()->limit ( $limit );
			}
	
	
	
			if ($data_collection->count () > 0) {
				foreach ( $data_collection as $modelRow ) {
					$dataArray = array();
					$dataArray = $modelRow->getData();
					$dataArray['store_currency_code'] = $dataArray['storecurrencycode'];
					unset($dataArray['storecurrencycode']);
					$data[] = $dataArray;
				}
	
			}
			return $data;
		} catch ( Exception $ex ) {
			return $data;
		}
	}
	
	
	
	
	public function getBrowsingByDate($lastdate,$lastid,$limit,$ipenable) {
		$retObj = array ();
		$data = array ();
		$new_lastId = 0;
		$RowCount = 0;	
		$ipenable = ($ipenable == true || $ipenable == "true")?true:false;
		try {
	
			$data_collection = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ();
			if(!$ipenable)
			{
				$data_collection->addFieldToFilter( 'email', array('neq' => 'null') );
			}
			if($lastid==0)
			{				
				$data_collection->addFieldToFilter('modified', array('gteq' => $lastdate))
				->addFieldToFilter('id', array('gt' => $lastid))				
				->addFieldToSelect ( "*" )
				->addAttributeToSort("modified","ASC")
				->addAttributeToSort("id","ASC");			
				$data_collection->getSelect ()->limit ( $limit );
				$RowCount = $data_collection->count ();
	
			}
			else
			{
	
				$data_collection_temp = Mage::getModel ( 'retail_analytics/recentlyviewed' )->getCollection ();
				if(!$ipenable)
				{
					$data_collection_temp->addFieldToFilter( 'email', array('neq' => 'null') );
				}
				$data_collection_temp->addFieldToFilter('modified', array('eq' => $lastdate))
				->addFieldToFilter('id', array('gt' => $lastid));					
				$RowCount =$data_collection_temp->count ();
				
				$data_collection->addFieldToFilter('modified', array('eq' => $lastdate))
				->addFieldToFilter('id', array('gt' => $lastid))				
				->addFieldToSelect ( "*" )
				->addAttributeToSort("id","ASC");
				$data_collection->getSelect ()->limit ( $limit );
	
			}
	
				
		
			if ($data_collection->count () > 0) {
				$lastdatesend = $data_collection->getLastItem ()->getModified();
				$lastidsend=$data_collection->getLastItem()->getID();				
				foreach ( $data_collection as $modelRow ) {
					$dataArray = array();
					$dataArray = $modelRow->getData();
					$dataArray['store_currency_code'] = $dataArray['storecurrencycode'];
					unset($dataArray['storecurrencycode']);
					$data[] = $dataArray;
				}
					
	
				$retObj ['data'] = $data;
				$retObj ['lastdate'] = $lastdatesend;
				if($lastdate != $lastdatesend  )
				{
					$lastidsend=0;
				}
				else if($RowCount<$limit)
				{
					$lastidsend=0;
					$duration = 1;
					$dateinsec = strtotime($lastdatesend);
					$lastdatesend = $dateinsec+$duration;
					$lastdatesend = date('Y-m-d H:i:s',$lastdatesend);
					$retObj ['lastdate'] = $lastdatesend;
				}
				$retObj ['lastid'] = $lastidsend;
				if ($data_collection->count () < $limit && $lastid==0)
				{
					$retObj ['islast'] = true;
				}
				else
				{
					$retObj ['islast'] = false;
				}
			} else {
				$duration = 1;
				$dateinsec = strtotime($lastdate);
				$lastdatesend = $dateinsec+$duration;
				$lastdatesend = date('Y-m-d H:i:s',$lastdatesend);
				$retObj ['lastdate'] = $lastdatesend;
				$retObj ['lastid'] = 0;
				if($lastid>0)
				{
					$retObj ['islast'] = false;
				}
				else
					$retObj ['islast'] = true;
				$retObj ['data'] = $data;
			}
			return $retObj;
		} catch ( Exception $e ) {
			$retObj ['lastid'] = -1;
			$retObj ['lastdate'] = $lastdate;
			$retObj ['islast'] = false;
			$retObj ['data'] = $data;
			return $retObj;
		}
	}
	
}