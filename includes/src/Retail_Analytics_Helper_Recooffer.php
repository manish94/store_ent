<?php

class Retail_Analytics_Helper_Recooffer extends Mage_Core_Helper_Abstract
{
		
	public function saveRecooffer($data) {			
		
		$recooffer = Mage::getModel('retail_analytics/recooffer');
		$recooffer->setCustomerid($data['customerid']);
		$recooffer->setEmail($data['email']);
		$recooffer->setValidfrom($data['validfrom']);
		$recooffer->setValidto($data['validto']);
		$recooffer->setProductid($data['productid']);
		$recooffer->setPrice($data['price']);
		$recooffer->setOfferprice($data['offerprice']);
		$recooffer->setCouponcode($data['couponcode']);
		$recooffer->setCampaign($data['campaign']);
		$recooffer->setCreated(now());
		$recooffer->save();
		
	}	
	
	
	public function addRecooffer($data) {
	
		try {	
			foreach ( $data as $row ) {				
				
					$this->saveRecooffer( $row );
			}
		}
		catch ( Exception $e ) {
			return false;
		}
		return true;
	}
	
	
	
	public function removeRecooffer($campaign) {	
	
		try {
			
			$data_collection = Mage::getModel ( 'retail_analytics/recooffer' )->getCollection()->addFieldToFilter ( 'campaign', array ($campaign) );
			foreach ($data_collection as $data)
			{
				$recooffer = Mage::getModel('retail_analytics/recooffer')->load($data->getData('id'));
				$recooffer->delete();	
			}	
			
		}			
		catch ( Exception $e ) {
			return false;
		}		
		return true;
	}	
	
		
}