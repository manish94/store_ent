<?php

class Retail_Analytics_Model_Config_Product extends Mage_Core_Model_Config_Data
{
	
	public function toOptionArray()
	{
		return array(
				array('value'=>'0', 'label'=>'Select'),
				array('value'=>'1', 'label'=>'All'),				
				array('value'=>'2', 'label'=>'Related'),	
				array('value'=>'3', 'label'=>'Upsell')
		);
	}
	 /**
     * Save object data.
     * Validates that the account is set.
     *
     * @return Nosto_Tagging_Model_Config_Account
     */
    public function save()
    {
        $product = $this->getValue();
       /*  if (empty($product)) {
            Mage::throwException(Mage::helper('retail_analytics')->__('Show product recommendations is required.'));
        } */

        return parent::save();
    }
}
