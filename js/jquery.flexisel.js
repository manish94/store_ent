!function(t){t.fn.flexisel=function(e){var i,n,s,o,l=t.extend({visibleItems:7,itemsToScroll:1,animationSpeed:400,infinite:!0,navigationTargetSelector:null,autoPlay:{enable:!1,interval:5e3,pauseOnHover:!0},responsiveBreakpoints:{portrait:{changePoint:480,visibleItems:1,itemsToScroll:1},landscape:{changePoint:640,visibleItems:2,itemsToScroll:1},tablet:{changePoint:768,visibleItems:3,itemsToScroll:1}}},e),a=t(this),r=t.extend(l,e),f=!0,c=r.visibleItems,d=r.itemsToScroll,h=[],u={init:function(){return this.each(function(){u.appendHTML(),u.setEventHandlers(),u.initializeItems()})},initializeItems:function(){var e=r.responsiveBreakpoints;for(var s in e)h.push(e[s]);h.sort(function(t,e){return t.changePoint-e.changePoint});var o=a.children();i=u.getCurrentItemWidth(),n=o.length,o.width(i),a.css({left:-i*(c+1)}),a.fadeIn(),t(window).trigger("resize")},appendHTML:function(){if(a.addClass("nbs-flexisel-ul"),a.wrap("<div class='nbs-flexisel-container'><div class='nbs-flexisel-inner'></div></div>"),a.find("li").addClass("nbs-flexisel-item"),r.navigationTargetSelector&&t(r.navigationTargetSelector).length>0?t("<div class='nbs-flexisel-nav-left'></div><div class='nbs-flexisel-nav-right'></div>").appendTo(r.navigationTargetSelector):(r.navigationTargetSelector=a.parent(),t("<div class='nbs-flexisel-nav-left'></div><div class='nbs-flexisel-nav-right'></div>").insertAfter(a)),r.infinite){var e=a.children(),i=e.clone(),n=e.clone();a.prepend(i),a.append(n)}},setEventHandlers:function(){var e=a.children();t(window).on("resize",function(n){clearTimeout(s),s=setTimeout(function(){u.calculateDisplay(),i=u.getCurrentItemWidth(),e.width(i),r.infinite?a.css({left:-i*Math.floor(e.length/2)}):(u.clearDisabled(),t(r.navigationTargetSelector).find(".nbs-flexisel-nav-left").addClass("disabled"),a.css({left:0}))},100)}),t(r.navigationTargetSelector).find(".nbs-flexisel-nav-left").on("click",function(t){u.scroll(!0)}),t(r.navigationTargetSelector).find(".nbs-flexisel-nav-right").on("click",function(t){u.scroll(!1)}),r.autoPlay.enable&&(u.setAutoplayInterval(),r.autoPlay.pauseOnHover===!0&&a.on({mouseenter:function(){f=!1},mouseleave:function(){f=!0}})),a[0].addEventListener("touchstart",u.touchHandler.handleTouchStart,!1),a[0].addEventListener("touchmove",u.touchHandler.handleTouchMove,!1)},calculateDisplay:function(){var e=t("html").width(),i=h[h.length-1].changePoint;for(var n in h){if(e>=i){c=r.visibleItems,d=r.itemsToScroll;break}if(e<h[n].changePoint){c=h[n].visibleItems,d=h[n].itemsToScroll;break}}},scroll:function(t){if("undefined"==typeof t&&(t=!0),1==f){if(f=!1,i=u.getCurrentItemWidth(),r.autoPlay.enable&&clearInterval(o),r.infinite)a.animate({left:t?"+="+i*d:"-="+i*d},r.animationSpeed,function(){f=!0,t?u.offsetItemsToBeginning(d):u.offsetItemsToEnd(d),u.offsetSliderPosition(t)});else{var e=i*d;t?a.animate({left:u.calculateNonInfiniteLeftScroll(e)},r.animationSpeed,function(){f=!0}):a.animate({left:u.calculateNonInfiniteRightScroll(e)},r.animationSpeed,function(){f=!0})}r.autoPlay.enable&&u.setAutoplayInterval()}},touchHandler:{xDown:null,yDown:null,handleTouchStart:function(t){this.xDown=t.touches[0].clientX,this.yDown=t.touches[0].clientY},handleTouchMove:function(t){if(this.xDown&&this.yDown){var e=t.touches[0].clientX,i=t.touches[0].clientY,n=this.xDown-e;this.yDown-i;Math.abs(n)>0&&(n>0?u.scroll(!1):u.scroll(!0)),this.xDown=null,this.yDown=null,f=!0}}},getCurrentItemWidth:function(){return a.parent().width()/c},offsetItemsToBeginning:function(t){"undefined"==typeof t&&(t=1);for(var e=0;t>e;e++)a.children().last().insertBefore(a.children().first())},offsetItemsToEnd:function(t){"undefined"==typeof t&&(t=1);for(var e=0;t>e;e++)a.children().first().insertAfter(a.children().last())},offsetSliderPosition:function(t){var e=parseInt(a.css("left").replace("px",""));t?e-=i*d:e+=i*d,a.css({left:e})},getOffsetPosition:function(){return parseInt(a.css("left").replace("px",""))},calculateNonInfiniteLeftScroll:function(e){return u.clearDisabled(),u.getOffsetPosition()+e>=0?(t(r.navigationTargetSelector).find(".nbs-flexisel-nav-left").addClass("disabled"),0):u.getOffsetPosition()+e},calculateNonInfiniteRightScroll:function(e){u.clearDisabled();var s=n*i-c*i;return u.getOffsetPosition()-e<=-s?(t(r.navigationTargetSelector).find(".nbs-flexisel-nav-right").addClass("disabled"),-s):u.getOffsetPosition()-e},setAutoplayInterval:function(){o=setInterval(function(){f&&u.scroll(!1)},r.autoPlay.interval)},clearDisabled:function(){var e=t(r.navigationTargetSelector);e.find(".nbs-flexisel-nav-left").removeClass("disabled"),e.find(".nbs-flexisel-nav-right").removeClass("disabled")}};return u[e]?u[e].apply(this,Array.prototype.slice.call(arguments,1)):"object"!=typeof e&&e?void t.error('Method "'+method+'" does not exist in flexisel plugin!'):u.init.apply(this)}}(jQuery),function(t){function e(t,e){return"function"==typeof t?t.call(e):t}function i(e,i){this.$element=t(e),this.options=i,this.enabled=!0,this.fixTitle()}i.prototype={show:function(){var i=this.getTitle();if(i&&this.enabled){var n=this.tip();n.find(".tipsy-inner")[this.options.html?"html":"text"](i),n[0].className="tipsy",n.remove().css({top:0,left:0,visibility:"hidden",display:"block"}).prependTo(document.body);var s,o=t.extend({},this.$element.offset(),{width:this.$element[0].offsetWidth,height:this.$element[0].offsetHeight}),l=n[0].offsetWidth,a=n[0].offsetHeight,r=e(this.options.gravity,this.$element[0]);switch(r.charAt(0)){case"n":s={top:o.top+o.height+this.options.offset,left:o.left+o.width/2-l/2};break;case"s":s={top:o.top-a-this.options.offset,left:o.left+o.width/2-l/2};break;case"e":s={top:o.top+o.height/2-a/2,left:o.left-l-this.options.offset};break;case"w":s={top:o.top+o.height/2-a/2,left:o.left+o.width+this.options.offset}}2==r.length&&("w"==r.charAt(1)?s.left=o.left+o.width/2-15:s.left=o.left+o.width/2-l+15),n.css(s).addClass("tipsy-"+r),n.find(".tipsy-arrow")[0].className="tipsy-arrow tipsy-arrow-"+r.charAt(0),this.options.className&&n.addClass(e(this.options.className,this.$element[0])),this.options.fade?n.stop().css({opacity:0,display:"block",visibility:"visible"}).animate({opacity:this.options.opacity}):n.css({visibility:"visible",opacity:this.options.opacity})}},hide:function(){this.options.fade?this.tip().stop().fadeOut(function(){t(this).remove()}):this.tip().remove()},fixTitle:function(){var t=this.$element;(t.attr("title")||"string"!=typeof t.attr("original-title"))&&t.attr("original-title",t.attr("title")||"").removeAttr("title")},getTitle:function(){var t,e=this.$element,i=this.options;this.fixTitle();var t,i=this.options;return"string"==typeof i.title?t=e.attr("title"==i.title?"original-title":i.title):"function"==typeof i.title&&(t=i.title.call(e[0])),t=(""+t).replace(/(^\s*|\s*$)/,""),t||i.fallback},tip:function(){return this.$tip||(this.$tip=t('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"></div>')),this.$tip},validate:function(){this.$element[0].parentNode||(this.hide(),this.$element=null,this.options=null)},enable:function(){this.enabled=!0},disable:function(){this.enabled=!1},toggleEnabled:function(){this.enabled=!this.enabled}},t.fn.tipsy=function(e){function n(n){var s=t.data(n,"tipsy");return s||(s=new i(n,t.fn.tipsy.elementOptions(n,e)),t.data(n,"tipsy",s)),s}function s(){var t=n(this);t.hoverState="in",0==e.delayIn?t.show():(t.fixTitle(),setTimeout(function(){"in"==t.hoverState&&t.show()},e.delayIn))}function o(){var t=n(this);t.hoverState="out",0==e.delayOut?t.hide():setTimeout(function(){"out"==t.hoverState&&t.hide()},e.delayOut)}if(e===!0)return this.data("tipsy");if("string"==typeof e){var l=this.data("tipsy");return l&&l[e](),this}if(e=t.extend({},t.fn.tipsy.defaults,e),e.live||this.each(function(){n(this)}),"manual"!=e.trigger){var a=e.live?"live":"bind",r="hover"==e.trigger?"mouseenter":"focus",f="hover"==e.trigger?"mouseleave":"blur";this[a](r,s)[a](f,o)}return this},t.fn.tipsy.defaults={className:null,delayIn:0,delayOut:0,fade:!1,fallback:"",gravity:"n",html:!1,live:!1,offset:0,opacity:.8,title:"title",trigger:"hover"},t.fn.tipsy.elementOptions=function(e,i){return t.metadata?t.extend({},i,t(e).metadata()):i},t.fn.tipsy.autoNS=function(){return t(this).offset().top>t(document).scrollTop()+t(window).height()/2?"s":"n"},t.fn.tipsy.autoWE=function(){return t(this).offset().left>t(document).scrollLeft()+t(window).width()/2?"e":"w"},t.fn.tipsy.autoBounds=function(e,i){return function(){var n={ns:i[0],ew:i.length>1?i[1]:!1},s=t(document).scrollTop()+e,o=t(document).scrollLeft()+e,l=t(this);return l.offset().top<s&&(n.ns="n"),l.offset().left<o&&(n.ew="w"),t(window).width()+t(document).scrollLeft()-l.offset().left<e&&(n.ew="e"),t(window).height()+t(document).scrollTop()-l.offset().top<e&&(n.ns="s"),n.ns+(n.ew?n.ew:"")}}}(jQuery);

(function($) {
    
    function maybeCall(thing, ctx) {
        return (typeof thing == 'function') ? (thing.call(ctx)) : thing;
    };
    
    function Tipsy(element, options) {
        this.$element = $(element);
        this.options = options;
        this.enabled = true;
        this.fixTitle();
    };
    
    Tipsy.prototype = {
        show: function() {
            var title = this.getTitle();
            if (title && this.enabled) {
                var $tip = this.tip();
                
                $tip.find('.tipsy-inner')[this.options.html ? 'html' : 'text'](title);
                $tip[0].className = 'tipsy'; // reset classname in case of dynamic gravity
                $tip.remove().css({top: 0, left: 0, visibility: 'hidden', display: 'block'}).prependTo(document.body);
                
                var pos = $.extend({}, this.$element.offset(), {
                    width: this.$element[0].offsetWidth,
                    height: this.$element[0].offsetHeight
                });
                
                var actualWidth = $tip[0].offsetWidth,
                    actualHeight = $tip[0].offsetHeight,
                    gravity = maybeCall(this.options.gravity, this.$element[0]);
                
                var tp;
                switch (gravity.charAt(0)) {
                    case 'n':
                        tp = {top: pos.top + pos.height + this.options.offset, left: pos.left + pos.width / 2 - actualWidth / 2};
                        break;
                    case 's':
                        tp = {top: pos.top - actualHeight - this.options.offset, left: pos.left + pos.width / 2 - actualWidth / 2};
                        break;
                    case 'e':
                        tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth - this.options.offset};
                        break;
                    case 'w':
                        tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width + this.options.offset};
                        break;
                }
                
                if (gravity.length == 2) {
                    if (gravity.charAt(1) == 'w') {
                        tp.left = pos.left + pos.width / 2 - 15;
                    } else {
                        tp.left = pos.left + pos.width / 2 - actualWidth + 15;
                    }
                }
                
                $tip.css(tp).addClass('tipsy-' + gravity);
                $tip.find('.tipsy-arrow')[0].className = 'tipsy-arrow tipsy-arrow-' + gravity.charAt(0);
                if (this.options.className) {
                    $tip.addClass(maybeCall(this.options.className, this.$element[0]));
                }
                
                if (this.options.fade) {
                    $tip.stop().css({opacity: 0, display: 'block', visibility: 'visible'}).animate({opacity: this.options.opacity});
                } else {
                    $tip.css({visibility: 'visible', opacity: this.options.opacity});
                }
            }
        },
        
        hide: function() {
            if (this.options.fade) {
                this.tip().stop().fadeOut(function() { $(this).remove(); });
            } else {
                this.tip().remove();
            }
        },
        
        fixTitle: function() {
            var $e = this.$element;
            if ($e.attr('title') || typeof($e.attr('original-title')) != 'string') {
                $e.attr('original-title', $e.attr('title') || '').removeAttr('title');
            }
        },
        
        getTitle: function() {
            var title, $e = this.$element, o = this.options;
            this.fixTitle();
            var title, o = this.options;
            if (typeof o.title == 'string') {
                title = $e.attr(o.title == 'title' ? 'original-title' : o.title);
            } else if (typeof o.title == 'function') {
                title = o.title.call($e[0]);
            }
            title = ('' + title).replace(/(^\s*|\s*$)/, "");
            return title || o.fallback;
        },
        
        tip: function() {
            if (!this.$tip) {
                this.$tip = $('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"></div>');
            }
            return this.$tip;
        },
        
        validate: function() {
            if (!this.$element[0].parentNode) {
                this.hide();
                this.$element = null;
                this.options = null;
            }
        },
        
        enable: function() { this.enabled = true; },
        disable: function() { this.enabled = false; },
        toggleEnabled: function() { this.enabled = !this.enabled; }
    };
    
    $.fn.tipsy = function(options) {
        
        if (options === true) {
            return this.data('tipsy');
        } else if (typeof options == 'string') {
            var tipsy = this.data('tipsy');
            if (tipsy) tipsy[options]();
            return this;
        }
        
        options = $.extend({}, $.fn.tipsy.defaults, options);
        
        function get(ele) {
            var tipsy = $.data(ele, 'tipsy');
            if (!tipsy) {
                tipsy = new Tipsy(ele, $.fn.tipsy.elementOptions(ele, options));
                $.data(ele, 'tipsy', tipsy);
            }
            return tipsy;
        }
        
        function enter() {
            var tipsy = get(this);
            tipsy.hoverState = 'in';
            if (options.delayIn == 0) {
                tipsy.show();
            } else {
                tipsy.fixTitle();
                setTimeout(function() { if (tipsy.hoverState == 'in') tipsy.show(); }, options.delayIn);
            }
        };
        
        function leave() {
            var tipsy = get(this);
            tipsy.hoverState = 'out';
            if (options.delayOut == 0) {
                tipsy.hide();
            } else {
                setTimeout(function() { if (tipsy.hoverState == 'out') tipsy.hide(); }, options.delayOut);
            }
        };
        
        if (!options.live) this.each(function() { get(this); });
        
        if (options.trigger != 'manual') {
            var binder   = options.live ? 'live' : 'bind',
                eventIn  = options.trigger == 'hover' ? 'mouseenter' : 'focus',
                eventOut = options.trigger == 'hover' ? 'mouseleave' : 'blur';
            this[binder](eventIn, enter)[binder](eventOut, leave);
        }
        
        return this;
        
    };
    
    $.fn.tipsy.defaults = {
        className: null,
        delayIn: 0,
        delayOut: 0,
        fade: false,
        fallback: '',
        gravity: 'n',
        html: false,
        live: false,
        offset: 0,
        opacity: 0.8,
        title: 'title',
        trigger: 'hover'
    };
    
    // Overwrite this method to provide options on a per-element basis.
    // For example, you could store the gravity in a 'tipsy-gravity' attribute:
    // return $.extend({}, options, {gravity: $(ele).attr('tipsy-gravity') || 'n' });
    // (remember - do not modify 'options' in place!)
    $.fn.tipsy.elementOptions = function(ele, options) {
        return $.metadata ? $.extend({}, options, $(ele).metadata()) : options;
    };
    
    $.fn.tipsy.autoNS = function() {
        return $(this).offset().top > ($(document).scrollTop() + $(window).height() / 2) ? 's' : 'n';
    };
    
    $.fn.tipsy.autoWE = function() {
        return $(this).offset().left > ($(document).scrollLeft() + $(window).width() / 2) ? 'e' : 'w';
    };
    
    /**
     * yields a closure of the supplied parameters, producing a function that takes
     * no arguments and is suitable for use as an autogravity function like so:
     *
     * @param margin (int) - distance from the viewable region edge that an
     *        element should be before setting its tooltip's gravity to be away
     *        from that edge.
     * @param prefer (string, e.g. 'n', 'sw', 'w') - the direction to prefer
     *        if there are no viewable region edges effecting the tooltip's
     *        gravity. It will try to vary from this minimally, for example,
     *        if 'sw' is preferred and an element is near the right viewable 
     *        region edge, but not the top edge, it will set the gravity for
     *        that element's tooltip to be 'se', preserving the southern
     *        component.
     */
     $.fn.tipsy.autoBounds = function(margin, prefer) {
		return function() {
			var dir = {ns: prefer[0], ew: (prefer.length > 1 ? prefer[1] : false)},
			    boundTop = $(document).scrollTop() + margin,
			    boundLeft = $(document).scrollLeft() + margin,
			    $this = $(this);

			if ($this.offset().top < boundTop) dir.ns = 'n';
			if ($this.offset().left < boundLeft) dir.ew = 'w';
			if ($(window).width() + $(document).scrollLeft() - $this.offset().left < margin) dir.ew = 'e';
			if ($(window).height() + $(document).scrollTop() - $this.offset().top < margin) dir.ns = 's';

			return dir.ns + (dir.ew ? dir.ew : '');
		};
	};
    
})(jQuery);